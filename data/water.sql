/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : water

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-12-27 18:13:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for water_audit_log
-- ----------------------------
DROP TABLE IF EXISTS `water_audit_log`;
CREATE TABLE `water_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `action` varchar(255) NOT NULL DEFAULT '' COMMENT '对进出账的增删改、交易付费的行为，json格式保存',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '操作的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户操作行为记录';

-- ----------------------------
-- Records of water_audit_log
-- ----------------------------

-- ----------------------------
-- Table structure for water_login_log
-- ----------------------------
DROP TABLE IF EXISTS `water_login_log`;
CREATE TABLE `water_login_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(128) NOT NULL DEFAULT '' COMMENT 'ip地址',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '登录的时间',
  `reg_os` varchar(128) NOT NULL DEFAULT 'pc' COMMENT '登录的设备',
  `account` varchar(50) NOT NULL DEFAULT '0' COMMENT '用户(手机号)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of water_login_log
-- ----------------------------
INSERT INTO `water_login_log` VALUES ('1', '127.0.0.1', '2017-12-21 18:09:44', 'pc', '1');
INSERT INTO `water_login_log` VALUES ('2', '127.0.0.1', '2017-12-21 18:11:51', 'pc', 'weipeng');
INSERT INTO `water_login_log` VALUES ('3', '127.0.0.1', '2017-12-22 09:58:18', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('4', '127.0.0.1', '2017-12-22 11:40:46', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('5', '127.0.0.1', '2017-12-22 16:06:23', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('6', '127.0.0.1', '2017-12-22 16:23:14', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('7', '127.0.0.1', '2017-12-22 16:50:08', 'pc', 'weipeng');
INSERT INTO `water_login_log` VALUES ('8', '127.0.0.1', '2017-12-22 21:23:35', 'pc', 'weipeng');
INSERT INTO `water_login_log` VALUES ('9', '127.0.0.1', '2017-12-22 23:36:56', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('10', '127.0.0.1', '2017-12-22 23:50:26', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('11', '127.0.0.1', '2017-12-23 20:12:36', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('12', '127.0.0.1', '2017-12-25 09:44:52', 'pc', 'weipeng');
INSERT INTO `water_login_log` VALUES ('13', '127.0.0.1', '2017-12-25 10:26:03', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('14', '127.0.0.1', '2017-12-25 11:11:34', 'pc', 'weipeng');
INSERT INTO `water_login_log` VALUES ('15', '127.0.0.1', '2017-12-25 14:31:58', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('16', '127.0.0.1', '2017-12-25 15:28:24', 'pc', 'weipeng');
INSERT INTO `water_login_log` VALUES ('17', '127.0.0.1', '2017-12-25 15:29:08', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('18', '127.0.0.1', '2017-12-26 15:14:20', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('19', '127.0.0.1', '2017-12-27 10:13:56', 'pc', 'admin');
INSERT INTO `water_login_log` VALUES ('20', '127.0.0.1', '2017-12-27 13:41:53', 'pc', 'weipeng');

-- ----------------------------
-- Table structure for water_privilege
-- ----------------------------
DROP TABLE IF EXISTS `water_privilege`;
CREATE TABLE `water_privilege` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  `privilege_name` varchar(20) NOT NULL DEFAULT '' COMMENT '权限名称',
  `action_name` varchar(100) NOT NULL DEFAULT '' COMMENT '控制器@动作名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态：1-启用，2-停用',
  `updated_at` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '上级id',
  `img_url` varchar(255) NOT NULL DEFAULT '' COMMENT '设置的logo',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '变动的时间',
  `is_del` tinyint(10) unsigned NOT NULL DEFAULT '0' COMMENT '''伪删除''',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='权限';

-- ----------------------------
-- Records of water_privilege
-- ----------------------------
INSERT INTO `water_privilege` VALUES ('1', '账单系统', '/bill/index/', '1', '0', '0', '/static/backend/images/icon01.png', '2017-12-26 17:04:32', '0');
INSERT INTO `water_privilege` VALUES ('2', '系统设置', '/operator/index/', '1', '0', '0', '/static/backend/images/icon06.png', '2017-12-26 17:04:32', '0');
INSERT INTO `water_privilege` VALUES ('3', '用户管理', '/user/index/', '1', '0', '0', '/static/backend/images/icon03.png', '2017-12-26 17:04:32', '0');
INSERT INTO `water_privilege` VALUES ('6', '添加权限', '/operator/add/', '1', '0', '2', '', '2017-12-26 17:04:32', '0');
INSERT INTO `water_privilege` VALUES ('7', '权限列表', '/operator/index/', '1', '0', '2', '', '2017-12-26 17:04:32', '0');
INSERT INTO `water_privilege` VALUES ('15', '8', '889', '1', '0', '2', '8989', '2017-12-27 10:14:50', '1');
INSERT INTO `water_privilege` VALUES ('16', 'sdds', 'dssd', '1', '0', '2', 'sdsd', '2017-12-27 13:42:06', '1');

-- ----------------------------
-- Table structure for water_setting
-- ----------------------------
DROP TABLE IF EXISTS `water_setting`;
CREATE TABLE `water_setting` (
  `skey` char(50) NOT NULL,
  `svalue` text NOT NULL,
  PRIMARY KEY (`skey`),
  UNIQUE KEY `skey` (`skey`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of water_setting
-- ----------------------------

-- ----------------------------
-- Table structure for water_staff_phone
-- ----------------------------
DROP TABLE IF EXISTS `water_staff_phone`;
CREATE TABLE `water_staff_phone` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `phone` bigint(11) unsigned NOT NULL DEFAULT '0' COMMENT ' 手机号码',
  `username` char(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通讯录';

-- ----------------------------
-- Records of water_staff_phone
-- ----------------------------

-- ----------------------------
-- Table structure for water_tests
-- ----------------------------
DROP TABLE IF EXISTS `water_tests`;
CREATE TABLE `water_tests` (
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of water_tests
-- ----------------------------
INSERT INTO `water_tests` VALUES (null, null);
INSERT INTO `water_tests` VALUES (null, null);
INSERT INTO `water_tests` VALUES (null, null);
INSERT INTO `water_tests` VALUES ('gfgf', null);
INSERT INTO `water_tests` VALUES ('gfgf', null);
INSERT INTO `water_tests` VALUES ('gfgf', '1');
INSERT INTO `water_tests` VALUES ('gfgf', null);
INSERT INTO `water_tests` VALUES ('gfgf', 'name=阿萨德&big_id=0&action_name=说的&status=1&img_url=是');

-- ----------------------------
-- Table structure for water_user
-- ----------------------------
DROP TABLE IF EXISTS `water_user`;
CREATE TABLE `water_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL DEFAULT '' COMMENT '账号(手机号码)',
  `username` char(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `pwd` varchar(100) NOT NULL DEFAULT '' COMMENT '登陆密码',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '0、启动  1、停用',
  `create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `pid` varchar(255) NOT NULL DEFAULT '0' COMMENT '权限id，ids',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of water_user
-- ----------------------------
INSERT INTO `water_user` VALUES ('1', 'admin', '魏鹏', '4f7cf9113876193e571cc6e4004548b721da31a1', '1', '0000-00-00 00:00:00', '1,2,3,6,7,12,15');
INSERT INTO `water_user` VALUES ('2', 'weipeng', 'weiepng', '23c5e931cc3512163caad418cfcef05754ce98c5', '1', '0000-00-00 00:00:00', '1,2,3,6,7,12');
