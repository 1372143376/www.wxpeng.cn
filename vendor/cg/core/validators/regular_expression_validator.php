<?php
namespace cg\core\validators;
use cg\core\helpers\json;
use cg\core\web\js_expression;
use Exception;

/**
 * RegularExpressionValidator validates that the attribute value matches the specified [[pattern]].
 *
 * If the [[not]] property is set true, the validator will ensure the attribute value do NOT match the [[pattern]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class regular_expression_validator extends validator
{
	/**
	 * @var string the regular expression to be matched with
	 */
	public $pattern;
	/**
	 * @var boolean whether to invert the validation logic. Defaults to false. If set to true,
	 * the regular expression defined via [[pattern]] should NOT match the attribute value.
	 */
	public $not = false;

	public function __construct($params)
	{
		parent::__construct($params);
		
		if ($this->pattern == null) {
			throw new Exception('The "pattern" property must be set.');
		}
		if ($this->message === null) {
			$this->message = '{attribute} 无效';
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function validateValue($value)
	{
		$valid = !is_array($value) &&
			(!$this->not && preg_match($this->pattern, $value)
				|| $this->not && !preg_match($this->pattern, $value));

		return $valid ? null : [$this->message, []];
	}

	/**
	 * @inheritdoc
	 */
	public function clientValidateAttribute($model, $attribute, $view)
	{
		$pattern = $this->pattern;
		$pattern = preg_replace('/\\\\x\{?([0-9a-fA-F]+)\}?/', '\u$1', $pattern);
		$deliminator = substr($pattern, 0, 1);
		$pos = strrpos($pattern, $deliminator, 1);
		$flag = substr($pattern, $pos + 1);
		if ($deliminator !== '/') {
			$pattern = '/' . str_replace('/', '\\/', substr($pattern, 1, $pos - 1)) . '/';
		} else {
			$pattern = substr($pattern, 0, $pos + 1);
		}
		if (!empty($flag)) {
			$pattern .= preg_replace('/[^igm]/', '', $flag);
		}

		$options = [
			'pattern' => new js_expression($pattern),
			'not' => $this->not,
			'message' => $this->format_message($this->message, $model, $attribute)
		];
		if ($this->skipOnEmpty) {
			$options['skipOnEmpty'] = 1;
		}

		return 'yii.validation.regularExpression(value, messages, ' . json::html_encode($options) . ');';
	}
}
