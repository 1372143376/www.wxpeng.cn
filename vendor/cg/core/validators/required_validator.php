<?php
namespace cg\core\validators;

/**
 * RequiredValidator validates that the specified attribute does not have null or empty value.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class required_validator extends validator
{
	/**
	 * @var boolean whether to skip this validator if the value being validated is empty.
	 */
	public $skipOnEmpty = false;
	/**
	 * @var boolean whether the comparison between the attribute value and [[requiredValue]] is strict.
	 * When this is true, both the values and types must match.
	 * Defaults to false, meaning only the values need to match.
	 * Note that when [[requiredValue]] is null, if this property is true, the validator will check
	 * if the attribute value is null; If this property is false, the validator will call [[isEmpty]]
	 * to check if the attribute value is empty.
	 */
	public $strict = false;
	/**
	 * @var string the user-defined error message. It may contain the following placeholders which
	 * will be replaced accordingly by the validator:
	 *
	 * - `{attribute}`: the label of the attribute being validated
	 * - `{value}`: the value of the attribute being validated
	 * - `{requiredValue}`: the value of [[requiredValue]]
	 */
	public $message;

	public function __construct($params)
	{
		parent::__construct($params);
		
		if ($this->message === null) {
			$this->message = "{attribute} 必填";
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function validateValue($value)
	{
		if ($this->strict && $value !== null || !$this->strict && !$this->isEmpty(is_string($value) ? trim($value) : $value)) {
			return null;
		}
		return [$this->message, []];
	}

	/**
	 * @inheritdoc
	 */
	public function clientValidateAttribute($model, $attribute, $view)
	{
		$options = [];
		$options['message'] = $this->format_message($this->message, $model, $attribute);
		if ($this->strict) {
			$options['strict'] = 1;
		}

		return 'yii.validation.required(value, messages, ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ');';
	}
}
