<?php
namespace cg\core;

class cg_config
{
	public $APP_PATH;
    public $VENDOR_PATH;
	public $RUNTIME_PATH;
	// 每个网站的名字
	public $APP_NAME;
	public $APP_ROOT;
	// APP 模块目录, 比如 API 域名下面的 v1, v2 模块
	public $APP_SUBDIR;
    public $CONTROLLER_NAMESPACE;
	public $AUTO_VIEW_RENDER_PATH;
	public $APP_URL;
	public $AUTO_ROUTE;
	public $ERROR_404_DOCUMENT;
	public $ERROR_404_ROUTE;
	public $CONTROLLER_PATH;
	public $MODEL_PATH;
	public $MODULE_PATH;
	public $config;

	/**
	 * @param config object $config
	 */
	public function init($config)
	{
		$this->config = $config;
		foreach ($config['system'] as $k => $v)
		{
			$this->{$k} = $v;
		}
		unset($this->config['system']);
		
		$this->VENDOR_PATH = $this->APP_ROOT . 'vendor/';
		$this->RUNTIME_PATH = $this->APP_ROOT . 'runtime/';
		if ($this->APP_SUBDIR == NULL)
		{
			$this->APP_SUBDIR = '/';
		}
		if ($this->AUTO_ROUTE == NULL)
		{
			$this->AUTO_ROUTE = FALSE;
		}
		$this->MODEL_PATH = $this->APP_ROOT. 'model/';
		$this->CONTROLLER_PATH = $this->APP_PATH . 'controller/';
		$this->MODULE_PATH = $this->APP_ROOT. 'module/';
	}
}
