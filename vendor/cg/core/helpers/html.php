<?php
namespace cg\core\helpers;

use cg\core\cg;

class html
{
	/**
	 * @var array list of void elements (element name => 1)
	 * @see http://www.w3.org/TR/html-markup/syntax.html#void-element
	 */
	public static $voidElements = [
		'link' => 1,
		'meta' => 1,
		'img' => 1
	];
	/**
	 * @var array the preferred order of attributes in a tag. This mainly affects the order of the attributes
	 * that are rendered by [[renderTagAttributes()]].
	 */
	public static $attributeOrder = [
		'type',
		'id',
		'class',
		'name',
		'value',

		'href',
		'src',
		'action',
		'method',

		'selected',
		'checked',
		'readonly',
		'disabled',
		'multiple',

		'size',
		'maxlength',
		'width',
		'height',
		'rows',
		'cols',

		'alt',
		'title',
		'rel',
		'media',
	];

	/**
	 * @var array list of tag attributes that should be specially handled when their values are of array type.
	 * In particular, if the value of the `data` attribute is `['name' => 'xyz', 'age' => 13]`, two attributes
	 * will be generated instead of one: `data-name="xyz" data-age="13"`.
	 * @since 2.0.3
	 */
	public static $dataAttributes = ['data', 'data-ng', 'ng'];


	/**
	 * Encodes special characters into HTML entities.
	 * The [[\yii\base\Application::charset|application charset]] will be used for encoding.
	 * @param string $content the content to be encoded
	 * @param boolean $doubleEncode whether to encode HTML entities in `$content`. If false,
	 * HTML entities in `$content` will not be further encoded.
	 * @return string the encoded content
	 * @see decode()
	 * @see http://www.php.net/manual/en/function.htmlspecialchars.php
	 */
	public static function encode($content, $doubleEncode = true)
	{
//		return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE, Yii::$app ? Yii::$app->charset : 'UTF-8', $doubleEncode);
		return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8', $doubleEncode);
	}

	/**
	 * Decodes special HTML entities back to the corresponding characters.
	 * This is the opposite of [[encode()]].
	 * @param string $content the content to be decoded
	 * @return string the decoded content
	 * @see encode()
	 * @see http://www.php.net/manual/en/function.htmlspecialchars-decode.php
	 */
	public static function decode($content)
	{
		return htmlspecialchars_decode($content, ENT_QUOTES);
	}

	/**
	 * Generates a complete HTML tag.
	 * @param string $name the tag name
	 * @param string $content the content to be enclosed between the start and end tags. It will not be HTML-encoded.
	 * If this is coming from end users, you should consider [[encode()]] it to prevent XSS attacks.
	 * @param array $options the HTML tag attributes (HTML options) in terms of name-value pairs.
	 * These will be rendered as the attributes of the resulting tag. The values will be HTML-encoded using [[encode()]].
	 * If a value is null, the corresponding attribute will not be rendered.
	 *
	 * For example when using `['class' => 'my-class', 'target' => '_blank', 'value' => null]` it will result in the
	 * html attributes rendered like this: `class="my-class" target="_blank"`.
	 *
	 * See [[renderTagAttributes()]] for details on how attributes are being rendered.
	 *
	 * @return string the generated HTML tag
	 * @see beginTag()
	 * @see endTag()
	 */
	public static function tag($name, $content = '', $options = [])
	{
		$html = "<$name" . static::renderTagAttributes($options) . '>';
		return isset(static::$voidElements[strtolower($name)]) ? $html : "$html$content</$name>";
	}

	/**
	 * Generates an image tag.
	 * @param array|string $src the image URL. This parameter will be processed by [[Url::to()]].
	 * @param array $options the tag options in terms of name-value pairs. These will be rendered as
	 * the attributes of the resulting tag. The values will be HTML-encoded using [[encode()]].
	 * If a value is null, the corresponding attribute will not be rendered.
	 * See [[renderTagAttributes()]] for details on how attributes are being rendered.
	 * @param bool $local
	 * @return string the generated image tag
	 */
	public static function img($src, $options = [], $local = true)
	{
		if ($local)
		{
			$options['src'] = cg::app()->get_url_service()->getStaticUrl($src);
		}
		return static::tag('img', '', $options);
	}

	/**
	 * Generates a link tag that refers to an external CSS file.
	 * @param array|string $url the URL of the external CSS file. This parameter will be processed by [[Url::to()]].
	 * @param array $options the tag options in terms of name-value pairs. The following option is specially handled:
	 *
	 * - condition: specifies the conditional comments for IE, e.g., `lt IE 9`. When this is specified,
	 *   the generated `link` tag will be enclosed within the conditional comments. This is mainly useful
	 *   for supporting old versions of IE browsers.
	 * - noscript: if set to true, `link` tag will be wrapped into `<noscript>` tags.
	 *
	 * The rest of the options will be rendered as the attributes of the resulting link tag. The values will
	 * be HTML-encoded using [[encode()]]. If a value is null, the corresponding attribute will not be rendered.
	 * See [[renderTagAttributes()]] for details on how attributes are being rendered.
	 * @return string the generated link tag
	 * @see Url::to()
	 */
	public static function cssFile($url, $options = [])
	{
		if (!isset($options['rel'])) {
			$options['rel'] = 'stylesheet';
		}
		$options['href'] = url::to($url);

		if (isset($options['condition'])) {
			$condition = $options['condition'];
			unset($options['condition']);
			return "<!--[if $condition]>\n" . static::tag('link', '', $options) . "\n<![endif]-->";
		} elseif (isset($options['noscript']) && $options['noscript'] === true) {
			unset($options['noscript']);
			return "<noscript>" . static::tag('link', '', $options) . "</noscript>";
		} else {
			return static::tag('link', '', $options) . PHP_EOL;
		}
	}

	/**
	 * Generates a script tag that refers to an external JavaScript file.
	 * @param string $url the URL of the external JavaScript file. This parameter will be processed by [[Url::to()]].
	 * @param array $options the tag options in terms of name-value pairs. The following option is specially handled:
	 *
	 * - condition: specifies the conditional comments for IE, e.g., `lt IE 9`. When this is specified,
	 *   the generated `script` tag will be enclosed within the conditional comments. This is mainly useful
	 *   for supporting old versions of IE browsers.
	 *
	 * The rest of the options will be rendered as the attributes of the resulting script tag. The values will
	 * be HTML-encoded using [[encode()]]. If a value is null, the corresponding attribute will not be rendered.
	 * See [[renderTagAttributes()]] for details on how attributes are being rendered.
	 * @return string the generated script tag
	 * @see Url::to()
	 */
	public static function jsFile($url, $options = [])
	{
		$options['src'] = url::to($url);
		if (isset($options['condition'])) {
			$condition = $options['condition'];
			unset($options['condition']);
			return "<!--[if $condition]>\n" . static::tag('script', '', $options) . "\n<![endif]-->";
		} else {
			return static::tag('script', '', $options) . PHP_EOL;
		}
	}

	/**
	 * Generates the meta tags containing CSRF token information.
	 * @return string the generated meta tags
	 * @see Request::enableCsrfValidation
	 */
	public static function csrfMetaTags()
	{
		$request = Yii::$app->getRequest();
		if ($request instanceof Request && $request->enableCsrfValidation) {
			return static::tag('meta', '', ['name' => 'csrf-param', 'content' => $request->csrfParam]) . "\n    "
			. static::tag('meta', '', ['name' => 'csrf-token', 'content' => $request->getCsrfToken()]) . "\n";
		} else {
			return '';
		}
	}

	/**
	 * Renders the HTML tag attributes.
	 *
	 * Attributes whose values are of boolean type will be treated as
	 * [boolean attributes](http://www.w3.org/TR/html5/infrastructure.html#boolean-attributes).
	 *
	 * Attributes whose values are null will not be rendered.
	 *
	 * The values of attributes will be HTML-encoded using [[encode()]].
	 *
	 * The "data" attribute is specially handled when it is receiving an array value. In this case,
	 * the array will be "expanded" and a list data attributes will be rendered. For example,
	 * if `'data' => ['id' => 1, 'name' => 'yii']`, then this will be rendered:
	 * `data-id="1" data-name="yii"`.
	 * Additionally `'data' => ['params' => ['id' => 1, 'name' => 'yii'], 'status' => 'ok']` will be rendered as:
	 * `data-params='{"id":1,"name":"yii"}' data-status="ok"`.
	 *
	 * @param array $attributes attributes to be rendered. The attribute values will be HTML-encoded using [[encode()]].
	 * @return string the rendering result. If the attributes are not empty, they will be rendered
	 * into a string with a leading white space (so that it can be directly appended to the tag name
	 * in a tag. If there is no attribute, an empty string will be returned.
	 */
	public static function renderTagAttributes($attributes)
	{
		if (count($attributes) > 1) {
			$sorted = [];
			foreach (static::$attributeOrder as $name) {
				if (isset($attributes[$name])) {
					$sorted[$name] = $attributes[$name];
				}
			}
			$attributes = array_merge($sorted, $attributes);
		}

		$html = '';
		foreach ($attributes as $name => $value) {
			if (is_bool($value)) {
				if ($value) {
					$html .= " $name";
				}
			} elseif (is_array($value)) {
				if (in_array($name, static::$dataAttributes)) {
					foreach ($value as $n => $v) {
						if (is_array($v)) {
							$html .= " $name-$n='" . Json::htmlEncode($v) . "'";
						} else {
							$html .= " $name-$n=\"" . static::encode($v) . '"';
						}
					}
				} else {
					$html .= " $name='" . Json::htmlEncode($value) . "'";
				}
			} elseif ($value !== null) {
				$html .= " $name=\"" . static::encode($value) . '"';
			}
		}

		return $html;
	}
}