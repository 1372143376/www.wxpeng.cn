<?php
namespace cg\core\helpers;

use cg\core\web\js_expression;

class json
{
	public static function html_encode($value)
	{
		$expressions = [];
		$value = static::process_data($value, $expressions, uniqid());
		$json = json_encode($value);

		return empty($expressions) ? $json : strtr($json, $expressions);
	}

	/**
	 * Pre-processes the data before sending it to `json_encode()`.
	 * @param mixed $data the data to be processed
	 * @param array $expressions collection of JavaScript expressions
	 * @param string $expPrefix a prefix internally used to handle JS expressions
	 * @return mixed the processed data
	 */
	protected static function process_data($data, &$expressions, $expPrefix)
	{
		if (is_object($data))
		{
			if ($data instanceof js_expression)
			{
				$token = "!{[$expPrefix=" . count($expressions) . ']}!';
				$expressions['"' . $token . '"'] = $data->expression;

				return $token;
			}
			elseif ($data instanceof \JsonSerializable)
			{
				$data = $data->jsonSerialize();
			}
			else
			{
				$result = [];
				foreach ($data as $name => $value)
				{
					$result[$name] = $value;
				}
				$data = $result;
			}

			if ($data === [])
			{
				return new \stdClass();
			}
		}

		if (is_array($data))
		{
			foreach ($data as $key => $value)
			{
				if (is_array($value) || is_object($value))
				{
					$data[$key] = static::process_data($value, $expressions, $expPrefix);
				}
			}
		}

		return $data;
	}
}
