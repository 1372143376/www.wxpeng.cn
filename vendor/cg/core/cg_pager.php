<?php
namespace cg\core;

/**
 * DooPager class file.
 *
 * @author Leng Sheng Hong <darkredz@gmail.com>
 * @link http://www.doophp.com/
 * @copyright Copyright &copy; 2009 Leng Sheng Hong
 * @license http://www.doophp.com/license
 *
 * A helper class to help generating pagination widget when displaying a list of items.
 *
 * You can change every component of the pagination widget through CSS. The CSS class name for the components can be changed if needed.
 *
 * <code>
 * </code>
 *
 * @author Leng Sheng Hong <darkredz@gmail.com>
 * @version $Id: DooPager.php 1000 2009-08-21 15:27:22
 * @package doo.helper
 * @since 1.0
 */
class cg_pager
{
	/**
	 * Text for the Previous link
	 * @var string
	 */
	private $prevText = '&laquo;';

	/**
	 * Text for the Next button link
	 * @var string
	 */
	private $nextText = '&raquo;';

	/**
	 * Items to be displayed per page
	 * @var int
	 */
	public $itemPerPage;

	/**
	 * Maximum Pager length
	 * @var int
	 */
	public $maxLength = 10;

	/**
	 * Total items to be split in the pagination
	 * @var int
	 */
	public $totalItem;

	/**
	 * Total pages in the pagination
	 * @var int
	 */
	public $totalPage;

	/**
	 * The pages HTML output
	 * @var string
	 */
	public $output;

	/**
	 * @var string name of the parameter storing the current page index.
	 * @see params
	 */
	public $pageParam = 'page';

	/**
	 * zero-based current page number
	 * @var int
	 */
	private $_page;

	private $request_service;

	/**
	 * Instantiate the Pager object
	 *
	 * @param int $totalItems Total items to be paginate
	 * @param int $itemPerPage Items to be shown in one page.
	 * @param int $maxLength Number of links for the pager navigation
	 * @param string $prevText Text for the Previous button link
	 * @param string $nextText Text for the Next button link
	 */
	function __construct($totalItems, $itemPerPage = 10, $maxLength = 10, $prevText = '上一页', $nextText = '下一页')
	{
		$this->totalItem = $totalItems;
		$this->maxLength = $maxLength;
		$this->itemPerPage = $itemPerPage;
		$this->prevText = $prevText;
		$this->nextText = $nextText;
		$this->request_service = cg::app()->get_request_service();
	}

	/**
	 * @return integer the offset of the data. This may be used to set the
	 * OFFSET value for a SQL statement for fetching the current page of data.
	 */
	public function getOffset()
	{
		return $this->getPage() * $this->itemPerPage;
	}

	/*
	* Returns the zero-based current page number.
	* @param boolean $recalculate whether to recalculate the current page based on the page size and item count.
	* @return integer the zero-based current page number.
	*/
	public function getPage($recalculate = false)
	{
		if ($this->_page === null || $recalculate)
		{
			$page = (int)$this->request_service->getQueryParam($this->pageParam, 1) - 1;
			$this->setPage($page);
		}

		return $this->_page;
	}

	/**
	 * Sets the current page number.
	 * @param integer $value the zero-based index of the current page.
	 * to validate the page number, both [[validatePage]] and this parameter must be true.
	 */
	private function setPage($value)
	{
		if ($value === null)
		{
			$this->_page = null;
		}
		else
		{
			if ($value < 0)
			{
				$value = 0;
			}
			$this->_page = $value;
		}
	}

	/**
	 * @return integer number of pages
	 */
	public function getPageCount()
	{
		return ceil($this->totalItem / $this->itemPerPage);
	}

	/**
	 * Returns a whole set of links for navigating to the first, last, next and previous pages.
	 * @param boolean $absolute whether the generated URLs should be absolute.
	 * @return array the links for navigational purpose. The array keys specify the purpose of the links (e.g. [[LINK_FIRST]]),
	 * and the array values are the corresponding URLs.
	 */
	public function getLinks($absolute = false)
	{
		// one-based current page number
		$currentPage = $this->getPage() + 1;
		$pageCount = $this->getPageCount();
		
		$links = [
			'self' => $this->createUrl($currentPage, $absolute),
			'current_page' => $currentPage,
			'page_count' => $pageCount
		];
		if ($currentPage > 0)
		{
			$links['first'] = $this->createUrl(1, $absolute);
			$links['prev'] = $this->createUrl($currentPage - 1, $absolute);
		}
		if ($currentPage < $pageCount)
		{
			$links['next'] = $this->createUrl($currentPage + 1, $absolute);
			$links['last'] = $this->createUrl($pageCount , $absolute);
		}

		return $links;
	}

    /**
     * Creates the URL suitable for pagination with the specified page number.
     * This method is mainly called by pagers when creating URLs used to perform pagination.
     * @param integer $page the zero-based page number that the URL should point to.
     * @param boolean $absolute whether to create an absolute URL. Defaults to `false`.
     * @return string the created URL
     * @see params
     * @see forcePageParam
     */
    public function createUrl($page, $absolute = false)
    {
        if (!isset($this->route))
        {
            $query_params = $this->request_service->getQueryParams();
            $query_params[$this->pageParam] = $page;
            $query_string = '?' . http_build_query($query_params);
            $url = $this->request_service->getPathInfo() . $query_string;
        }
        else
        {
            // use pretty url
            $url = str_replace('$page', $page, $this->route);
            if($page == 1)
			{
				$url = dirname($url) . '/';
			}
        }
        if ($absolute)
        {
            $url_service = cg::app()->get_url_service();
            $url = $url_service->createAbsoluteUrl($url);
        }

        return $url;
    }

	/**
	 * Paginate the list of items and prepare pager components to be use in View.
	 * generate default pagination style
	 *
	 * @param bool $absolute
	 * @return array An array of pager component, access via <strong>pages, jump_menu, page_size, current_page, total_page, next_link, prev_link</strong>
	 */
	public function paginate($absolute = false)
	{
		$links = $this->getLinks();

		$this->totalPage = $links['page_count'];
		$current_page = $links['current_page'];

		// check whether prev link is needed
		if (isset($links['prev']) && $current_page != 1)
		{
			$page_url = $links['prev'];
			$first_page_url = $links['first'];
			$this->output = <<<PAGE_ITEM
			<li class="page-item">
  <a class="page-link" href="{$first_page_url}" aria-label="Previous">
	<span>首页</span>
  </a>
</li>
<li class="page-item">
  <a class="page-link" href="{$page_url}" aria-label="Previous">
	<span aria-hidden="true">{$this->prevText}</span>
  </a>
</li>
PAGE_ITEM;
		}

		if ($this->totalPage > $this->maxLength)
		{
			$midRange = $this->maxLength - 2;

			$start_range = $current_page - floor($midRange / 2);
			$end_range = $current_page + floor($midRange / 2);

			if ($start_range <= 0)
			{
				$end_range += abs($start_range) + 1;
				$start_range = 1;
			}

			if ($end_range > $this->totalPage)
			{
				$start_range -= $end_range - $this->totalPage;
				$end_range = $this->totalPage;
			}

			while ($end_range - $start_range + 1 < $this->maxLength - 1)
			{
				$end_range++;
			}

			$modulus = (int)$this->maxLength % 2 == 0;
			$center = floor($this->maxLength / 2);

			if ($current_page > $center)
			{
				$end_range--;
			}

			if ($modulus == 0 && $this->totalPage - $current_page + 1 <= $center)
			{
				while ($end_range - $start_range + 1 < $this->maxLength - 1)
				{
					$start_range--;
				}
			}
			$range = range($start_range, $end_range);

			for ($i = 1; $i <= $this->totalPage; $i++)
			{
				// loop through all pages. if first, last, or in range, display
				if ($i == 1 || $i == $this->totalPage || in_array($i, $range))
				{
					$lastDot = '';
					if ($modulus == 1)
					{
						// if maxLength is not divisible by 2
						if ($i == $this->totalPage && $current_page < ($this->totalPage - ($this->maxLength - $center - $modulus)))
						{
							$lastDot = '... ';
						}
					}
					else
					{
						// not show ... when current_page <= (totalPage - maxLength / 2)
						if ($i == $this->totalPage && $current_page <= ($this->totalPage - ($this->maxLength - $center)))
						{
							$lastDot = '... ';
						}
					}

					$prevDot = '';
					if ($range[0] > 2 && $i == 1)
					{
						$prevDot = " ...";
					}

					$this->output .= $this->generate_page_item($i, $current_page, $absolute, $prevDot, $lastDot);
				}
			}
		}
		else
		{
			if ($this->totalPage == 1)
			{
				return;
			}
			for ($i = 1; $i <= $this->totalPage; $i++)
			{
				$this->output .= $this->generate_page_item($i, $current_page, $absolute);
			}
		}

		// check whether next link is needed
		if (isset($links['next']) && $current_page != $this->totalPage)
		{
			$page_url = $links['next'];
			$last_page_url = $links['last'];
			$this->output .= <<<PAGE_ITEM

<li class="page-item">
  <a class="page-link" href="{$page_url}" aria-label="Next">
	<span aria-hidden="true">{$this->nextText}</span>
  </a>
</li>
<li class="page-item">
  <a class="page-link" href="{$last_page_url}" aria-label="Next">
	<span aria-hidden="true">末页</span>
  </a>
</li>
PAGE_ITEM;
		}
	}
	
	private function generate_page_item($i, $current_page, $absolute, $prevDot = '', $lastDot = '')
	{
		$is_active = '';
		$page_url = 'javascript:void(0);';
		if ($i == $current_page)
		{
			$is_active = ' active';
		}
		else
		{
			$page_url = $this->createUrl($i, $absolute);
		}
		$output = <<<PAGE_ITEM
<li class="page-item{$is_active}">
  <a class="page-link" href="{$page_url}">{$lastDot}{$i}{$prevDot}</a>
</li>
PAGE_ITEM;
		
		return $output;
	}
}
