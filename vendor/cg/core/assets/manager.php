<?php
namespace cg\core\assets;

use cg\core\assets\resource\resource_base;
use cg\core\cg;
use Exception;

/**
 * port from Phalcon\Assets\Manager
 *
 * Manages collections of CSS/Javascript assets
 * ```php
 * // These Javascripts are located in the page's bottom
 * $js = $manager->collection('jsFooter');
 *
 * // This a remote resource that does not need filtering
 * $js->addJs('code.jquery.com/jquery-1.10.0.min.js', false, false);
 *
 * // These are local resources that must be filtered
 * $js->addJs('common-functions.js');
 * $js->addJs('page-functions.js');
 * ```
 */
class manager
{
	use collection_trait;
	/**
	 * @var collection[]
	 */
	private $_collections;
	/**
	 * @var array inline scripts
	 */
	private $inlineJs = [];
	private $url_service;

	/**
	 *
	 */
	public function __construct()
	{
		$this->url_service = cg::app()->get_url_service();
		$this->project_name = cg::config()->APP_NAME;
	}

	/**
	 * @param string $js The JS code block to be added
	 */
	public function addInlineJs($js)
	{
		$this->inlineJs[] = $js;
	}

	/**
	 * Sets a collection in the Assets Manager
	 *
	 *<code>
	 * $assets->set('js', $collection);
	 *</code>
	 * @param string $id
	 * @param $collection
	 * @return $this
	 */
	public function set(string $id, $collection)
	{
		$this->_collections[$id] = $collection;
		return $this;
	}

	/**
	 * Returns a collection by its id
	 *
	 *<code>
	 * $scripts = $assets->get('js');
	 *</code>
	 * @param string $id
	 * @return collection
	 * @throws Exception
	 */
	public function get(string $id)
	{
		if (!isset($this->_collections[$id]))
		{
			throw new Exception("The collection does not exist in the manager");
		}

		return $this->_collections[$id];
	}

	/**
	 * Creates/Returns a collection of resources
	 * @param string $name
	 * @return collection
	 */
	public function collection(string $name)
	{
		if (isset($this->_collections[$name]))
		{
			$collection = $this->_collections[$name];
		}
		else
		{
			$collection = new collection();
			$this->_collections[$name] = $collection;
		}

		return $collection;
	}

	/**
	 * Traverses a collection calling the callback to generate its HTML
	 *
	 * @param array $resources
	 * @param callable $callback
	 */
	private function output($resources, $callback)
	{
		/**
		 * walk in resources
		 * @var resource_base $resource
		 */
		foreach ($resources as $resource)
		{
			/**
			 * Is the resource local?
			 */
			$local = $resource->getLocal();

			$path = $resource->getPath();
			// resource url
			$url = $path;

			// if is local resource
			if ($local)
			{
				$local_path = cg::config()->APP_ROOT . 'web' . $this->url_service->getLocalStatic($path);
				if (($timestamp = @filemtime($local_path)) > 0)
				{
					$url .= "?v=$timestamp";
				}
				$url = $this->url_service->getStaticUrl($url);
			}

			/**
			 * Gets extra HTML attributes in the resource
			 */
			$attributes = $resource->getAttributes();

			/**
			 * Prepare the parameters for the callback
			 */
			$parameters = [
				$url,
				$attributes
			];

			/**
			 * Call the callback to generate the HTML
			 */
			$html = call_user_func_array($callback, $parameters);

			echo $html;
		}
	}

	/**
	 * Prints the HTML for CSS resources
	 *
	 * @param string $collectionName
	 * @return mixed
	 */
	public function outputCss($collectionName = null)
	{
		if ($collectionName === null)
		{
			$resources = $this->_resources['css'];
		}
		else
		{
			$resources = $this->get($collectionName)->getResources()['css'];
		}
		$this->output($resources, [
			'cg\core\helpers\html',
			'cssFile'
		]);
	}

	/**
	 * Prints the HTML for JS resources
	 *
	 * @param string $collectionName
	 * @return mixed
	 */
	public function outputJs($collectionName = null)
	{
		if ($collectionName === null)
		{
			$resources = $this->_resources['js'];
		}
		else
		{
			$resources = $this->get($collectionName)->getResources()['js'];
		}

		$this->output($resources, [
			'cg\core\helpers\html',
			'jsFile'
		]);
	}

	/**
	 * output all inline scripts
	 */
	public function outputInlineJs()
	{
		if (!empty($this->inlineJs))
		{
			echo '<script type="text/javascript">' . implode(PHP_EOL, $this->inlineJs) . '</script>' . PHP_EOL;
		}
	}
}
