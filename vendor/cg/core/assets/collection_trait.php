<?php
namespace cg\core\assets;

use cg\core\assets\resource\resource_css;
use cg\core\assets\resource\resource_js;
use cg\core\assets\resource\resource_trait;

trait collection_trait
{
	use resource_trait;

	private $_resources = [];
	protected $project_name;

	/**
	 * Adds a CSS resource to the collection
	 * @param string $path
	 * @param bool $local
	 * @param null|array $attributes
	 * @return $this
	 */
	public function addCss(string $path, $local = true, $attributes = null)
	{
		$path = $this->project_name . '/css/' . $path;
		$this->_resources['css'][] = new resource_css($path, $local, $this->getCollectionAttributes($attributes));

		return $this;
	}

	/**
	 * Adds a common CSS resource to the collection
	 * @param string $path
	 * @param bool $local
	 * @param null|array $attributes
	 * @return $this
	 */
	public function addCommonCss(string $path, $local = true, $attributes = null)
	{
		$path = 'common/css/' . $path;
		$this->_resources['css'][] = new resource_css($path, $local, $this->getCollectionAttributes($attributes));

		return $this;
	}

	/**
	 * Adds a javascript resource to the collection
	 *
	 * @param string $path
	 * @param bool $local
	 * @param array $attributes
	 * @return $this
	 */
	public function addJs(string $path, $local = true, $attributes = null)
	{
		$path = $this->project_name . '/js/' . $path;
		$this->_resources['js'][] = new resource_js($path, $local, $this->getCollectionAttributes($attributes));

		return $this;
	}

	/**
	 * Adds a common javascript resource to the collection
	 *
	 * @param string $path
	 * @param bool $local
	 * @param array $attributes
	 * @return $this
	 */
	public function addCommonJs(string $path, $local = true, $attributes = null)
	{
		$path = 'common/js/' . $path;
		$this->_resources['js'][] = new resource_js($path, $local, $this->getCollectionAttributes($attributes));

		return $this;
	}

	private function getCollectionAttributes($attributes)
	{
		if (!empty($attributes))
		{
			$collectionAttributes = $attributes;
		}
		else
		{
			$collectionAttributes = $this->_attributes;
		}

		return $collectionAttributes;
	}
}
