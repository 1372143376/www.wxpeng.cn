<?php
namespace cg\core\assets;

use cg\core\cg;

class collection
{
	use collection_trait;

	public function __construct()
	{
		$this->project_name = cg::config()->APP_NAME;
	}

	public function getResources()
	{
		return $this->_resources;
	}
}