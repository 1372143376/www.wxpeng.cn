<?php
namespace cg\core\assets\resource;

/**
 * Phalcon\Assets\Resource\Js
 *
 * Represents Javascript resources
 */
class resource_js extends resource_base
{
	/**
	 * resource_js constructor.
	 * @param string $path
	 * @param bool $local
	 * @param array $attributes
	 */
	public function __construct(string $path, bool $local = true, $attributes = [])
	{
		parent::__construct('js', $path, $local, $attributes);
	}
}