<?php
namespace cg\core\assets\resource;

/**
 * Phalcon\Assets\Resource\Css
 *
 * Represents CSS resources
 */
class resource_css extends resource_base
{
	/**
	 * Phalcon\Assets\Resource\Css
	 * @param string $path
	 * @param bool $local
	 * @param array $attributes
	 */
	public function __construct(string $path, bool $local = true, $attributes = [])
	{
		parent::__construct('css', $path, $local, $attributes);
	}
}