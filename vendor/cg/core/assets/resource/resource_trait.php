<?php
namespace cg\core\assets\resource;

trait resource_trait
{
	/**
	 * @var array
	 */
	private $_attributes;

	public function getAttributes()
	{
		return $this->_attributes;
	}

	/**
	 * Sets extra HTML attributes
	 * @param array $attributes
	 * @return $this
	 */
	public function setAttributes(array $attributes)
	{
		$this->_attributes = $attributes;
		return $this;
	}
}