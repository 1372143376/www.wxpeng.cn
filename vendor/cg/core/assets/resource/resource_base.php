<?php
namespace cg\core\assets\resource;

class resource_base
{
	use resource_trait;
	
	/**
	 * @var string
	 */
	protected $_type;

	/**
	 * @var string
	 */
	protected $_path;

	/**
	 * @var bool
	 */
	protected $_local;

	/**
	 * Phalcon\Assets\Resource constructor
	 * @param string $type
	 * @param string $path
	 * @param bool $local
	 * @param array $attributes
	 */
	public function __construct(string $type, string $path, bool $local = true, $attributes = [])
	{
		$this->_type = $type;
		$this->_path = $path;
		$this->_local = $local;
		$this->_attributes = $attributes;
	}

	public function getType()
	{
		return $this->_type;
	}
	
	public function getPath()
	{
		return $this->_path;
	}

	public function getLocal()
	{
		return $this->_local;
	}
}