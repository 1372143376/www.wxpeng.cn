<?php
namespace cg\core;

//use Predis\Client;
class cg_controller
{
	public $cookie;
	public $params;
	public $extension;
	public $autoroute = TRUE;
	public $routematch;

	//redis缓冲
	public $redis_cache;
	//memcache缓冲
	public $memcache_cache;

	public $template_engine, $template_dir;
	protected $controller_name, $action_name, $module_name;
	public $timestamp, $ip, $datetime;
	public $is_windows = false;
	protected $request_service;

	public function __construct()
	{
		if (DIRECTORY_SEPARATOR == '\\')
		{
			$this->is_windows = true;
		}
		$this->request_service = cg::app()->get_request_service();		
		$this->cookie = &$_COOKIE;

		$this->memcache_cache = cg::cache('memcache');
		//$this->redis_cache = new Client();

		$this->timestamp = time();
		$this->datetime = date('Y-m-d H:i:s', $this->timestamp);
		$this->ip = $this->get_ip();
	}

	/**
	 * This will be called before the actual action is executed
	 */
	public function beforeRun($resource, $action, $module_name = '')
	{
		$this->controller_name = $resource;
		$this->action_name = $action;
		$this->module_name = $module_name;
	}


	/**
	 * This will be called if the action method returns null or success status(200 to 299 not including 204) after the actual action is executed
	 * @param mixed $routeResult The result returned by an action
	 */
	public function afterRun($routeResult)
	{
	}

	/**
	 * @return View
	 */
	public function view()
	{
		return cg::view($this->template_engine, $this->template_dir);
	}

	public function lang($words, $args = '')
	{
		return $this->view()->lang($words, $args);
	}

	public function get_ip()
	{
		$ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
		$ips = explode(',', $ip);
		if (!empty($ips))
		{
			$ip = $ips[0];
		}

		return $ip;
	}
}
