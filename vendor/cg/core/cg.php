<?php
namespace cg\core;

use cg\core\caching\cache;
use cg\core\caching\cache_memcache;
use cg\core\caching\cache_memcached;
use cg\core\db\connection;
use League\Container\Container;
use MongoDB\Client;

class cg
{
	private static $_app;
	private static $_config;
	private static $_db;
	private static $_cache;
	private static $_router;
	private static $_view;
	/**
	 * @var Container
	 */
	public static $container;

	/**
	 * @return cg_config
	 */
	public static function config()
	{
		if (self::$_config === NULL)
		{
			self::$_config = new cg_config();
		}
		return self::$_config;
	}

	/**
	 * @return cg_router
	 */
	public static function router()
	{
		if (self::$_router === null)
		{
			self::$_router = new cg_router();
		}
		return self::$_router;
	}

	/**
	 * @return cg_app
	 */
	public static function app()
	{
		if (self::$_app === null)
		{
			self::$container = new Container();
			self::$_app = new cg_app();
		}
		return self::$_app;
	}

	/**
	 * @param $db_config_name
	 * @return connection
	 */
	public static function db($db_config_name)
	{
		$db_config = self::config()->config['db'][$db_config_name];
		if (!isset(self::$_db[$db_config_name]))
		{
			self::$_db[$db_config_name] = new connection($db_config);
		}
		return self::$_db[$db_config_name];
	}
	
	public static function get_db_connections()
	{
		return self::$_db;
	}

	/**
	 * @param string $db_config_name 数据库配置
	 * @return Client
	 */
	public static function connect_mongodb($db_config_name)
	{
		$db_config = self::config()->config['db'][$db_config_name];
		if (!isset(self::$_db[$db_config_name]))
		{
			self::$_db[$db_config_name] = new Client($db_config['server'], $db_config['options']);
		}
		return self::$_db[$db_config_name];
	}

	/**
	 * @param string $cache_type
	 * @return cache
	 */
	public static function cache($cache_type = '')
	{
		if (empty($cache_type))
		{
			$cache_type = self::config()->config['cache']['cache_type'];
		}
		if ($cache_type == 'memcached')
		{
			if (isset(self::$_cache['memcached']))
			{
				return self::$_cache['memcached'];
			}
			self::$_cache['memcached'] = new cache_memcached(self::config()->config['cache']['memcache']); //note: config array key is memcache
			return self::$_cache['memcached'];
		}
		else //默认memcache
		{
			if (isset(self::$_cache['memcache']))
			{
				return self::$_cache['memcache'];
			}
			self::$_cache['memcache'] = new cache_memcache(self::config()->config['cache']['memcache']);
			return self::$_cache['memcache'];
		}
	}

	/**
	 * @param string $template_engine
	 * @param string $template_dir
	 * @return View
	 */
	public static function view($template_engine = 'php', $template_dir = '')
	{
		if (isset(self::$_view['php']))
		{
			return self::$_view['php'];
		}
		self::$_view['php'] = new View($template_dir);

		return self::$_view['php'];
	}
}
