<?php
namespace cg\core;

use RuntimeException;

class View
{
	/**
	 * The name of the template layout.
	 * @var string
	 */
	private $layout = '';
	private $template_dir = '';
	private $lang;
	private $default_lang;
	private $lang_data = array();
	/**
	 * Collection of preassigned template data.
	 * @var array Data
	 */
	private $data;
	private $content_name;

	public function __construct($template_dir = '')
	{
		$this->get_lang();
		$this->load_lang_file('lang.php');
		$this->template_dir = $template_dir;
	}

	public function set_layout_name($layout)
	{
		$this->layout = 'layouts/' . $layout;
	}
	
	public function get_content_name()
	{
		return $this->content_name;
	}

	public function get_data()
	{
		return $this->data;
	}
	
	public function get_lang_data($words)
	{
		return $this->lang_data[$words];
	}

	public function render($template_name, array $data = [])
	{
		$this->data = $data;
		$this->content_name = $template_name;

		return $this->make($template_name)->render($this->layout);
	}

	/**
	 * Create a new template.
	 * @param string $template
	 * @return Template
	 */
	public function make( $template)
	{
		$path = $this->get_view_dir() . $template . '.php';
		if (file_exists($path))
		{
			return new Template($this, $path);
		}
		else
		{
			throw new RuntimeException("Not found $template template in $path");
		}
	}
	
	private function get_view_dir()
	{
		return cg::config()->APP_PATH . 'view/' . $this->template_dir . '/';
	}

	public function lang($words, $args = '')
	{
		if (!isset($this->lang_data[$words]))
		{
			return $words;
		}
		$args = func_get_args();
		$words = array_shift($args);
		return vsprintf($this->lang_data[$words], $args);
	}

	public function load_lang_file($filename)
	{
		$lang_file = cg::config()->APP_ROOT . 'lang/' . $this->lang . '/' . $filename;
		if (!file_exists($lang_file))
		{
			$lang_file = cg::config()->APP_ROOT . 'lang/' . $this->default_lang . '/' . $filename;
		}
		if (!file_exists($lang_file))
		{
			//die('lang file :' . $lang_file . ' not exists');
		}
		$this->lang_data += include $lang_file;
	}

	public function get_template_file($filename)
	{
		$file = cg::config()->APP_PATH . 'view/' . $this->template_dir . '/' . $filename;
		return $file;
	}

	private function get_lang()
	{
		$lang_config = cg::config()->config['lang'];
		$this->lang = $lang_config['default'];
		$this->default_lang = $this->lang;
		//return; //@todo

		if (!$lang_config['multi_lang'])
		{
			if (empty($this->lang))
			{
				$this->lang = 'cn';
			}
			return;
		}

		$all_langs = $lang_config['all'];

		if (isset($_GET['lang']))
		{
			if (in_array($_GET['lang'], array_keys($all_langs)))
			{
				setcookie('lang', $_GET['lang'], time() + 86400 * 30, '/');
				$this->lang = $_GET['lang'];
			}
		}
		elseif (isset($_COOKIE['lang']))
		{
			if (in_array($_COOKIE['lang'], array_keys($all_langs)))
			{
				$this->lang = $_COOKIE['lang'];
			}
		}
		else
		{
			$lang = array_search($this->language(), $all_langs);
			if ($lang !== false)
			{
				$this->lang = $lang;
				setcookie('lang', $lang, time() + 86400 * 30, '/');
			}
		}
	}

	/**
	 * @return string  zh-cn,en-us
	 */
	private function language()
	{
		$langcode = (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '';
		$langcode = (!empty($langcode)) ? explode(';', $langcode) : $langcode;
		$langcode = (!empty($langcode[0])) ? explode(',', $langcode[0]) : $langcode;
		return $langcode[0];
	}
}
