<?php
namespace cg\core;

use cg\core\db\connection;
use cg\core\db\query;
use cg\core\helpers\json;
use cg\core\validators\validator;
use cg\core\web\form_field;
use Exception;
use ReflectionClass;

class cg_model
{
	/**
	 * The name of the default scenario.
	 */
	const SCENARIO_DEFAULT = 'default';

	protected $db_config_name = '';
	protected $table_prefix = '';
	protected $table = '';
	protected $pk = 'id';
	/**
	 * @var array validation errors (attribute name => array of errors)
	 */
	private $_errors;
	/**
	 * @var array list of validators
	 */
	private $_validators;
	/**
	 * @var string current scenario
	 */
	private $_scenario = self::SCENARIO_DEFAULT;
	/**
	 * @var string current form name
	 */
	private $_form_name;
	/**
	 * @var array validation result, include style code and message
	 */
	private $validation_result;
	/**
	 * @var form_field[]
	 */
	private $form_fields = [];
	private $error_css_class = 'has-danger';
	private $success_css_class = 'has-success';

	public function table($table)
	{
		return $this->table_prefix . $table;
	}

	/**
	 * @param string $db_config_name
	 * @return Connection
	 */
	final public function db($db_config_name = '')
	{
		if (empty($db_config_name))
		{
			return cg::db($this->db_config_name);
		}
		else
		{
			return cg::db($db_config_name);
		}
	}

	/**
	 * @return query
	 */
	public function get_query()
	{
		$query = new query($this->db());
		return $query->from($this->table);
	}

	final public function find($pk_id, $use_cache = false, $cache_time = 3600)
	{
		$pk_id = intval($pk_id);

		$table = $this->table;
		$pk = $this->pk;
		$_find = function (connection $db) use ($table, $pk, $pk_id)
		{
			$sql = "select * from $table where $pk = '$pk_id'";
			return $db->createCommand($sql)->queryOne();
		};

		if ($use_cache)
		{
			return $this->db()->cache($_find, $cache_time);
		}
		else
		{
			return $this->db()->noCache($_find);
		}
	}

	final public function insert($arr_fields)
	{
		$this->db()->createCommand()->insert($this->table, $arr_fields)->execute();

		return $this->db()->getLastInsertID();
	}

	final public function update($arr_fields, $pk_id, $update_cache = true)
	{
		if (empty($arr_fields))
		{
			return 0;
		}
		$result = $this->db()
			->createCommand()
			->update(
				$this->table,
				$arr_fields,
				"$this->pk=$pk_id"
			)
			->execute();

		if ($update_cache)
		{
			// @todo
		}
		return $result;
	}

	final public function delete($pk_id)
	{
		$pk_id = intval($pk_id);
		$sql = "delete from $this->table where `$this->pk`=:pk_id";

		return $this->db()->createCommand($sql, [
			':pk_id' => $pk_id
		])->execute();
	}

	/**
	 * Returns the validation rules for attributes.
	 *
	 * Validation rules are used by [[validate()]] to check if attribute values are valid.
	 * Child classes may override this method to declare different validation rules.
	 *
	 * Each rule is an array with the following structure:
	 *
	 * ~~~
	 * [
	 *     ['attribute1', 'attribute2'],
	 *     'validator type',
	 *     'on' => ['scenario1', 'scenario2'],
	 *     ...other parameters...
	 * ]
	 * ~~~
	 *
	 * where
	 *
	 *  - attribute list: required, specifies the attributes array to be validated, for single attribute you can pass string;
	 *  - validator type: required, specifies the validator to be used. It can be a built-in validator name,
	 *    a method name of the model class, an anonymous function, or a validator class name.
	 *  - on: optional, specifies the [[scenario|scenarios]] array when the validation
	 *    rule can be applied. If this option is not set, the rule will apply to all scenarios.
	 *  - additional name-value pairs can be specified to initialize the corresponding validator properties.
	 *    Please refer to individual validator class API for possible properties.
	 *
	 * A validator can be either an object of a class extending [[Validator]], or a model class method
	 * (called *inline validator*) that has the following signature:
	 *
	 * ~~~
	 * // $params refers to validation parameters given in the rule
	 * function validatorName($attribute, $params)
	 * ~~~
	 *
	 * In the above `$attribute` refers to currently validated attribute name while `$params` contains an array of
	 * validator configuration options such as `max` in case of `string` validator. Currently validate attribute value
	 * can be accessed as `$this->[$attribute]`.
	 *
	 * Yii also provides a set of [[Validator::builtInValidators|built-in validators]].
	 * They each has an alias name which can be used when specifying a validation rule.
	 *
	 * Below are some examples:
	 *
	 * ~~~
	 * [
	 *     // built-in "required" validator
	 *     [['username', 'password'], 'required'],
	 *     // built-in "string" validator customized with "min" and "max" properties
	 *     ['username', 'string', 'min' => 3, 'max' => 12],
	 *     // built-in "compare" validator that is used in "register" scenario only
	 *     ['password', 'compare', 'compareAttribute' => 'password2', 'on' => 'register'],
	 *     // an inline validator defined via the "authenticate()" method in the model class
	 *     ['password', 'authenticate', 'on' => 'login'],
	 *     // a validator of class "DateRangeValidator"
	 *     ['dateRange', 'DateRangeValidator'],
	 * ];
	 * ~~~
	 *
	 * Note, in order to inherit rules defined in the parent class, a child class needs to
	 * merge the parent rules with child rules using functions such as `array_merge()`.
	 *
	 * @return array validation rules
	 * @see scenarios()
	 */
	public function rules()
	{
		return [];
	}

	/**
	 * Populates the model with the data from end user.
	 *
	 * assignments only be done to the safe attributes
	 * A safe attribute is one that is associated with a validation rule in the current [[scenario]].
	 *
	 * @param array $data the data array. This is usually `$_POST` or `$_GET`, but can also be any valid array
	 * supplied by end user.
	 * @param string $form_name
	 * @return bool
	 */
	public function load($data, $form_name = null)
	{
		$this->_form_name = $form_name === null ? $this->form_name() : $form_name;

		// enable ajax validation
		$assets = cg::app()->get_assets_manager();
		$assets->collection('footer')
			->addCommonJs('cg.js')
			->addCommonJs('cg-validation.js')
			->addCommonJs('cg-form.js');
		$client_attributes = [];
		foreach ($this->safeAttributes() as $safeAttribute)
		{
			$this->form_fields[$safeAttribute] = new form_field($this, $safeAttribute);
			$client_attributes[] = $this->form_fields[$safeAttribute]->getClientOptions();
		}
		$attributes = json::html_encode($client_attributes);
		$options = json_encode([
			// the container CSS class representing the corresponding attribute has validation error
			'errorCssClass' => $this->error_css_class,
			// the container CSS class representing the corresponding attribute passes validation
			'successCssClass' => $this->success_css_class
		]);
		$inline_js = <<<INLINE_JS
jQuery('#{$this->_form_name}').yiiActiveForm($attributes, $options);
INLINE_JS;
		$assets->addInlineJs($inline_js);
		$attributes = $this->safeAttributes();
		foreach ($attributes as $name)
		{
			$container_style = $this->form_fields[$name]->get_container_style();
			$this->validation_result[$name] = [
				'style' => " $container_style",
				'message' => ''
			];
		}
		
		if (empty($data))
		{
			// non submit status
			return false;
		}
		else
		{
			$attributes = array_flip($attributes);
			foreach ($data as $name => $value)
			{
				if (isset($attributes[$name]))
				{
					$this->$name = $value;
				}
			}
			return true;
		}
	}

	/**
	 * Returns the form name that this model class should use.
	 *
	 * The form name is mainly used by [[\yii\widgets\ActiveForm]] to determine how to name
	 * the input fields for the attributes in a model. If the form name is "A" and an attribute
	 * name is "b", then the corresponding input name would be "A[b]". If the form name is
	 * an empty string, then the input name would be "b".
	 *
	 * By default, this method returns the model class name (without the namespace part)
	 * as the form name. You may override it when the model is used in different forms.
	 *
	 * @return string the form name of this model class.
	 */
	private function form_name()
	{
		$reflector = new ReflectionClass($this);

		return $reflector->getShortName() . 'form';
	}
	
	public function get_form_name()
	{
		return $this->_form_name;
	}
	
	/**
	 * Returns the attribute names that are safe to be massively assigned in the current scenario.
	 * @return string[] safe attribute names
	 */
	public function safeAttributes()
	{
		$scenario = $this->getScenario();
		$scenarios = $this->scenarios();
		if (!isset($scenarios[$scenario]))
		{
			return [];
		}
		$attributes = [];
		foreach ($scenarios[$scenario] as $attribute)
		{
			if ($attribute[0] !== '!')
			{
				$attributes[] = $attribute;
			}
		}

		return $attributes;
	}
	
	public function getAttribute($attribute)
	{
		return isset($this->$attribute) ? $this->$attribute : '';
	}

	/**
	 * Returns the attribute labels.
	 *
	 * Attribute labels are mainly used for display purpose. For example, given an attribute
	 * `firstName`, we can declare a label `First Name` which is more user-friendly and can
	 * be displayed to end users.
	 *
	 * Note, in order to inherit labels defined in the parent class, a child class needs to
	 * merge the parent labels with child labels using functions such as `array_merge()`.
	 *
	 * @return array attribute labels (name => label)
	 */
	public function attributeLabels()
	{
		return [];
	}

	/**
	 * Returns the text label for the specified attribute.
	 * @param string $attribute the attribute name
	 * @return string the attribute label
	 * @see generateAttributeLabel()
	 * @see attributeLabels()
	 */
	public function getAttributeLabel($attribute)
	{
		$labels = $this->attributeLabels();
		return isset($labels[$attribute]) ? $labels[$attribute] : $attribute;
	}

	/**
	 * Returns all the validators declared in [[rules()]].
	 *
	 * This method differs from [[getActiveValidators()]] in that the latter
	 * only returns the validators applicable to the current [[scenario]].
	 *
	 * Because this method returns an ArrayObject object, you may
	 * manipulate it by inserting or removing validators (useful in model behaviors).
	 * For example,
	 *
	 * ~~~
	 * $model->validators[] = $newValidator;
	 * ~~~
	 *
	 * @return validator[] all the validators declared in the model.
	 */
	public function getValidators()
	{
		if ($this->_validators === null)
		{
			$this->_validators = $this->createValidators();
		}
		return $this->_validators;
	}

	/**
	 * Creates validator objects based on the validation rules specified in [[rules()]].
	 * Unlike [[getValidators()]], each time this method is called, a new list of validators will be returned.
	 * @return array validators
	 * @throws Exception if any validation rule configuration is invalid
	 */
	public function createValidators()
	{
		$validators = [];
		foreach ($this->rules() as $rule)
		{
			if ($rule instanceof validator)
			{
				$validators[] = $rule;
			}
			elseif (is_array($rule) && isset($rule[0], $rule[1]))
			{ // attributes, validator type
				$validator = validator::createValidator($rule[1], $this, (array)$rule[0], array_slice($rule, 2));
				$validators[] = $validator;
			}
			else
			{
				throw new Exception('Invalid validation rule: a rule must specify both attribute names and validator type.');
			}
		}
		return $validators;
	}

	/**
	 * Returns a list of scenarios and the corresponding active attributes.
	 * An active attribute is one that is subject to validation in the current scenario.
	 * The returned array should be in the following format:
	 *
	 * ```php
	 * [
	 *     'scenario1' => ['attribute11', 'attribute12', ...],
	 *     'scenario2' => ['attribute21', 'attribute22', ...],
	 *     ...
	 * ]
	 * ```
	 *
	 * By default, an active attribute is considered safe and can be massively assigned.
	 * If an attribute should NOT be massively assigned (thus considered unsafe),
	 * please prefix the attribute with an exclamation character (e.g. `'!rank'`).
	 *
	 * The default implementation of this method will return all scenarios found in the [[rules()]]
	 * declaration. A special scenario named [[SCENARIO_DEFAULT]] will contain all attributes
	 * found in the [[rules()]]. Each scenario will be associated with the attributes that
	 * are being validated by the validation rules that apply to the scenario.
	 *
	 * @return array a list of scenarios and the corresponding active attributes.
	 */
	public function scenarios()
	{
		$scenarios = [self::SCENARIO_DEFAULT => []];
		foreach ($this->getValidators() as $validator)
		{
			foreach ($validator->on as $scenario)
			{
				$scenarios[$scenario] = [];
			}
			foreach ($validator->except as $scenario)
			{
				$scenarios[$scenario] = [];
			}
		}
		$names = array_keys($scenarios);

		foreach ($this->getValidators() as $validator)
		{
			if (empty($validator->on) && empty($validator->except))
			{
				foreach ($names as $name)
				{
					foreach ($validator->attributes as $attribute)
					{
						$scenarios[$name][$attribute] = true;
					}
				}
			}
			elseif (empty($validator->on))
			{
				foreach ($names as $name)
				{
					if (!in_array($name, $validator->except, true))
					{
						foreach ($validator->attributes as $attribute)
						{
							$scenarios[$name][$attribute] = true;
						}
					}
				}
			}
			else
			{
				foreach ($validator->on as $name)
				{
					foreach ($validator->attributes as $attribute)
					{
						$scenarios[$name][$attribute] = true;
					}
				}
			}
		}

		foreach ($scenarios as $scenario => $attributes)
		{
			if (!empty($attributes))
			{
				$scenarios[$scenario] = array_keys($attributes);
			}
		}

		return $scenarios;
	}

	/**
	 * Returns the validators applicable to the current [[scenario]].
	 * @param string $attribute the name of the attribute whose applicable validators should be returned.
	 * If this is null, the validators for ALL attributes in the model will be returned.
	 * @return \cg\core\validators\validator[] the validators applicable to the current [[scenario]].
	 */
	public function getActiveValidators($attribute = null)
	{
		$validators = [];
		$scenario = $this->getScenario();
		foreach ($this->getValidators() as $validator)
		{
			if ($validator->isActive($scenario) && ($attribute === null || in_array($attribute, $validator->attributes, true)))
			{
				$validators[] = $validator;
			}
		}
		return $validators;
	}

	/**
	 * Returns the scenario that this model is used in.
	 *
	 * Scenario affects how validation is performed and which attributes can
	 * be massively assigned.
	 *
	 * @return string the scenario that this model is in. Defaults to [[SCENARIO_DEFAULT]].
	 */
	public function getScenario()
	{
		return $this->_scenario;
	}

	/**
	 * Sets the scenario for the model.
	 * Note that this method does not check if the scenario exists or not.
	 * The method [[validate()]] will perform this check.
	 * @param string $value the scenario that this model is in.
	 */
	public function setScenario($value)
	{
		$this->_scenario = $value;
	}

	/**
	 * Returns the attribute names that are subject to validation in the current scenario.
	 * @return string[] safe attribute names
	 */
	public function activeAttributes()
	{
		$scenario = $this->getScenario();
		$scenarios = $this->scenarios();
		if (!isset($scenarios[$scenario]))
		{
			return [];
		}
		$attributes = $scenarios[$scenario];
		foreach ($attributes as $i => $attribute)
		{
			if ($attribute[0] === '!')
			{
				$attributes[$i] = substr($attribute, 1);
			}
		}

		return $attributes;
	}

	/**
	 * Performs the data validation.
	 *
	 * This method executes the validation rules applicable to the current [[scenario]].
	 * The following criteria are used to determine whether a rule is currently applicable:
	 *
	 * - the rule must be associated with the attributes relevant to the current scenario;
	 * - the rules must be effective for the current scenario.
	 *
	 * This method will call [[beforeValidate()]] and [[afterValidate()]] before and
	 * after the actual validation, respectively. If [[beforeValidate()]] returns false,
	 * the validation will be cancelled and [[afterValidate()]] will not be called.
	 *
	 * Errors found during the validation can be retrieved via [[getErrors()]],
	 * [[getFirstErrors()]] and [[getFirstError()]].
	 *
	 * @param array $attributeNames list of attribute names that should be validated.
	 * If this parameter is empty, it means any attribute listed in the applicable
	 * validation rules should be validated.
	 * @param boolean $clearErrors whether to call [[clearErrors()]] before performing validation
	 * @return bool whether the validation is successful without any error.
	 * @throws Exception
	 */
	public function validate($attributeNames = null, $clearErrors = true)
	{
		if ($clearErrors)
		{
			$this->clearErrors();
		}

		$scenarios = $this->scenarios();
		$scenario = $this->getScenario();
		if (!isset($scenarios[$scenario]))
		{
			throw new Exception("Unknown scenario: $scenario");
		}

		if ($attributeNames === null)
		{
			$attributeNames = $this->activeAttributes();
		}

		foreach ($this->getActiveValidators() as $validator)
		{
			$validator->validateAttributes($this, $attributeNames);
		}

		// generate validation result for view
		foreach ($attributeNames as $attributeName)
		{
			$container_style = $this->form_fields[$attributeName]->get_container_style();
			if (isset($this->_errors[$attributeName]))
			{
				$this->validation_result[$attributeName] = [
					'style' => " $container_style $this->error_css_class",
					'message' => $this->_errors[$attributeName][0]
				];
			}
			else
			{
				$this->validation_result[$attributeName] = [
					'style' => " $container_style $this->success_css_class",
					'message' => ''
				];
			}
		}

		return !$this->hasErrors();
	}

	public function validation_style($attribute)
	{
		return $this->validation_result[$attribute]['style'];
	}
	
	public function validation_message($attribute)
	{
		return $this->validation_result[$attribute]['message'];
	}

	/**
	 * Removes errors for all attributes or a single attribute.
	 * @param string $attribute attribute name. Use null to remove errors for all attribute.
	 */
	public function clearErrors($attribute = null)
	{
		if ($attribute === null)
		{
			$this->_errors = [];
		}
		else
		{
			unset($this->_errors[$attribute]);
		}
	}

	/**
	 * Returns a value indicating whether there is any validation error.
	 * @param string|null $attribute attribute name. Use null to check all attributes.
	 * @return boolean whether there is any error.
	 */
	public function hasErrors($attribute = null)
	{
		return $attribute === null ? !empty($this->_errors) : isset($this->_errors[$attribute]);
	}

	/**
	 * Returns the errors for all attribute or a single attribute.
	 * @param string $attribute attribute name. Use null to retrieve errors for all attributes.
	 * @property array An array of errors for all attributes. Empty array is returned if no error.
	 * The result is a two-dimensional array. See [[getErrors()]] for detailed description.
	 * @return array errors for all attributes or the specified attribute. Empty array is returned if no error.
	 * Note that when returning errors for all attributes, the result is a two-dimensional array, like the following:
	 *
	 * ~~~
	 * [
	 *     'username' => [
	 *         'Username is required.',
	 *         'Username must contain only word characters.',
	 *     ],
	 *     'email' => [
	 *         'Email address is invalid.',
	 *     ]
	 * ]
	 * ~~~
	 *
	 * @see getFirstError()
	 */
	public function getErrors($attribute = null)
	{
		if ($attribute === null)
		{
			return $this->_errors === null ? [] : $this->_errors;
		}
		else
		{
			return isset($this->_errors[$attribute]) ? $this->_errors[$attribute] : [];
		}
	}

	/**
	 * Adds a new error to the specified attribute.
	 * @param string $attribute attribute name
	 * @param string $error new error message
	 */
	public function addError($attribute, $error = '')
	{
		$this->_errors[$attribute][] = $error;
	}
}
