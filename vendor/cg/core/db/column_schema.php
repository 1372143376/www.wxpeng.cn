<?php
namespace cg\core\db;

/**
 * ColumnSchema class describes the metadata of a column in a database table.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class column_schema
{
	/**
	 * @var string name of this column (without quotes).
	 */
	public $name;
	/**
	 * @var boolean whether this column can be null.
	 */
	public $allowNull;
	/**
	 * @var string the DB type of this column. Possible DB types vary according to the type of DBMS.
	 */
	public $dbType;
	/**
	 * @var mixed default value of this column
	 */
	public $defaultValue;
	/**
	 * @var array enumerable values. This is set only if the column is declared to be an enumerable type.
	 */
	public $enumValues;
	/**
	 * @var integer display size of the column.
	 */
	public $size;
	/**
	 * @var integer precision of the column data, if it is numeric.
	 */
	public $precision;
	/**
	 * @var integer scale of the column data, if it is numeric.
	 */
	public $scale;
	/**
	 * @var boolean whether this column is a primary key
	 */
	public $isPrimaryKey;
	/**
	 * @var boolean whether this column is auto-incremental
	 */
	public $autoIncrement = false;
	/**
	 * @var boolean whether this column is unsigned. This is only meaningful
	 * when [[type]] is `smallint`, `integer` or `bigint`.
	 */
	public $unsigned;
	/**
	 * @var string comment of this column. Not all DBMS support this.
	 */
	public $comment;
}
