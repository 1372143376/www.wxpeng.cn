<?php
namespace cg\core\db\mysql;
use cg\core\db\connection;
use Exception;

/**
 * QueryBuilder is the query builder for MySQL databases.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class query_builder extends \cg\core\db\query_builder
{
	/**
	 * Constructor.
	 * @param connection $connection the database connection.
	 */
	public function __construct($connection)
	{
		$this->db = $connection;
	}
	
	/**
	 * Builds a SQL statement for renaming a column.
	 * @param string $table the table whose column is to be renamed. The name will be properly quoted by the method.
	 * @param string $oldName the old name of the column. The name will be properly quoted by the method.
	 * @param string $newName the new name of the column. The name will be properly quoted by the method.
	 * @return string the SQL statement for renaming a DB column.
	 * @throws Exception
	 */
	public function renameColumn($table, $oldName, $newName)
	{
		$quotedTable = $this->db->quoteTableName($table);
		$row = $this->db->createCommand('SHOW CREATE TABLE ' . $quotedTable)->queryOne();
		if ($row === false) {
			throw new Exception("Unable to find column '$oldName' in table '$table'.");
		}
		if (isset($row['Create Table'])) {
			$sql = $row['Create Table'];
		} else {
			$row = array_values($row);
			$sql = $row[1];
		}
		if (preg_match_all('/^\s*`(.*?)`\s+(.*?),?$/m', $sql, $matches)) {
			foreach ($matches[1] as $i => $c) {
				if ($c === $oldName) {
					return "ALTER TABLE $quotedTable CHANGE "
					. $this->db->quoteColumnName($oldName) . ' '
					. $this->db->quoteColumnName($newName) . ' '
					. $matches[2][$i];
				}
			}
		}
		// try to give back a SQL anyway
		return "ALTER TABLE $quotedTable CHANGE "
		. $this->db->quoteColumnName($oldName) . ' '
		. $this->db->quoteColumnName($newName);
	}

	/**
	 * Creates a SQL statement for resetting the sequence value of a table's primary key.
	 * The sequence will be reset such that the primary key of the next new row inserted
	 * will have the specified value or 1.
	 * @param string $tableName the name of the table whose primary key sequence will be reset
	 * @param mixed $value the value for the primary key of the next new row inserted. If this is not set,
	 * the next new row's primary key will have a value 1.
	 * @return string the SQL statement for resetting sequence
	 * @throws InvalidParamException if the table does not exist or there is no sequence associated with the table.
	 */
	public function resetSequence($tableName, $value = null)
	{
		$table = $this->db->getTableSchema($tableName);
		if ($table !== null && $table->sequenceName !== null) {
			$tableName = $this->db->quoteTableName($tableName);
			if ($value === null) {
				$key = reset($table->primaryKey);
				$value = $this->db->createCommand("SELECT MAX(`$key`) FROM $tableName")->queryScalar() + 1;
			} else {
				$value = (int) $value;
			}

			return "ALTER TABLE $tableName AUTO_INCREMENT=$value";
		} elseif ($table === null) {
			throw new InvalidParamException("Table not found: $tableName");
		} else {
			throw new InvalidParamException("There is no sequence associated with table '$tableName'.");
		}
	}

	/**
	 * Builds a SQL statement for enabling or disabling integrity check.
	 * @param boolean $check whether to turn on or off the integrity check.
	 * @param string $table the table name. Meaningless for MySQL.
	 * @param string $schema the schema of the tables. Meaningless for MySQL.
	 * @return string the SQL statement for checking integrity
	 */
	public function checkIntegrity($check = true, $schema = '', $table = '')
	{
		return 'SET FOREIGN_KEY_CHECKS = ' . ($check ? 1 : 0);
	}

	/**
	 * @inheritdoc
	 */
	public function buildLimit($limit, $offset)
	{
		$sql = '';
		if ($this->hasLimit($limit)) {
			$sql = 'LIMIT ' . $limit;
			if ($this->hasOffset($offset)) {
				$sql .= ' OFFSET ' . $offset;
			}
		} elseif ($this->hasOffset($offset)) {
			// limit is not optional in MySQL
			// http://stackoverflow.com/a/271650/1106908
			// http://dev.mysql.com/doc/refman/5.0/en/select.html#idm47619502796240
			$sql = "LIMIT $offset, 18446744073709551615"; // 2^64-1
		}

		return $sql;
	}
}
