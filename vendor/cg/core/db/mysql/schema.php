<?php
namespace cg\core\db\mysql;
use cg\core\db\column_schema;
use cg\core\db\expression;
use cg\core\db\table_schema;
use Exception;

/**
 * Schema is the class for retrieving metadata from a MySQL database (version 4.1.x and 5.x).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class schema extends \cg\core\db\schema
{
	/**
	 * Quotes a table name for use in a query.
	 * A simple table name has no schema prefix.
	 * @param string $name table name
	 * @return string the properly quoted table name
	 */
	public function quoteSimpleTableName($name)
	{
		return strpos($name, "`") !== false ? $name : "`" . $name . "`";
	}

	/**
	 * Quotes a column name for use in a query.
	 * A simple column name has no prefix.
	 * @param string $name column name
	 * @return string the properly quoted column name
	 */
	public function quoteSimpleColumnName($name)
	{
		return strpos($name, '`') !== false || $name === '*' ? $name : '`' . $name . '`';
	}

	/**
	 * Creates a query builder for the MySQL database.
	 * @return query_builder query builder instance
	 */
	public function createQueryBuilder()
	{
		return new query_builder($this->db);
	}

	/**
	 * Loads the metadata for the specified table.
	 * @param string $name table name
	 * @return table_schema driver dependent table metadata. Null if the table does not exist.
	 */
	protected function loadTableSchema($name)
	{
		$table = new table_schema();
		$this->resolveTableNames($table, $name);

		if ($this->findColumns($table)) {
			$this->findConstraints($table);

			return $table;
		} else {
			return null;
		}
	}

	/**
	 * Resolves the table name and schema name (if any).
	 * @param table_schema $table the table metadata object
	 * @param string $name the table name
	 */
	protected function resolveTableNames($table, $name)
	{
		$parts = explode('.', str_replace('`', '', $name));
		if (isset($parts[1])) {
			$table->schemaName = $parts[0];
			$table->name = $parts[1];
			$table->fullName = $table->schemaName . '.' . $table->name;
		} else {
			$table->fullName = $table->name = $parts[0];
		}
	}

	/**
	 * Loads the column information into a [[ColumnSchema]] object.
	 * @param array $info column information
	 * @return column_schema the column schema object
	 */
	protected function loadColumnSchema($info)
	{
		$column = $this->createColumnSchema();

		$column->name = $info['field'];
		$column->allowNull = $info['null'] === 'YES';
		$column->isPrimaryKey = strpos($info['key'], 'PRI') !== false;
		$column->autoIncrement = stripos($info['extra'], 'auto_increment') !== false;
		$column->comment = $info['comment'];

		$column->dbType = $info['type'];
		$column->unsigned = stripos($column->dbType, 'unsigned') !== false;

		if (preg_match('/^(\w+)(?:\(([^\)]+)\))?/', $column->dbType, $matches)) {
			$type = strtolower($matches[1]);
			if (!empty($matches[2])) {
				if ($type === 'enum') {
					$values = explode(',', $matches[2]);
					foreach ($values as $i => $value) {
						$values[$i] = trim($value, "'");
					}
					$column->enumValues = $values;
				} else {
					$values = explode(',', $matches[2]);
					$column->size = $column->precision = (int) $values[0];
					if (isset($values[1])) {
						$column->scale = (int) $values[1];
					}
				}
			}
		}

		if (!$column->isPrimaryKey) {
			if ($column->dbType === 'timestamp' && $info['default'] === 'CURRENT_TIMESTAMP') {
				$column->defaultValue = new Expression('CURRENT_TIMESTAMP');
			} elseif (isset($type) && $type === 'bit') {
				$column->defaultValue = bindec(trim($info['default'],'b\''));
			} else {
				$column->defaultValue = $info['default'];
			}
		}

		return $column;
	}

	/**
	 * Collects the metadata of table columns.
	 * @param table_schema $table the table metadata
	 * @return boolean whether the table exists in the database
	 * @throws Exception if DB query fails
	 */
	protected function findColumns($table)
	{
		$sql = 'SHOW FULL COLUMNS FROM ' . $this->quoteTableName($table->fullName);
		try {
			$columns = $this->db->createCommand($sql)->queryAll();
		} catch (Exception $e) {
			$previous = $e->getPrevious();
			if ($previous instanceof \PDOException && strpos($previous->getMessage(), 'SQLSTATE[42S02') !== false) {
				// table does not exist
				// https://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html#error_er_bad_table_error
				return false;
			}
			throw $e;
		}
		foreach ($columns as $info) {
			if ($this->db->getSlavePdo()->getAttribute(\PDO::ATTR_CASE) !== \PDO::CASE_LOWER) {
				$info = array_change_key_case($info, CASE_LOWER);
			}
			$column = $this->loadColumnSchema($info);
			$table->columns[$column->name] = $column;
			if ($column->isPrimaryKey) {
				$table->primaryKey[] = $column->name;
				if ($column->autoIncrement) {
					$table->sequenceName = '';
				}
			}
		}

		return true;
	}

	/**
	 * Gets the CREATE TABLE sql string.
	 * @param table_schema $table the table metadata
	 * @return string $sql the result of 'SHOW CREATE TABLE'
	 */
	protected function getCreateTableSql($table)
	{
		$row = $this->db->createCommand('SHOW CREATE TABLE ' . $this->quoteTableName($table->fullName))->queryOne();
		if (isset($row['Create Table'])) {
			$sql = $row['Create Table'];
		} else {
			$row = array_values($row);
			$sql = $row[1];
		}

		return $sql;
	}

	/**
	 * Collects the foreign key column details for the given table.
	 * @param table_schema $table the table metadata
	 */
	protected function findConstraints($table)
	{
		$sql = $this->getCreateTableSql($table);

		$regexp = '/FOREIGN KEY\s+\(([^\)]+)\)\s+REFERENCES\s+([^\(^\s]+)\s*\(([^\)]+)\)/mi';
		if (preg_match_all($regexp, $sql, $matches, PREG_SET_ORDER)) {
			foreach ($matches as $match) {
				$fks = array_map('trim', explode(',', str_replace('`', '', $match[1])));
				$pks = array_map('trim', explode(',', str_replace('`', '', $match[3])));
				$constraint = [str_replace('`', '', $match[2])];
				foreach ($fks as $k => $name) {
					$constraint[$name] = $pks[$k];
				}
				$table->foreignKeys[] = $constraint;
			}
		}
	}

	/**
	 * Returns all unique indexes for the given table.
	 * Each array element is of the following structure:
	 *
	 * ~~~
	 * [
	 *  'IndexName1' => ['col1' [, ...]],
	 *  'IndexName2' => ['col2' [, ...]],
	 * ]
	 * ~~~
	 *
	 * @param TableSchema $table the table metadata
	 * @return array all unique indexes for the given table.
	 */
	public function findUniqueIndexes($table)
	{
		$sql = $this->getCreateTableSql($table);
		$uniqueIndexes = [];

		$regexp = '/UNIQUE KEY\s+([^\(\s]+)\s*\(([^\(\)]+)\)/mi';
		if (preg_match_all($regexp, $sql, $matches, PREG_SET_ORDER)) {
			foreach ($matches as $match) {
				$indexName = str_replace('`', '', $match[1]);
				$indexColumns = array_map('trim', explode(',', str_replace('`', '', $match[2])));
				$uniqueIndexes[$indexName] = $indexColumns;
			}
		}

		return $uniqueIndexes;
	}

	/**
	 * Returns all table names in the database.
	 * @param string $schema the schema of the tables. Defaults to empty string, meaning the current or default schema.
	 * @return array all table names in the database. The names have NO schema name prefix.
	 */
	protected function findTableNames($schema = '')
	{
		$sql = 'SHOW TABLES';
		if ($schema !== '') {
			$sql .= ' FROM ' . $this->quoteSimpleTableName($schema);
		}

		return $this->db->createCommand($sql)->queryColumn();
	}
}
