<?php
namespace cg\core\db;

use cg\core\caching\cache;
use DebugBar\DataCollector\PDO\TracedStatement;
use Exception;
use PDO;
use PDOException;

/**
 * Command represents a SQL statement to be executed against a database.
 *
 * A command object is usually created by calling [[Connection::createCommand()]].
 * The SQL statement it represents can be set via the [[sql]] property.
 *
 * To execute a non-query SQL (such as INSERT, DELETE, UPDATE), call [[execute()]].
 * To execute a SQL statement that returns result data set (such as SELECT),
 * use [[queryAll()]], [[queryOne()]], [[queryColumn()]], [[queryScalar()]], or [[query()]].
 * For example,
 *
 * ~~~
 * $users = $connection->createCommand('SELECT * FROM user')->queryAll();
 * ~~~
 *
 * Command supports SQL statement preparation and parameter binding.
 * Call [[bindValue()]] to bind a value to a SQL parameter;
 * Call [[bindParam()]] to bind a PHP variable to a SQL parameter.
 * When binding a parameter, the SQL statement is automatically prepared.
 * You may also call [[prepare()]] explicitly to prepare a SQL statement.
 *
 * Command also supports building SQL statements by providing methods such as [[insert()]],
 * [[update()]], etc. For example,
 *
 * ~~~
 * $connection->createCommand()->insert('user', [
 *     'name' => 'Sam',
 *     'age' => 30,
 * ])->execute();
 * ~~~
 *
 * To build SELECT SQL statements, please use [[QueryBuilder]] instead.
 *
 * @property string $rawSql The raw SQL with parameter values inserted into the corresponding placeholders in
 * [[sql]]. This property is read-only.
 * @property string $sql The SQL statement to be executed.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class command
{
	/**
	 * @var Connection the DB connection that this command is associated with
	 */
	public $db;
	/**
	 * @var \PDOStatement the PDOStatement object that this command is associated with
	 */
	public $pdoStatement;
	/**
	 * @var integer the default fetch mode for this command.
	 * @see http://www.php.net/manual/en/function.PDOStatement-setFetchMode.php
	 */
	public $fetchMode = PDO::FETCH_ASSOC;
	/**
	 * @var array the parameters (name => value) that are bound to the current PDO statement.
	 * This property is maintained by methods such as [[bindValue()]]. It is mainly provided for logging purpose
	 * and is used to generate [[rawSql]]. Do not modify it directly.
	 */
	public $params = [];
	/**
	 * @var integer the default number of seconds that query results can remain valid in cache.
	 * Use 0 to indicate that the cached data will never expire. And use a negative number to indicate
	 * query cache should not be used.
	 * @see cache()
	 */
	public $queryCacheDuration;

	/**
	 * @var array pending parameters to be bound to the current PDO statement.
	 */
	private $_pendingParams = [];
	/**
	 * @var string the SQL statement that this command represents
	 */
	private $_sql;

	public function __construct($db, $sql)
	{
		$this->db = $db;
		$this->_sql = $sql;
	}


	/**
	 * Enables query cache for this command.
	 * @param integer $duration the number of seconds that query result of this command can remain valid in the cache.
	 * If this is not set, the value of [[Connection::queryCacheDuration]] will be used instead.
	 * Use 0 to indicate that the cached data will never expire.
	 * @return static the command object itself
	 */
	public function cache($duration = null)
	{
		$this->queryCacheDuration = $duration === null ? $this->db->queryCacheDuration : $duration;
		return $this;
	}

	/**
	 * Disables query cache for this command.
	 * @return static the command object itself
	 */
	public function noCache()
	{
		$this->queryCacheDuration = -1;
		return $this;
	}

	/**
	 * Returns the SQL statement for this command.
	 * @return string the SQL statement to be executed
	 */
	public function getSql()
	{
		return $this->_sql;
	}

	/**
	 * Specifies the SQL statement to be executed.
	 * The previous SQL execution (if any) will be cancelled, and [[params]] will be cleared as well.
	 * @param string $sql the SQL statement to be set.
	 * @return static this command instance
	 */
	public function setSql($sql)
	{
		if ($sql !== $this->_sql) {
			$this->cancel();
			$this->_sql = $this->db->quoteSql($sql);
			$this->_pendingParams = [];
			$this->params = [];
		}

		return $this;
	}

	/**
	 * Returns the raw SQL by inserting parameter values into the corresponding placeholders in [[sql]].
	 * Note that the return value of this method should mainly be used for logging purpose.
	 * It is likely that this method returns an invalid SQL due to improper replacement of parameter placeholders.
	 * @return string the raw SQL with parameter values inserted into the corresponding placeholders in [[sql]].
	 */
	public function getRawSql()
	{
		if (empty($this->params)) {
			return $this->_sql;
		}
		$params = [];
		foreach ($this->params as $name => $value) {
			if (is_string($value)) {
				$params[$name] = $this->db->quoteValue($value);
			} elseif ($value === null) {
				$params[$name] = 'NULL';
			} elseif (!is_object($value) && !is_resource($value)) {
				$params[$name] = $value;
			}
		}
		if (!isset($params[1])) {
			return strtr($this->_sql, $params);
		}
		$sql = '';
		foreach (explode('?', $this->_sql) as $i => $part) {
			$sql .= (isset($params[$i]) ? $params[$i] : '') . $part;
		}

		return $sql;
	}

	/**
	 * Prepares the SQL statement to be executed.
	 * For complex SQL statement that is to be executed multiple times,
	 * this may improve performance.
	 * For SQL statement with binding parameters, this method is invoked
	 * automatically.
	 * @param boolean $forRead whether this method is called for a read query. If null, it means
	 * the SQL statement should be used to determine whether it is for read or write.
	 * @throws Exception if there is any DB error
	 */
	public function prepare($forRead = null)
	{
		if ($this->pdoStatement) {
			$this->bindPendingParams();
			return;
		}

		$sql = $this->getSql();

//		if ($this->db->getTransaction()) {
//			// master is in a transaction. use the same connection.
//			$forRead = false;
//		}
		$forRead = false;
		if ($forRead || $forRead === null && $this->isReadQuery($sql)) {
			$pdo = $this->db->getSlavePdo();
		} else {
			$pdo = $this->db->getMasterPdo();
		}

		try {
			$this->pdoStatement = $pdo->prepare($sql);
			$this->bindPendingParams();
		} catch (Exception $e) {
			$message = $e->getMessage() . "\nFailed to prepare SQL: $sql";
			$errorInfo = $e instanceof PDOException ? $e->errorInfo : null;
			throw new Exception($message, $errorInfo, (int) $e->getCode(), $e);
		}
	}

	/**
	 * Cancels the execution of the SQL statement.
	 * This method mainly sets [[pdoStatement]] to be null.
	 */
	public function cancel()
	{
		$this->pdoStatement = null;
	}

	/**
	 * Binds a parameter to the SQL statement to be executed.
	 * @param string|integer $name parameter identifier. For a prepared statement
	 * using named placeholders, this will be a parameter name of
	 * the form `:name`. For a prepared statement using question mark
	 * placeholders, this will be the 1-indexed position of the parameter.
	 * @param mixed $value Name of the PHP variable to bind to the SQL statement parameter
	 * @param integer $dataType SQL data type of the parameter. If null, the type is determined by the PHP type of the value.
	 * @param integer $length length of the data type
	 * @param mixed $driverOptions the driver-specific options
	 * @return static the current command being executed
	 * @see http://www.php.net/manual/en/function.PDOStatement-bindParam.php
	 */
	public function bindParam($name, &$value, $dataType = null, $length = null, $driverOptions = null)
	{
		$this->prepare();

		if ($dataType === null) {
			$dataType = $this->db->getSchema()->getPdoType($value);
		}
		if ($length === null) {
			$this->pdoStatement->bindParam($name, $value, $dataType);
		} elseif ($driverOptions === null) {
			$this->pdoStatement->bindParam($name, $value, $dataType, $length);
		} else {
			$this->pdoStatement->bindParam($name, $value, $dataType, $length, $driverOptions);
		}
		$this->params[$name] =& $value;

		return $this;
	}

	/**
	 * Binds pending parameters that were registered via [[bindValue()]] and [[bindValues()]].
	 * Note that this method requires an active [[pdoStatement]].
	 */
	protected function bindPendingParams()
	{
		foreach ($this->_pendingParams as $name => $value) {
			$this->pdoStatement->bindValue($name, $value[0], $value[1]);
		}
		$this->_pendingParams = [];
	}

	/**
	 * Binds a value to a parameter.
	 * @param string|integer $name Parameter identifier. For a prepared statement
	 * using named placeholders, this will be a parameter name of
	 * the form `:name`. For a prepared statement using question mark
	 * placeholders, this will be the 1-indexed position of the parameter.
	 * @param mixed $value The value to bind to the parameter
	 * @param integer $dataType SQL data type of the parameter. If null, the type is determined by the PHP type of the value.
	 * @return static the current command being executed
	 * @see http://www.php.net/manual/en/function.PDOStatement-bindValue.php
	 */
	public function bindValue($name, $value, $dataType = null)
	{
		if ($dataType === null) {
			$dataType = $this->db->getSchema()->getPdoType($value);
		}
		$this->_pendingParams[$name] = [$value, $dataType];
		$this->params[$name] = $value;

		return $this;
	}

	/**
	 * Binds a list of values to the corresponding parameters.
	 * This is similar to [[bindValue()]] except that it binds multiple values at a time.
	 * Note that the SQL data type of each value is determined by its PHP type.
	 * @param array $values the values to be bound. This must be given in terms of an associative
	 * array with array keys being the parameter names, and array values the corresponding parameter values,
	 * e.g. `[':name' => 'John', ':age' => 25]`. By default, the PDO type of each value is determined
	 * by its PHP type. You may explicitly specify the PDO type by using an array: `[value, type]`,
	 * e.g. `[':name' => 'John', ':profile' => [$profile, \PDO::PARAM_LOB]]`.
	 * @return static the current command being executed
	 */
	public function bindValues($values)
	{
		if (empty($values)) {
			return $this;
		}

		foreach ($values as $name => $value) {
			if (is_array($value)) {
				$this->_pendingParams[$name] = $value;
				$this->params[$name] = $value[0];
			} else {
				$type = $this->getPdoType($value);
				$this->_pendingParams[$name] = [$value, $type];
				$this->params[$name] = $value;
			}
		}

		return $this;
	}

	/**
	 * Determines the PDO type for the given PHP data value.
	 * @param mixed $data the data whose PDO type is to be determined
	 * @return integer the PDO type
	 * @see http://www.php.net/manual/en/pdo.constants.php
	 */
	public function getPdoType($data)
	{
		static $typeMap = [
			// php type => PDO type
			'boolean' => \PDO::PARAM_BOOL,
			'integer' => \PDO::PARAM_INT,
			'string' => \PDO::PARAM_STR,
			'resource' => \PDO::PARAM_LOB,
			'NULL' => \PDO::PARAM_NULL,
		];
		$type = gettype($data);

		return isset($typeMap[$type]) ? $typeMap[$type] : \PDO::PARAM_STR;
	}

	/**
	 * Executes the SQL statement and returns query result.
	 * This method is for executing a SQL query that returns result set, such as `SELECT`.
	 * @return \PDOStatement the reader object for fetching the query result
	 * @throws Exception execution failed
	 */
	public function query()
	{
		return $this->queryInternal('');
	}

	/**
	 * Executes the SQL statement and returns ALL rows at once.
	 * @param integer $fetchMode the result fetch mode. Please refer to [PHP manual](http://www.php.net/manual/en/function.PDOStatement-setFetchMode.php)
	 * for valid fetch modes. If this parameter is null, the value set in [[fetchMode]] will be used.
	 * @return array all rows of the query result. Each array element is an array representing a row of data.
	 * An empty array is returned if the query results in nothing.
	 * @throws Exception execution failed
	 */
	public function queryAll($fetchMode = null)
	{
		return $this->queryInternal('fetchAll', $fetchMode);
	}

	/**
	 * Executes the SQL statement and returns the first row of the result.
	 * This method is best used when only the first row of result is needed for a query.
	 * @param integer $fetchMode the result fetch mode. Please refer to [PHP manual](http://www.php.net/manual/en/function.PDOStatement-setFetchMode.php)
	 * for valid fetch modes. If this parameter is null, the value set in [[fetchMode]] will be used.
	 * @return array|boolean the first row (in terms of an array) of the query result. False is returned if the query
	 * results in nothing.
	 * @throws Exception execution failed
	 */
	public function queryOne($fetchMode = null)
	{
		return $this->queryInternal('fetch', $fetchMode);
	}

	/**
	 * Executes the SQL statement and returns the value of the first column in the first row of data.
	 * This method is best used when only a single value is needed for a query.
	 * @return string|null|boolean the value of the first column in the first row of the query result.
	 * False is returned if there is no value.
	 * @throws Exception execution failed
	 */
	public function queryScalar()
	{
		$result = $this->queryInternal('fetchColumn', 0);
		if (is_resource($result) && get_resource_type($result) === 'stream') {
			return stream_get_contents($result);
		} else {
			return $result;
		}
	}

	/**
	 * Executes the SQL statement and returns the first column of the result.
	 * This method is best used when only the first column of result (i.e. the first element in each row)
	 * is needed for a query.
	 * @return array the first column of the query result. Empty array is returned if the query results in nothing.
	 * @throws Exception execution failed
	 */
	public function queryColumn()
	{
		return $this->queryInternal('fetchAll', PDO::FETCH_COLUMN);
	}

	/**
	 * Creates an INSERT command.
	 * For example,
	 *
	 * ~~~
	 * $connection->createCommand()->insert('user', [
	 *     'name' => 'Sam',
	 *     'age' => 30,
	 * ])->execute();
	 * ~~~
	 *
	 * The method will properly escape the column names, and bind the values to be inserted.
	 *
	 * Note that the created command is not executed until [[execute()]] is called.
	 *
	 * @param string $table the table that new rows will be inserted into.
	 * @param array $columns the column data (name => value) to be inserted into the table.
	 * @return Command the command object itself
	 */
	public function insert($table, $columns)
	{
		$params = [];
		$sql = $this->db->getQueryBuilder()->insert($table, $columns, $params);

		return $this->setSql($sql)->bindValues($params);
	}

	/**
	 * Creates a batch INSERT command.
	 * For example,
	 *
	 * ~~~
	 * $connection->createCommand()->batchInsert('user', ['name', 'age'], [
	 *     ['Tom', 30],
	 *     ['Jane', 20],
	 *     ['Linda', 25],
	 * ])->execute();
	 * ~~~
	 *
	 * The method will properly escape the column names, and quote the values to be inserted.
	 *
	 * Note that the values in each row must match the corresponding column names.
	 *
	 * Also note that the created command is not executed until [[execute()]] is called.
	 *
	 * @param string $table the table that new rows will be inserted into.
	 * @param array $columns the column names
	 * @param array $rows the rows to be batch inserted into the table
	 * @return Command the command object itself
	 */
	public function batchInsert($table, $columns, $rows)
	{
		$sql = $this->db->getQueryBuilder()->batchInsert($table, $columns, $rows);

		return $this->setSql($sql);
	}

	/**
	 * Creates an UPDATE command.
	 * For example,
	 *
	 * ~~~
	 * $connection->createCommand()->update('user', ['status' => 1], 'age > 30')->execute();
	 * ~~~
	 *
	 * The method will properly escape the column names and bind the values to be updated.
	 *
	 * Note that the created command is not executed until [[execute()]] is called.
	 *
	 * @param string $table the table to be updated.
	 * @param array $columns the column data (name => value) to be updated.
	 * @param string|array $condition the condition that will be put in the WHERE part. Please
	 * refer to [[Query::where()]] on how to specify condition.
	 * @param array $params the parameters to be bound to the command
	 * @return Command the command object itself
	 */
	public function update($table, $columns, $condition = '', $params = [])
	{
		$sql = $this->db->getQueryBuilder()->update($table, $columns, $condition, $params);

		return $this->setSql($sql)->bindValues($params);
	}

	/**
	 * Creates a DELETE command.
	 * For example,
	 *
	 * ~~~
	 * $connection->createCommand()->delete('user', 'status = 0')->execute();
	 * ~~~
	 *
	 * The method will properly escape the table and column names.
	 *
	 * Note that the created command is not executed until [[execute()]] is called.
	 *
	 * @param string $table the table where the data will be deleted from.
	 * @param string|array $condition the condition that will be put in the WHERE part. Please
	 * refer to [[Query::where()]] on how to specify condition.
	 * @param array $params the parameters to be bound to the command
	 * @return Command the command object itself
	 */
	public function delete($table, $condition = '', $params = [])
	{
		$sql = $this->db->getQueryBuilder()->delete($table, $condition, $params);

		return $this->setSql($sql)->bindValues($params);
	}

	/**
	 * Executes the SQL statement.
	 * This method should only be used for executing non-query SQL statement, such as `INSERT`, `DELETE`, `UPDATE` SQLs.
	 * No result set will be returned.
	 * @return integer number of rows affected by the execution.
	 * @throws Exception execution failed
	 */
	public function execute()
	{
		$sql = $this->getSql();

		$rawSql = $this->getRawSql();

//		Yii::info($rawSql, __METHOD__);

		if ($sql == '') {
			return 0;
		}

		$this->prepare(false);

		$token = $rawSql;
		try {
//			Yii::beginProfile($token, __METHOD__);

			$this->pdoStatement->execute();
			$n = $this->pdoStatement->rowCount();

//			Yii::endProfile($token, __METHOD__);

			return $n;
		} catch (Exception $e) {
//			Yii::endProfile($token, __METHOD__);
			throw new Exception($e->getMessage() . ' rawSql:' . $rawSql);
		}
	}

	/**
	 * Performs the actual DB query of a SQL statement.
	 * @param string $method method of PDOStatement to be called
	 * @param integer $fetchMode the result fetch mode. Please refer to [PHP manual](http://www.php.net/manual/en/function.PDOStatement-setFetchMode.php)
	 * for valid fetch modes. If this parameter is null, the value set in [[fetchMode]] will be used.
	 * @return mixed the method execution result
	 * @throws Exception if the query causes any problem
	 */
	protected function queryInternal($method, $fetchMode = null)
	{
		$rawSql = $this->getRawSql();

//		Yii::info($rawSql, 'yii\db\Command::query');

		if ($method !== '') {
			$info = $this->db->getQueryCacheInfo($this->queryCacheDuration, null);
			if (is_array($info)) {
				/** @var cache $cache */
				$cache = $info[0];
				$cacheKey = [
					__CLASS__,
					$method,
					$fetchMode,
					$this->db->dsn,
					$this->db->username,
					$rawSql,
				];
				$result = $cache->get($cacheKey);
				if (is_array($result) && isset($result[0])) {
//					Yii::trace('Query result served from cache', 'yii\db\Command::query');
					return $result[0];
				}
			}
		}

		// PHP Debug Bar start Collecting data
		$trace = new TracedStatement($rawSql);
		$trace->start();
		
		$this->prepare(true);

		$ex = null;
		try {
			$this->pdoStatement->execute();

			if ($method === '') {
				return $this->pdoStatement;
			} else {
				if ($fetchMode === null) {
					$fetchMode = $this->fetchMode;
				}
				$result = call_user_func_array([$this->pdoStatement, $method], (array) $fetchMode);
				$this->pdoStatement->closeCursor();
			}
		} catch (Exception $e) {
			$ex = $e;
			throw new Exception($e->getMessage() . ' rawSql:' . $rawSql);
		}

		$trace->end($ex, $this->pdoStatement->rowCount());
		// PHP Debug Bar finish collecting data
		$this->db->addExecutedStatement($trace);

		if (isset($cache, $cacheKey, $info)) {
			$cache->set($cacheKey, [$result], $info[1], $info[2]);
//			Yii::trace('Saved query result in cache', 'yii\db\Command::query');
		}

		return $result;
	}

	/**
	 * Returns a value indicating whether a SQL statement is for read purpose.
	 * @param string $sql the SQL statement
	 * @return boolean whether a SQL statement is for read purpose.
	 */
	public function isReadQuery($sql)
	{
		$pattern = '/^\s*(SELECT)\b/i';
		return preg_match($pattern, $sql) > 0;
	}
}
