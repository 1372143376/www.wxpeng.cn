<?php
namespace cg\core\db;

use DebugBar\DataCollector\AssetProvider;
use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\PDO\TracedStatement;
use DebugBar\DataCollector\Renderable;
use DebugBar\DataCollector\TimeDataCollector;

class db_collector extends DataCollector implements Renderable, AssetProvider
{
	private $connections;
	
	public function __construct($connections)
	{
		$this->connections = $connections;
	}

	/**
	 * Returns an array with the following keys:
	 *  - base_path
	 *  - base_url
	 *  - css: an array of filenames
	 *  - js: an array of filenames
	 *
	 * @return array
	 */
	public function getAssets()
	{
		return array(
			'css' => 'widgets/sqlqueries/widget.css',
			'js' => 'widgets/sqlqueries/widget.js'
		);
	}

	/**
	 * Called by the DebugBar when data needs to be collected
	 *
	 * @return array Collected data
	 */
	public function collect()
	{
		$data = array(
			'nb_statements' => 0,
			'nb_failed_statements' => 0,
			'accumulated_duration' => 0,
			'memory_usage' => 0,
			'peak_memory_usage' => 0,
			'statements' => array()
		);

		foreach ($this->connections as $name => $connection) {
			$pdo_data = $this->collectPDO($connection, null);
			$data['nb_statements'] += $pdo_data['nb_statements'];
			$data['nb_failed_statements'] += $pdo_data['nb_failed_statements'];
			$data['accumulated_duration'] += $pdo_data['accumulated_duration'];
			$data['memory_usage'] += $pdo_data['memory_usage'];
			$data['peak_memory_usage'] = max($data['peak_memory_usage'], $pdo_data['peak_memory_usage']);
			$data['statements'] = array_merge($data['statements'],
				array_map(function ($s) use ($name) { $s['connection'] = $name; return $s; }, $pdo_data['statements']));
		}

		$data['accumulated_duration_str'] = $this->getDataFormatter()->formatDuration($data['accumulated_duration']);
		$data['memory_usage_str'] = $this->getDataFormatter()->formatBytes($data['memory_usage']);
		$data['peak_memory_usage_str'] = $this->getDataFormatter()->formatBytes($data['peak_memory_usage']);
		
		return $data;
	}

	/**
	 * Collects data from a single TraceablePDO instance
	 *
	 * @param Connection $connection
	 * @param TimeDataCollector $timeCollector
	 * @return array
	 */
	private function collectPDO(Connection $connection, TimeDataCollector $timeCollector = null)
	{
		$stmts = array();
		/**
		 * @var TracedStatement $stmt
		 */
		foreach ($connection->getExecutedStatements() as $stmt) {
			$stmts[] = array(
				'sql' => $stmt->getSql(),
				'row_count' => $stmt->getRowCount(),
				'stmt_id' => $stmt->getPreparedId(),
				'prepared_stmt' => $stmt->getSql(),
				'params' => (object) $stmt->getParameters(),
				'duration' => $stmt->getDuration(),
				'duration_str' => $this->getDataFormatter()->formatDuration($stmt->getDuration()),
				'memory' => $stmt->getMemoryUsage(),
				'memory_str' => $this->getDataFormatter()->formatBytes($stmt->getMemoryUsage()),
				'end_memory' => $stmt->getEndMemory(),
				'end_memory_str' => $this->getDataFormatter()->formatBytes($stmt->getEndMemory()),
				'is_success' => $stmt->isSuccess(),
				'error_code' => $stmt->getErrorCode(),
				'error_message' => $stmt->getErrorMessage()
			);
			if ($timeCollector !== null) {
				$timeCollector->addMeasure($stmt->getSql(), $stmt->getStartTime(), $stmt->getEndTime());
			}
		}

		return array(
			'nb_statements' => count($stmts),
			'nb_failed_statements' => count($connection->getFailedExecutedStatements()),
			'accumulated_duration' => $connection->getAccumulatedStatementsDuration(),
			'accumulated_duration_str' => $this->getDataFormatter()->formatDuration($connection->getAccumulatedStatementsDuration()),
			'memory_usage' => $connection->getMemoryUsage(),
			'memory_usage_str' => $this->getDataFormatter()->formatBytes($connection->getPeakMemoryUsage()),
			'peak_memory_usage' => $connection->getPeakMemoryUsage(),
			'peak_memory_usage_str' => $this->getDataFormatter()->formatBytes($connection->getPeakMemoryUsage()),
			'statements' => $stmts
		);
	}

	/**
	 * Returns the unique name of the collector
	 *
	 * @return string
	 */
	public function getName()
	{
		return 'pdo';
	}

	/**
	 * Returns a hash where keys are control names and their values
	 * an array of options as defined in {@see DebugBar\JavascriptRenderer::addControl()}
	 *
	 * @return array
	 */
	public function getWidgets()
	{
		return array(
			"database" => array(
				"icon" => "inbox",
				"widget" => "PhpDebugBar.Widgets.SQLQueriesWidget",
				"map" => "pdo",
				"default" => "[]"
			),
			"database:badge" => array(
				"map" => "pdo.nb_statements",
				"default" => 0
			)
		);
	}
}