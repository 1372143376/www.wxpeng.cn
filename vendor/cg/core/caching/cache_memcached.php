<?php
namespace cg\core\caching;

use Exception;

/**
 * MemCache implements a cache application component based on [memcache](http://pecl.php.net/package/memcache)
 * and [memcached](http://pecl.php.net/package/memcached).
 *
 * MemCache supports both [memcache](http://pecl.php.net/package/memcache) and
 * [memcached](http://pecl.php.net/package/memcached). By setting [[useMemcached]] to be true or false,
 * one can let MemCache to use either memcached or memcache, respectively.
 *
 * MemCache can be configured with a list of memcache servers by settings its [[servers]] property.
 * By default, MemCache assumes there is a memcache server running on localhost at port 11211.
 *
 * See [[Cache]] for common cache operations that MemCache supports.
 *
 * Note, there is no security measure to protected data in memcache.
 * All data in memcache can be accessed by any process running in the system.
 *
 * To use MemCache as the cache application component, configure the application as follows,
 *
 * ~~~
 * [
 *     'components' => [
 *         'cache' => [
 *             'class' => 'yii\caching\MemCache',
 *             'servers' => [
 *                 [
 *                     'host' => 'server1',
 *                     'port' => 11211,
 *                     'weight' => 60,
 *                 ],
 *                 [
 *                     'host' => 'server2',
 *                     'port' => 11211,
 *                     'weight' => 40,
 *                 ],
 *             ],
 *         ],
 *     ],
 * ]
 * ~~~
 *
 * In the above, two memcache servers are used: server1 and server2. You can configure more properties of
 * each server, such as `persistent`, `weight`, `timeout`. Please see [[MemCacheServer]] for available options.
 *
 * @property \Memcache|\Memcached $memcache The memcache (or memcached) object used by this cache component.
 * This property is read-only.
 * @property cache_mem_server[] $servers List of memcache server configurations. Note that the type of this
 * property differs in getter and setter. See [[getServers()]] and [[setServers()]] for details.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class cache_memcached extends cache
{
	/**
	 * @var boolean whether to use memcached or memcache as the underlying caching extension.
	 * If true, [memcached](http://pecl.php.net/package/memcached) will be used.
	 * If false, [memcache](http://pecl.php.net/package/memcache) will be used.
	 * Defaults to false.
	 */
	public $useMemcached = false;
	/**
	 * @var string an ID that identifies a Memcached instance. This property is used only when [[useMemcached]] is true.
	 * By default the Memcached instances are destroyed at the end of the request. To create an instance that
	 * persists between requests, you may specify a unique ID for the instance. All instances created with the
	 * same ID will share the same connection.
	 * @see http://ca2.php.net/manual/en/memcached.construct.php
	 */
	public $persistentId;
	/**
	 * @var array options for Memcached. This property is used only when [[useMemcached]] is true.
	 * @see http://ca2.php.net/manual/en/memcached.setoptions.php
	 */
	public $options;
	/**
	 * @var string memcached sasl username. This property is used only when [[useMemcached]] is true.
	 * @see http://php.net/manual/en/memcached.setsaslauthdata.php
	 */
	public $username;
	/**
	 * @var string memcached sasl password. This property is used only when [[useMemcached]] is true.
	 * @see http://php.net/manual/en/memcached.setsaslauthdata.php
	 */
	public $password;

	/**
	 * @var \Memcached the Memcache instance
	 */
	private $_cache = null;
	/**
	 * @var array list of memcache server configurations
	 */
	private $_servers = [];


	/**
	 * Initializes this application component.
	 * It creates the memcache instance and adds memcache servers.
	 * @param array $config
	 * @throws Exception
	 */
	public function __construct(array $config)
	{
		$this->_servers = $config['server'];

		$this->keyPrefix = $config['key_prefix'];

		$this->addServers($this->getMemcache(), $this->_servers);
	}

	/**
	 * @param \Memcached $cache
	 * @param array $servers
	 * @throws Exception
	 */
	protected function addServers($cache, $servers)
	{
		if (empty($servers))
		{
			$servers = [
				new cache_mem_server([
					'host' => '127.0.0.1',
					'port' => 11211,
				])
			];
		}
		$this->addMemcachedServers($cache, $servers);
	}

	/**
	 * @param \Memcached $cache
	 * @param array $servers
	 */
	protected function addMemcachedServers($cache, $servers)
	{
		$existingServers = [];
		if ($this->persistentId !== null)
		{
			foreach ($cache->getServerList() as $s)
			{
				$existingServers[$s['host'] . ':' . $s['port']] = true;
			}
		}
		foreach ($servers as $server)
		{
			if (empty($existingServers) || !isset($existingServers[$server->host . ':' . $server->port]))
			{
				$cache->addServer($server->host, $server->port, $server->weight);
			}
		}
	}

	/**
	 * Returns the underlying memcache (or memcached) object.
	 * @return \Memcached the memcache (or memcached) object used by this cache component.
	 * @throws Exception if memcache or memcached extension is not loaded
	 */
	public function getMemcache()
	{
		if ($this->_cache === null)
		{
			$extension = 'memcached';
			if (!extension_loaded($extension))
			{
				throw new Exception("cache_memcached requires PHP $extension extension to be loaded.");
			}

			$this->_cache = $this->persistentId !== null ? new \Memcached($this->persistentId) : new \Memcached;
			if ($this->username !== null || $this->password !== null)
			{
				$this->_cache->setOption(\Memcached::OPT_BINARY_PROTOCOL, true);
				$this->_cache->setSaslAuthData($this->username, $this->password);
			}
			if (!empty($this->options))
			{
				$this->_cache->setOptions($this->options);
			}
		}

		return $this->_cache;
	}

	/**
	 * Returns the memcache or memcached server configurations.
	 * @return cache_mem_server[] list of memcache server configurations.
	 */
	public function getServers()
	{
		return $this->_servers;
	}

	/**
	 * @param array $config list of memcache or memcached server configurations. Each element must be an array
	 * with the following keys: host, port, persistent, weight, timeout, retryInterval, status.
	 * @see http://php.net/manual/en/memcache.addserver.php
	 * @see http://php.net/manual/en/memcached.addserver.php
	 */
	public function setServers($config)
	{
		foreach ($config as $c)
		{
			$this->_servers[] = new cache_mem_server($c);
		}
	}

	/**
	 * Retrieves a value from cache with a specified key.
	 * This is the implementation of the method declared in the parent class.
	 * @param string $key a unique key identifying the cached value
	 * @return string|boolean the value stored in cache, false if the value is not in the cache or expired.
	 */
	protected function getValue($key)
	{
		return $this->_cache->get($key);
	}

	/**
	 * Retrieves multiple values from cache with the specified keys.
	 * @param array $keys a list of keys identifying the cached values
	 * @return array a list of cached values indexed by the keys
	 */
	protected function getValues($keys)
	{
		return $this->_cache->getMulti($keys);
	}

	/**
	 * Stores a value identified by a key in cache.
	 * This is the implementation of the method declared in the parent class.
	 *
	 * @param string $key the key identifying the value to be cached
	 * @param string $value the value to be cached
	 * @param integer $duration the number of seconds in which the cached value will expire. 0 means never expire.
	 * @return boolean true if the value is successfully stored into cache, false otherwise
	 */
	protected function setValue($key, $value, $duration)
	{
		$expire = $duration > 0 ? $duration + time() : 0;

		return $this->_cache->set($key, $value, $expire);
	}

	/**
	 * Stores multiple key-value pairs in cache.
	 * @param array $data array where key corresponds to cache key while value is the value stored
	 * @param integer $duration the number of seconds in which the cached values will expire. 0 means never expire.
	 * @return array array of failed keys. Always empty in case of using memcached.
	 */
	protected function setValues($data, $duration)
	{
		$this->_cache->setMulti($data, $duration > 0 ? $duration + time() : 0);

		return [];
	}

	/**
	 * Stores a value identified by a key into cache if the cache does not contain this key.
	 * This is the implementation of the method declared in the parent class.
	 *
	 * @param string $key the key identifying the value to be cached
	 * @param string $value the value to be cached
	 * @param integer $duration the number of seconds in which the cached value will expire. 0 means never expire.
	 * @return boolean true if the value is successfully stored into cache, false otherwise
	 */
	protected function addValue($key, $value, $duration)
	{
		$expire = $duration > 0 ? $duration + time() : 0;

		return $this->_cache->add($key, $value, $expire);
	}

	/**
	 * Deletes a value with the specified key from cache
	 * This is the implementation of the method declared in the parent class.
	 * @param string $key the key of the value to be deleted
	 * @return boolean if no error happens during deletion
	 */
	protected function deleteValue($key)
	{
		return $this->_cache->delete($key, 0);
	}

	/**
	 * Deletes all values from cache.
	 * This is the implementation of the method declared in the parent class.
	 * @return boolean whether the flush operation was successful.
	 */
	protected function flushValues()
	{
		return $this->_cache->flush();
	}
}
