<?php
namespace cg\core\caching;
use Exception;

/**
 * MemCache implements a cache application component based on [memcache](http://pecl.php.net/package/memcache)
 * and [memcached](http://pecl.php.net/package/memcached).
 *
 * MemCache supports both [memcache](http://pecl.php.net/package/memcache) and
 * [memcached](http://pecl.php.net/package/memcached). By setting [[useMemcached]] to be true or false,
 * one can let MemCache to use either memcached or memcache, respectively.
 *
 * MemCache can be configured with a list of memcache servers by settings its [[servers]] property.
 * By default, MemCache assumes there is a memcache server running on localhost at port 11211.
 *
 * See [[Cache]] for common cache operations that MemCache supports.
 *
 * Note, there is no security measure to protected data in memcache.
 * All data in memcache can be accessed by any process running in the system.
 *
 * To use MemCache as the cache application component, configure the application as follows,
 *
 * ~~~
 * [
 *     'components' => [
 *         'cache' => [
 *             'class' => 'yii\caching\MemCache',
 *             'servers' => [
 *                 [
 *                     'host' => 'server1',
 *                     'port' => 11211,
 *                     'weight' => 60,
 *                 ],
 *                 [
 *                     'host' => 'server2',
 *                     'port' => 11211,
 *                     'weight' => 40,
 *                 ],
 *             ],
 *         ],
 *     ],
 * ]
 * ~~~
 *
 * In the above, two memcache servers are used: server1 and server2. You can configure more properties of
 * each server, such as `persistent`, `weight`, `timeout`. Please see [[MemCacheServer]] for available options.
 *
 * @property \Memcache|\Memcached $memcache The memcache (or memcached) object used by this cache component.
 * This property is read-only.
 * @property cache_mem_server[] $servers List of memcache server configurations. Note that the type of this
 * property differs in getter and setter. See [[getServers()]] and [[setServers()]] for details.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class cache_memcache extends cache
{
	/**
	 * @var \Memcache the Memcache instance
	 */
	private $_cache = null;
	/**
	 * @var array list of memcache server configurations
	 */
	private $_servers = [];


	/**
	 * Initializes this application component.
	 * It creates the memcache instance and adds memcache servers.
	 * @param array $config
	 * @throws Exception
	 */
	public function __construct(array $config)
	{
		$this->_servers = $config['server'];
		$this->addServers($this->getMemcache(), $this->_servers);
	}

	/**
	 * @param \Memcache|\Memcached $cache
	 * @param array $servers
	 * @throws Exception
	 */
	protected function addServers($cache, $servers)
	{
		if (empty($servers)) {
			$servers = [new cache_mem_server([
				'host' => '127.0.0.1',
				'port' => 11211,
			])];
		} else {
			foreach ($servers as $server) {
				if ($server->host === null) {
					throw new Exception("The 'host' property must be specified for every memcache server.");
				}
			}
		}
		
		$this->addMemcacheServers($cache, $servers);
	}

	/**
	 * @param \Memcache $cache
	 * @param array $servers
	 */
	protected function addMemcacheServers($cache, $servers)
	{
		$class = new \ReflectionClass($cache);
		$paramCount = $class->getMethod('addServer')->getNumberOfParameters();
		foreach ($servers as $server) {
			// $timeout is used for memcache versions that do not have $timeoutms parameter
			$timeout = (int) ($server->timeout / 1000) + (($server->timeout % 1000 > 0) ? 1 : 0);
			if ($paramCount === 9) {
				$cache->addServer(
					$server->host,
					$server->port,
					$server->persistent,
					$server->weight,
					$timeout,
					$server->retryInterval,
					$server->status,
					$server->failureCallback,
					$server->timeout
				);
			} else {
				$cache->addServer(
					$server->host,
					$server->port,
					$server->persistent,
					$server->weight,
					$timeout,
					$server->retryInterval,
					$server->status,
					$server->failureCallback
				);
			}
		}
	}

	/**
	 * Returns the underlying memcache object.
	 * @return \Memcache the memcache object used by this cache component.
	 * @throws Exception if memcache or memcached extension is not loaded
	 */
	public function getMemcache()
	{
		if ($this->_cache === null) {
			$extension = 'memcache';
			if (!extension_loaded($extension)) {
				throw new Exception("cache_memcache requires PHP $extension extension to be loaded.");
			}

			$this->_cache = new \Memcache;
		}

		return $this->_cache;
	}

	/**
	 * Returns the memcache or memcached server configurations.
	 * @return cache_mem_server[] list of memcache server configurations.
	 */
	public function getServers()
	{
		return $this->_servers;
	}

	/**
	 * @param array $config list of memcache or memcached server configurations. Each element must be an array
	 * with the following keys: host, port, persistent, weight, timeout, retryInterval, status.
	 * @see http://php.net/manual/en/memcache.addserver.php
	 * @see http://php.net/manual/en/memcached.addserver.php
	 */
	public function setServers($config)
	{
		foreach ($config as $c) {
			$this->_servers[] = new cache_mem_server($c);
		}
	}

	/**
	 * Retrieves a value from cache with a specified key.
	 * This is the implementation of the method declared in the parent class.
	 * @param string $key a unique key identifying the cached value
	 * @return string|boolean the value stored in cache, false if the value is not in the cache or expired.
	 */
	protected function getValue($key)
	{
		return $this->_cache->get($key);
	}

	/**
	 * Stores a value identified by a key in cache.
	 * This is the implementation of the method declared in the parent class.
	 *
	 * @param string $key the key identifying the value to be cached
	 * @param string $value the value to be cached
	 * @param integer $duration the number of seconds in which the cached value will expire. 0 means never expire.
	 * @return boolean true if the value is successfully stored into cache, false otherwise
	 */
	protected function setValue($key, $value, $duration)
	{
		$expire = $duration > 0 ? $duration + time() : 0;

		return $this->_cache->set($key, $value, 0, $expire);
	}

	/**
	 * Stores multiple key-value pairs in cache.
	 * @param array $data array where key corresponds to cache key while value is the value stored
	 * @param integer $duration the number of seconds in which the cached values will expire. 0 means never expire.
	 * @return array array of failed keys. Always empty in case of using memcached.
	 */
	protected function setValues($data, $duration)
	{
		return parent::setValues($data, $duration);
	}

	/**
	 * Stores a value identified by a key into cache if the cache does not contain this key.
	 * This is the implementation of the method declared in the parent class.
	 *
	 * @param string $key the key identifying the value to be cached
	 * @param string $value the value to be cached
	 * @param integer $duration the number of seconds in which the cached value will expire. 0 means never expire.
	 * @return boolean true if the value is successfully stored into cache, false otherwise
	 */
	protected function addValue($key, $value, $duration)
	{
		$expire = $duration > 0 ? $duration + time() : 0;

		return $this->_cache->add($key, $value, 0, $expire);
	}

	/**
	 * Deletes a value with the specified key from cache
	 * This is the implementation of the method declared in the parent class.
	 * @param string $key the key of the value to be deleted
	 * @return boolean if no error happens during deletion
	 */
	protected function deleteValue($key)
	{
		return $this->_cache->delete($key, 0);
	}

	/**
	 * Deletes all values from cache.
	 * This is the implementation of the method declared in the parent class.
	 * @return boolean whether the flush operation was successful.
	 */
	protected function flushValues()
	{
		return $this->_cache->flush();
	}
}
