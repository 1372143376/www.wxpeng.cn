<?php
namespace cg\core;

use LogicException;

class Template
{
	/**
	 * Instance of the template engine.
	 * @var View
	 */
	protected $engine;
	/**
	 * template path
	 * @var string
	 */
	protected $path;

	public function __construct(View $engine, $path)
	{
		$this->engine = $engine;
		$this->path = $path;
	}

	/**
	 * Render the template and layout.
	 * @param string $layout_name the name of template layout
	 * @param array $data
	 * @return string
	 */
	public function render($layout_name = '', $data = [])
	{
		try {
			extract($this->engine->get_data());
			if (!empty($data))
			{
				extract($data);
			}
			
			if (empty($layout_name))
			{
				ob_start();
				include $this->path;
				return ob_get_clean();
			}
			else
			{
				return $this->engine->make($layout_name)->render();
			}
			
		} catch (LogicException $e) {
			if (ob_get_length() > 0) {
				ob_end_clean();
			}
			throw $e;
		}
	}

	public function get_content()
	{
		return $this->engine->make($this->engine->get_content_name())->render();
	}

	/**
	 * if $data not empty, variable in $data exists only in this partial
	 * @param string $name
	 * @param array $data variable only will exists in the scope of the partial
	 */
	public function partial(string $name, array $data = [])
	{
		echo $this->engine->make($name)->render('', $data);
	}

	public function lang($words)
	{
		if ($this->engine->get_lang_data($words))
		{
			return $words;
		}
		else
		{
			return '';
		}
	}
}
