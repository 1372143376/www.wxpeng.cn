<?php
namespace cg\core\web;

use cg\core\cg;

class url
{
	private $localStaticBase = '/static/';
	/**
	 * @var string for example: http://cdn.demo.com/
	 */
	private $_cdnPrefix;
	
	/**
	 * Sets a cdn prefix for all the resources
	 * @param string $prefix
	 * @return $this
	 */
	public function setCdnPrefix(string $prefix)
	{
		$this->_cdnPrefix = $prefix;
	}

	/**
	 * return local relative path of resource
	 * @param string $path
	 * @return string
	 */
	public function getLocalStatic($path)
	{
		return $this->localStaticBase . $path;
	}

	/**
	 * return resource url
	 * return cdn url if cdnPrefix has been set
	 * @param string $url
	 * @return string
	 */
	public function getStaticUrl($url)
	{
		if ($this->_cdnPrefix)
		{
			return $this->_cdnPrefix . $url;
		}
		else
		{
			return $this->localStaticBase . $url;
		}
	}

	/**
	 * Creates an absolute URL using the given route and query parameters.
	 *
	 * @param $url
	 * @param string $scheme the scheme to use for the url (either `http` or `https`). If not specified
	 * the scheme of the current request will be used.
	 * @return string the created URL
	 */
	public function createAbsoluteUrl($url, $scheme = null)
	{
		if ($scheme != null)
		{
			$url = $scheme . '://' . $url;
		}
		else
		{
			$request_service = cg::app()->get_request_service();
			$url = $request_service->getHostInfo() . $url;
		}

		return $url;
	}
}