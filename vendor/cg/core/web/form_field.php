<?php
namespace cg\core\web;

use cg\core\cg_model;
use cg\core\validators\validator;

class form_field
{
	private $model;
	private $attribute;
	private $input_id;
	private $container_style;

	/**
	 * form_field constructor.
	 * @param cg_model $model
	 * @param string $attribute
	 */
	public function __construct($model, $attribute)
	{
		$this->input_id = $model->get_form_name() . '-' . $attribute;
		$this->container_style = 'field-' . $this->input_id;
		$this->model = $model;
		$this->attribute = $attribute;
	}

	/**
	 * Returns the JS options for the field.
	 * @return array the JS options
	 */
	public function getClientOptions()
	{
		$validators = [];
		foreach ($this->model->getActiveValidators($this->attribute) as $validator) {
			/* @var $validator validator */
			$js = $validator->clientValidateAttribute($this->model, $this->attribute, '');
			if ($validator->enableClientValidation && $js != '') {
				if ($validator->whenClient !== null) {
					$js = "if (({$validator->whenClient})(attribute, value)) { $js }";
				}
				$validators[] = $js;
			}
		}

		$container = '.' . $this->container_style;
		$options = [
			'id' => $this->input_id,
			'container' => $container,
			'input' => $container . ' .form-control',
			'error' => '.text-help',
			'name' => $this->attribute
		];

		if (!empty($validators)) {
			$options['validate'] = new js_expression("function (attribute, value, messages, deferred, \$form) {" . implode('', $validators) . '}');
		}

		// only get the options that are different from the default ones (set in yii.activeForm.js)
		return array_diff_assoc($options, [
			'validateOnChange' => true,
			'validateOnBlur' => true,
			'validateOnType' => false,
			'validationDelay' => 500,
			'encodeError' => true,
			'error' => '.help-block',
		]);
	}

	public function get_container_style()
	{
		return $this->container_style;
	}
}
