<?php
namespace cg\core\web;

/**
 * JsExpression marks a string as a JavaScript expression.
 *
 * When using [[\yii\helpers\Json::encode()]] or [[\yii\helpers\Json::htmlEncode()]] to encode a value, JsonExpression objects
 * will be specially handled and encoded as a JavaScript expression instead of a string.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class js_expression
{
	/**
	 * @var string the JavaScript expression represented by this object
	 */
	public $expression;


	/**
	 * Constructor.
	 * @param string $expression the JavaScript expression represented by this object
	 */
	public function __construct($expression)
	{
		$this->expression = $expression;
	}

	/**
	 * The PHP magic function converting an object into a string.
	 * @return string the JavaScript expression.
	 */
	public function __toString()
	{
		return $this->expression;
	}
}
