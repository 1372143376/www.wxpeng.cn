<?php
namespace cg\core\web;

/**
 * The web Request class represents an HTTP request
 *
 * It encapsulates the $_SERVER variable and resolves its inconsistency among different Web servers.
 * Also it provides an interface to retrieve request parameters from $_POST, $_GET, $_COOKIES and REST
 * parameters sent via other HTTP methods like PUT or DELETE.
 *
 * Request is configured as an application component in [[\yii\web\Application]] by default.
 * You can access that instance via `Yii::$app->request`.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class request
{
	/**
	 * The name of the HTTP header for sending CSRF token.
	 */
	const CSRF_HEADER = 'X-CSRF-Token';
	/**
	 * The length of the CSRF token mask.
	 */
	const CSRF_MASK_LENGTH = 8;

	/**
	 * @var boolean whether to enable CSRF (Cross-Site Request Forgery) validation. Defaults to true.
	 * When CSRF validation is enabled, forms submitted to an Yii Web application must be originated
	 * from the same application. If not, a 400 HTTP exception will be raised.
	 *
	 * Note, this feature requires that the user client accepts cookie. Also, to use this feature,
	 * forms submitted via POST method must contain a hidden input whose name is specified by [[csrfParam]].
	 * You may use [[\yii\helpers\Html::beginForm()]] to generate his hidden input.
	 *
	 * In JavaScript, you may get the values of [[csrfParam]] and [[csrfToken]] via `yii.getCsrfParam()` and
	 * `yii.getCsrfToken()`, respectively. The [[\yii\web\YiiAsset]] asset must be registered.
	 * You also need to include CSRF meta tags in your pages by using [[\yii\helpers\Html::csrfMetaTags()]].
	 *
	 * @see Controller::enableCsrfValidation
	 * @see http://en.wikipedia.org/wiki/Cross-site_request_forgery
	 */
	public $enableCsrfValidation = true;
	/**
	 * @var string the name of the token used to prevent CSRF. Defaults to '_csrf'.
	 * This property is used only when [[enableCsrfValidation]] is true.
	 */
	public $csrfParam = '_csrf';
	/**
	 * @var array the configuration for creating the CSRF [[Cookie|cookie]]. This property is used only when
	 * both [[enableCsrfValidation]] and [[enableCsrfCookie]] are true.
	 */
	public $csrfCookie = ['httpOnly' => true];
	/**
	 * @var boolean whether to use cookie to persist CSRF token. If false, CSRF token will be stored
	 * in session under the name of [[csrfParam]]. Note that while storing CSRF tokens in session increases
	 * security, it requires starting a session for every page, which will degrade your site performance.
	 */
	public $enableCsrfCookie = true;
	/**
	 * @var boolean whether cookies should be validated to ensure they are not tampered. Defaults to true.
	 */
	public $enableCookieValidation = true;
	/**
	 * @var string a secret key used for cookie validation. This property must be set if [[enableCookieValidation]] is true.
	 */
	public $cookieValidationKey;
	/**
	 * @var string the name of the POST parameter that is used to indicate if a request is a PUT, PATCH or DELETE
	 * request tunneled through POST. Defaults to '_method'.
	 * @see getMethod()
	 * @see getBodyParams()
	 */
	public $methodParam = '_method';
	/**
	 * @var array the parsers for converting the raw HTTP request body into [[bodyParams]].
	 * The array keys are the request `Content-Types`, and the array values are the
	 * corresponding configurations for [[Yii::createObject|creating the parser objects]].
	 * A parser must implement the [[RequestParserInterface]].
	 *
	 * To enable parsing for JSON requests you can use the [[JsonParser]] class like in the following example:
	 *
	 * ```
	 * [
	 *     'application/json' => 'yii\web\JsonParser',
	 * ]
	 * ```
	 *
	 * To register a parser for parsing all request types you can use `'*'` as the array key.
	 * This one will be used as a fallback in case no other types match.
	 *
	 * @see getBodyParams()
	 */
	public $parsers = [];

	/**
	 * @var CookieCollection Collection of request cookies.
	 */
	private $_cookies;

	/**
	 * Returns the method of the current request (e.g. GET, POST, HEAD, PUT, PATCH, DELETE).
	 * @return string request method, such as GET, POST, HEAD, PUT, PATCH, DELETE.
	 * The value returned is turned into upper case.
	 */
	public function getMethod()
	{
		return $_SERVER['REQUEST_METHOD'];
	}

	/**
	 * Returns whether this is a GET request.
	 * @return boolean whether this is a GET request.
	 */
	public function getIsGet()
	{
		return $this->getMethod() === 'GET';
	}

	/**
	 * Returns whether this is a POST request.
	 * @return boolean whether this is a POST request.
	 */
	public function getIsPost()
	{
		return $this->getMethod() === 'POST';
	}

	/**
	 * Returns whether this is an AJAX (XMLHttpRequest) request.
	 * @return boolean whether this is an AJAX (XMLHttpRequest) request.
	 */
	public function getIsAjax()
	{
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
	}

	/**
	 * Returns whether this is a PJAX request
	 * @return boolean whether this is a PJAX request
	 */
	public function getIsPjax()
	{
		return $this->getIsAjax() && !empty($_SERVER['HTTP_X_PJAX']);
	}

	/**
	 * Returns POST parameter with a given name. If name isn't specified, returns an array of all POST parameters.
	 *
	 * @param string $name the parameter name
	 * @param mixed $defaultValue the default parameter value if the parameter does not exist.
	 * @return array|mixed
	 */
	public function post($name = null, $defaultValue = null)
	{
		if ($name === null)
		{
			return $this->getBodyParams();
		}
		else
		{
			return $this->getBodyParam($name, $defaultValue);
		}
	}

	private $_bodyParams;

	/**
	 * Returns the request parameters given in the request body.
	 */
	public function getBodyParams()
	{
		if ($this->_bodyParams === null) {
			return $_POST;
		}

		return $this->_bodyParams;
	}

	/**
	 * Returns the named request body parameter value.
	 * If the parameter does not exist, the second parameter passed to this method will be returned.
	 * @param string $name the parameter name
	 * @param mixed $defaultValue the default parameter value if the parameter does not exist.
	 * @return mixed the parameter value
	 * @see getBodyParams()
	 * @see setBodyParams()
	 */
	public function getBodyParam($name, $defaultValue = null)
	{
		$params = $this->getBodyParams();

		return isset($params[$name]) ? $params[$name] : $defaultValue;
	}

	private $_queryParams;

	/**
	 * Returns the request parameters given in the [[queryString]].
	 *
	 * This method will return the contents of `$_GET` if params where not explicitly set.
	 * @return array the request GET parameter values.
	 * @see setQueryParams()
	 */
	public function getQueryParams()
	{
		if ($this->_queryParams === null)
		{
			return $_GET;
		}

		return $this->_queryParams;
	}

	/**
	 * Sets the request [[queryString]] parameters.
	 * @param array $values the request query parameters (name-value pairs)
	 * @see getQueryParam()
	 * @see getQueryParams()
	 */
	public function setQueryParams($values)
	{
		$this->_queryParams = $values;
	}

	/**
	 * Returns GET parameter with a given name. If name isn't specified, returns an array of all GET parameters.
	 *
	 * @param string $name the parameter name
	 * @param mixed $defaultValue the default parameter value if the parameter does not exist.
	 * @return array|mixed
	 */
	public function get($name = null, $defaultValue = null)
	{
		if ($name === null)
		{
			return $this->getQueryParams();
		}
		else
		{
			return $this->getQueryParam($name, $defaultValue);
		}
	}

	/**
	 * Returns the named GET parameter value.
	 * If the GET parameter does not exist, the second parameter passed to this method will be returned.
	 * @param string $name the GET parameter name.
	 * @param mixed $defaultValue the default parameter value if the GET parameter does not exist.
	 * @return mixed the GET parameter value
	 * @see getBodyParam()
	 */
	public function getQueryParam($name, $defaultValue = null)
	{
		$params = $this->getQueryParams();

		return isset($params[$name]) ? $params[$name] : $defaultValue;
	}

	private $_hostInfo;

	/**
	 * Returns the schema and host part of the current request URL.
	 * The returned URL does not have an ending slash.
	 * By default this is determined based on the user request information.
	 * You may explicitly specify it by setting the [[setHostInfo()|hostInfo]] property.
	 * @return string schema and hostname part (with port number if needed) of the request URL (e.g. `http://www.yiiframework.com`)
	 * @see setHostInfo()
	 */
	public function getHostInfo()
	{
		if ($this->_hostInfo === null)
		{
			$secure = $this->getIsSecureConnection();
			$http = $secure ? 'https' : 'http';
			if (isset($_SERVER['HTTP_HOST']))
			{
				$this->_hostInfo = $http . '://' . $_SERVER['HTTP_HOST'];
			}
			else
			{
				$this->_hostInfo = $http . '://' . $_SERVER['SERVER_NAME'];
				$port = $secure ? $this->getSecurePort() : $this->getPort();
				if (($port !== 80 && !$secure) || ($port !== 443 && $secure))
				{
					$this->_hostInfo .= ':' . $port;
				}
			}
		}

		return $this->_hostInfo;
	}

	/**
	 * Sets the schema and host part of the application URL.
	 * This setter is provided in case the schema and hostname cannot be determined
	 * on certain Web servers.
	 * @param string $value the schema and host part of the application URL. The trailing slashes will be removed.
	 */
	public function setHostInfo($value)
	{
		$this->_hostInfo = rtrim($value, '/');
	}

	private $_baseUrl;

	/**
	 * Returns the relative URL for the application.
	 * This is similar to [[scriptUrl]] except that it does not include the script file name,
	 * and the ending slashes are removed.
	 * @return string the relative URL for the application
	 * @see setScriptUrl()
	 */
	public function getBaseUrl()
	{
		if ($this->_baseUrl === null)
		{
			$this->_baseUrl = rtrim(dirname($this->getScriptUrl()), '\\/');
		}

		return $this->_baseUrl;
	}

	/**
	 * Sets the relative URL for the application.
	 * By default the URL is determined based on the entry script URL.
	 * This setter is provided in case you want to change this behavior.
	 * @param string $value the relative URL for the application
	 */
	public function setBaseUrl($value)
	{
		$this->_baseUrl = $value;
	}
	
	private $_pathInfo;

	/**
	 * Returns the path info of the currently requested URL.
	 * A path info refers to the part that is after the entry script and before the question mark (query string).
	 * The starting and ending slashes are both removed.
	 * @return string part of the request URL that is after the entry script and before the question mark.
	 * Note, the returned path info is already URL-decoded.
	 */
	public function getPathInfo()
	{
		if ($this->_pathInfo === null) {
			$this->_pathInfo = $this->resolvePathInfo();
		}

		return $this->_pathInfo;
	}

	/**
	 * Resolves the path info part of the currently requested URL.
	 * A path info refers to the part that is after the entry script and before the question mark (query string).
	 * @todo 
	 * why to remove starting slashes?
//	 * The starting slashes are both removed (ending slashes will be kept).
	 * @return string part of the request URL that is after the entry script and before the question mark.
	 * Note, the returned path info is decoded.
	 */
	protected function resolvePathInfo()
	{
		$pathInfo = $this->getUrl();

		if (($pos = strpos($pathInfo, '?')) !== false) {
			$pathInfo = substr($pathInfo, 0, $pos);
		}


//		if ($pathInfo[0] === '/') {
//			$pathInfo = substr($pathInfo, 1);
//		}

		return (string) $pathInfo;
	}
	

	private $_scriptFile;

	/**
	 * Returns the entry script file path.
	 * The default implementation will simply return `$_SERVER['SCRIPT_FILENAME']`.
	 * @return string the entry script file path
	 */
	public function getScriptFile()
	{
		return isset($this->_scriptFile) ? $this->_scriptFile : $_SERVER['SCRIPT_FILENAME'];
	}

	/**
	 * Sets the entry script file path.
	 * The entry script file path normally can be obtained from `$_SERVER['SCRIPT_FILENAME']`.
	 * If your server configuration does not return the correct value, you may configure
	 * this property to make it right.
	 * @param string $value the entry script file path.
	 */
	public function setScriptFile($value)
	{
		$this->_scriptFile = $value;
	}

	/**
	 * Returns the currently requested absolute URL.
	 * This is a shortcut to the concatenation of [[hostInfo]] and [[url]].
	 * @return string the currently requested absolute URL.
	 */
	public function getAbsoluteUrl()
	{
		return $this->getHostInfo() . $this->getUrl();
	}

	private $_url;

	/**
	 * Returns the currently requested relative URL.
	 * This refers to the portion of the URL that is after the [[hostInfo]] part.
	 * It includes the [[queryString]] part if any.
	 * @return string the currently requested relative URL. Note that the URI returned is URL-encoded.
	 */
	public function getUrl()
	{
		if ($this->_url === null)
		{
			$this->_url = $_SERVER['REQUEST_URI'];
		}

		return $this->_url;
	}

	/**
	 * Sets the currently requested relative URL.
	 * The URI must refer to the portion that is after [[hostInfo]].
	 * Note that the URI should be URL-encoded.
	 * @param string $value the request URI to be set
	 */
	public function setUrl($value)
	{
		$this->_url = $value;
	}

	/**
	 * Returns part of the request URL that is after the question mark.
	 * @return string part of the request URL that is after the question mark
	 */
	public function getQueryString()
	{
		return isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
	}

	/**
	 * Return if the request is sent via secure channel (https).
	 * @return boolean if the request is sent via secure channel (https)
	 */
	public function getIsSecureConnection()
	{
		return isset($_SERVER['HTTPS']) && (strcasecmp($_SERVER['HTTPS'], 'on') === 0 || $_SERVER['HTTPS'] == 1)
		|| isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
	}

	/**
	 * Returns the server name.
	 * @return string server name
	 */
	public function getServerName()
	{
		return $_SERVER['SERVER_NAME'];
	}

	/**
	 * Returns the server port number.
	 * @return integer server port number
	 */
	public function getServerPort()
	{
		return (int)$_SERVER['SERVER_PORT'];
	}

	/**
	 * Returns the URL referrer, null if not present
	 * @return string URL referrer, null if not present
	 */
	public function getReferrer()
	{
		return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
	}

	/**
	 * Returns the user agent, null if not present.
	 * @return string user agent, null if not present
	 */
	public function getUserAgent()
	{
		return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
	}

	/**
	 * Returns the user IP address.
	 * @return string user IP address. Null is returned if the user IP address cannot be detected.
	 */
	public function getUserIP()
	{
		return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
	}

	private $_port;

	/**
	 * Returns the port to use for insecure requests.
	 * Defaults to 80, or the port specified by the server if the current
	 * request is insecure.
	 * @return integer port number for insecure requests.
	 * @see setPort()
	 */
	public function getPort()
	{
		if ($this->_port === null)
		{
			$this->_port = !$this->getIsSecureConnection() && isset($_SERVER['SERVER_PORT']) ? (int)$_SERVER['SERVER_PORT'] : 80;
		}

		return $this->_port;
	}

	/**
	 * Sets the port to use for insecure requests.
	 * This setter is provided in case a custom port is necessary for certain
	 * server configurations.
	 * @param integer $value port number.
	 */
	public function setPort($value)
	{
		if ($value != $this->_port)
		{
			$this->_port = (int)$value;
			$this->_hostInfo = null;
		}
	}

	private $_securePort;

	/**
	 * Returns the port to use for secure requests.
	 * Defaults to 443, or the port specified by the server if the current
	 * request is secure.
	 * @return integer port number for secure requests.
	 * @see setSecurePort()
	 */
	public function getSecurePort()
	{
		if ($this->_securePort === null)
		{
			$this->_securePort = $this->getIsSecureConnection() && isset($_SERVER['SERVER_PORT']) ? (int)$_SERVER['SERVER_PORT'] : 443;
		}

		return $this->_securePort;
	}

	/**
	 * Sets the port to use for secure requests.
	 * This setter is provided in case a custom port is necessary for certain
	 * server configurations.
	 * @param integer $value port number.
	 */
	public function setSecurePort($value)
	{
		if ($value != $this->_securePort)
		{
			$this->_securePort = (int)$value;
			$this->_hostInfo = null;
		}
	}

	private $_contentTypes;

	/**
	 * Returns the content types acceptable by the end user.
	 * This is determined by the `Accept` HTTP header. For example,
	 *
	 * ```php
	 * $_SERVER['HTTP_ACCEPT'] = 'text/plain; q=0.5, application/json; version=1.0, application/xml; version=2.0;';
	 * $types = $request->getAcceptableContentTypes();
	 * print_r($types);
	 * // displays:
	 * // [
	 * //     'application/json' => ['q' => 1, 'version' => '1.0'],
	 * //      'application/xml' => ['q' => 1, 'version' => '2.0'],
	 * //           'text/plain' => ['q' => 0.5],
	 * // ]
	 * ```
	 *
	 * @return array the content types ordered by the quality score. Types with the highest scores
	 * will be returned first. The array keys are the content types, while the array values
	 * are the corresponding quality score and other parameters as given in the header.
	 */
	public function getAcceptableContentTypes()
	{
		if ($this->_contentTypes === null)
		{
			if (isset($_SERVER['HTTP_ACCEPT']))
			{
				$this->_contentTypes = $this->parseAcceptHeader($_SERVER['HTTP_ACCEPT']);
			}
			else
			{
				$this->_contentTypes = [];
			}
		}

		return $this->_contentTypes;
	}

	/**
	 * Sets the acceptable content types.
	 * Please refer to [[getAcceptableContentTypes()]] on the format of the parameter.
	 * @param array $value the content types that are acceptable by the end user. They should
	 * be ordered by the preference level.
	 * @see getAcceptableContentTypes()
	 * @see parseAcceptHeader()
	 */
	public function setAcceptableContentTypes($value)
	{
		$this->_contentTypes = $value;
	}

	/**
	 * Returns request content-type
	 * The Content-Type header field indicates the MIME type of the data
	 * contained in [[getRawBody()]] or, in the case of the HEAD method, the
	 * media type that would have been sent had the request been a GET.
	 * For the MIME-types the user expects in response, see [[acceptableContentTypes]].
	 * @return string request content-type. Null is returned if this information is not available.
	 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.17
	 * HTTP 1.1 header field definitions
	 */
	public function getContentType()
	{
		if (isset($_SERVER["CONTENT_TYPE"]))
		{
			return $_SERVER["CONTENT_TYPE"];
		}
		elseif (isset($_SERVER["HTTP_CONTENT_TYPE"]))
		{
			//fix bug https://bugs.php.net/bug.php?id=66606
			return $_SERVER["HTTP_CONTENT_TYPE"];
		}

		return null;
	}

	private $_languages;

	/**
	 * Returns the languages acceptable by the end user.
	 * This is determined by the `Accept-Language` HTTP header.
	 * @return array the languages ordered by the preference level. The first element
	 * represents the most preferred language.
	 */
	public function getAcceptableLanguages()
	{
		if ($this->_languages === null)
		{
			if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
			{
				$this->_languages = array_keys($this->parseAcceptHeader($_SERVER['HTTP_ACCEPT_LANGUAGE']));
			}
			else
			{
				$this->_languages = [];
			}
		}

		return $this->_languages;
	}

	/**
	 * @param array $value the languages that are acceptable by the end user. They should
	 * be ordered by the preference level.
	 */
	public function setAcceptableLanguages($value)
	{
		$this->_languages = $value;
	}

	/**
	 * Returns the user-preferred language that should be used by this application.
	 * The language resolution is based on the user preferred languages and the languages
	 * supported by the application. The method will try to find the best match.
	 * @param array $languages a list of the languages supported by the application. If this is empty, the current
	 * application language will be returned without further processing.
	 * @return string the language that the application should use.
	 */
	public function getPreferredLanguage(array $languages = [])
	{
		if (empty($languages))
		{
			return Yii::$app->language;
		}
		foreach ($this->getAcceptableLanguages() as $acceptableLanguage)
		{
			$acceptableLanguage = str_replace('_', '-', strtolower($acceptableLanguage));
			foreach ($languages as $language)
			{
				$normalizedLanguage = str_replace('_', '-', strtolower($language));

				if ($normalizedLanguage === $acceptableLanguage || // en-us==en-us
					strpos($acceptableLanguage, $normalizedLanguage . '-') === 0 || // en==en-us
					strpos($normalizedLanguage, $acceptableLanguage . '-') === 0
				)
				{ // en-us==en

					return $language;
				}
			}
		}

		return reset($languages);
	}

	/**
	 * Returns the cookie collection.
	 * Through the returned cookie collection, you may access a cookie using the following syntax:
	 *
	 * ~~~
	 * $cookie = $request->cookies['name']
	 * if ($cookie !== null) {
	 *     $value = $cookie->value;
	 * }
	 *
	 * // alternatively
	 * $value = $request->cookies->getValue('name');
	 * ~~~
	 *
	 * @return CookieCollection the cookie collection.
	 */
	public function getCookies()
	{
		if ($this->_cookies === null)
		{
			$this->_cookies = new CookieCollection($this->loadCookies(), [
				'readOnly' => true,
			]);
		}

		return $this->_cookies;
	}

	/**
	 * Converts `$_COOKIE` into an array of [[Cookie]].
	 * @return array the cookies obtained from request
	 * @throws InvalidConfigException if [[cookieValidationKey]] is not set when [[enableCookieValidation]] is true
	 */
	protected function loadCookies()
	{
		$cookies = [];
		if ($this->enableCookieValidation)
		{
			if ($this->cookieValidationKey == '')
			{
				throw new InvalidConfigException(get_class($this) . '::cookieValidationKey must be configured with a secret key.');
			}
			foreach ($_COOKIE as $name => $value)
			{
				if (!is_string($value))
				{
					continue;
				}
				$data = Yii::$app->getSecurity()->validateData($value, $this->cookieValidationKey);
				if ($data === false)
				{
					continue;
				}
				$data = @unserialize($data);
				if (is_array($data) && isset($data[0], $data[1]) && $data[0] === $name)
				{
					$cookies[$name] = new Cookie([
						'name' => $name,
						'value' => $data[1],
						'expire' => null
					]);
				}
			}
		}
		else
		{
			foreach ($_COOKIE as $name => $value)
			{
				$cookies[$name] = new Cookie([
					'name' => $name,
					'value' => $value,
					'expire' => null
				]);
			}
		}

		return $cookies;
	}

	private $_csrfToken;

	/**
	 * Returns the token used to perform CSRF validation.
	 *
	 * This token is a masked version of [[rawCsrfToken]] to prevent [BREACH attacks](http://breachattack.com/).
	 * This token may be passed along via a hidden field of an HTML form or an HTTP header value
	 * to support CSRF validation.
	 * @param boolean $regenerate whether to regenerate CSRF token. When this parameter is true, each time
	 * this method is called, a new CSRF token will be generated and persisted (in session or cookie).
	 * @return string the token used to perform CSRF validation.
	 */
	public function getCsrfToken($regenerate = false)
	{
		if ($this->_csrfToken === null || $regenerate)
		{
			if ($regenerate || ($token = $this->loadCsrfToken()) === null)
			{
				$token = $this->generateCsrfToken();
			}
			// the mask doesn't need to be very random
			$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.';
			$mask = substr(str_shuffle(str_repeat($chars, 5)), 0, static::CSRF_MASK_LENGTH);
			// The + sign may be decoded as blank space later, which will fail the validation
			$this->_csrfToken = str_replace('+', '.', base64_encode($mask . $this->xorTokens($token, $mask)));
		}

		return $this->_csrfToken;
	}

	/**
	 * Loads the CSRF token from cookie or session.
	 * @return string the CSRF token loaded from cookie or session. Null is returned if the cookie or session
	 * does not have CSRF token.
	 */
	protected function loadCsrfToken()
	{
		if ($this->enableCsrfCookie)
		{
			return $this->getCookies()->getValue($this->csrfParam);
		}
		else
		{
			return Yii::$app->getSession()->get($this->csrfParam);
		}
	}

	/**
	 * Generates  an unmasked random token used to perform CSRF validation.
	 * @return string the random token for CSRF validation.
	 */
	protected function generateCsrfToken()
	{
		$token = Yii::$app->getSecurity()->generateRandomString();
		if ($this->enableCsrfCookie)
		{
			$cookie = $this->createCsrfCookie($token);
			Yii::$app->getResponse()->getCookies()->add($cookie);
		}
		else
		{
			Yii::$app->getSession()->set($this->csrfParam, $token);
		}
		return $token;
	}

	/**
	 * Returns the XOR result of two strings.
	 * If the two strings are of different lengths, the shorter one will be padded to the length of the longer one.
	 * @param string $token1
	 * @param string $token2
	 * @return string the XOR result
	 */
	private function xorTokens($token1, $token2)
	{
		$n1 = StringHelper::byteLength($token1);
		$n2 = StringHelper::byteLength($token2);
		if ($n1 > $n2)
		{
			$token2 = str_pad($token2, $n1, $token2);
		}
		elseif ($n1 < $n2)
		{
			$token1 = str_pad($token1, $n2, $n1 === 0 ? ' ' : $token1);
		}

		return $token1 ^ $token2;
	}

	/**
	 * @return string the CSRF token sent via [[CSRF_HEADER]] by browser. Null is returned if no such header is sent.
	 */
	public function getCsrfTokenFromHeader()
	{
		$key = 'HTTP_' . str_replace('-', '_', strtoupper(static::CSRF_HEADER));
		return isset($_SERVER[$key]) ? $_SERVER[$key] : null;
	}

	/**
	 * Creates a cookie with a randomly generated CSRF token.
	 * Initial values specified in [[csrfCookie]] will be applied to the generated cookie.
	 * @param string $token the CSRF token
	 * @return Cookie the generated cookie
	 * @see enableCsrfValidation
	 */
	protected function createCsrfCookie($token)
	{
		$options = $this->csrfCookie;
		$options['name'] = $this->csrfParam;
		$options['value'] = $token;
		return new Cookie($options);
	}

	/**
	 * Performs the CSRF validation.
	 *
	 * This method will validate the user-provided CSRF token by comparing it with the one stored in cookie or session.
	 * This method is mainly called in [[Controller::beforeAction()]].
	 *
	 * Note that the method will NOT perform CSRF validation if [[enableCsrfValidation]] is false or the HTTP method
	 * is among GET, HEAD or OPTIONS.
	 *
	 * @param string $token the user-provided CSRF token to be validated. If null, the token will be retrieved from
	 * the [[csrfParam]] POST field or HTTP header.
	 * This parameter is available since version 2.0.4.
	 * @return boolean whether CSRF token is valid. If [[enableCsrfValidation]] is false, this method will return true.
	 */
	public function validateCsrfToken($token = null)
	{
		$method = $this->getMethod();
		// only validate CSRF token on non-"safe" methods http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.1.1
		if (!$this->enableCsrfValidation || in_array($method, [
				'GET',
				'HEAD',
				'OPTIONS'
			], true)
		)
		{
			return true;
		}

		$trueToken = $this->loadCsrfToken();

		if ($token !== null)
		{
			return $this->validateCsrfTokenInternal($token, $trueToken);
		}
		else
		{
			return $this->validateCsrfTokenInternal($this->getBodyParam($this->csrfParam), $trueToken)
			|| $this->validateCsrfTokenInternal($this->getCsrfTokenFromHeader(), $trueToken);
		}
	}

	/**
	 * Validates CSRF token
	 *
	 * @param string $token
	 * @param string $trueToken
	 * @return boolean
	 */
	private function validateCsrfTokenInternal($token, $trueToken)
	{
		$token = base64_decode(str_replace('.', '+', $token));
		$n = StringHelper::byteLength($token);
		if ($n <= static::CSRF_MASK_LENGTH)
		{
			return false;
		}
		$mask = StringHelper::byteSubstr($token, 0, static::CSRF_MASK_LENGTH);
		$token = StringHelper::byteSubstr($token, static::CSRF_MASK_LENGTH, $n - static::CSRF_MASK_LENGTH);
		$token = $this->xorTokens($mask, $token);

		return $token === $trueToken;
	}
}
