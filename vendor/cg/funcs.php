<?php
namespace cg;

use phpmailer\PHPMailer;
use Zhuzhichao\IpLocationZh\IP;

class funcs
{

	public static function valid_order_sn($order_sn)
	{
		if (preg_match('/[0-9]{13}/', $order_sn))
		{
			return true;
		}
		return false;
	}

	/**
	 *
	 * 验证由逗号隔开的图片md5
	 */
	public static function valid_guid($guid)
	{
		if (preg_match('/^[\-a-f0-9]+$/i', $guid))
		{
			return true;
		}
		return false;
	}

	/**
	 *
	 * 验证由逗号隔开的图片md5
	 */
	public static function valid_md5($md5)
	{
		if (preg_match('/[a-f,0-9]+/i', $md5))
		{
			return true;
		}
		return false;
	}

	public static function html2ubb($str)
	{
		//[quote]aa[/quote]
		//<div class="quote"><blockquote></blockquote></div>
		$str = str_replace('<div class="quote"><blockquote>', '[quote]', $str);
		$str = str_replace('</blockquote></div>', '[/quote]', $str);
		//[table][tr][td][/td][/tr][/table]
		$str = preg_replace('/<([\/]?)t(able|r|d)[^>]*>/', '[$1t$2]', $str);
		$str = preg_replace('/\r/', "", $str);
		$str = preg_replace('/on(load|click|dbclick|mouseover|mousedown|mouseup)="[^"]+"/i', "", $str);
		// 获取音频ubb代码
		$str = preg_replace_callback('/<span id=".*?"><\/span><script[^>]+>.*?\'soundFile=(.*?)\'.*?<\/script>/', function ($matches)
		{
			return '[audio]' . urldecode($matches[1]) . '[/audio]';
		}, $str);
		// 获取视频ubb代码
		$str = preg_replace('/<span id=".*?"><\/span><script[^>]+>.*?\'width\', \'(\d+)\', \'height\', \'(\d+)\'.*?\'src\', encodeURI\(\'(.*?)\'\).*?<\/script>/i', " [flash=$1,$2]$3[/flash]", $str);
		$str = preg_replace('/<span id=".*?"><\/span><script[^>]+>.*?\'width\', \'(\d+)\', \'height\', \'(\d+)\'.*?\'src\', \'(.*?)\'.*?<\/script>/i', " [flash=$1,$2]$3[/flash]", $str);
		// 去除无效js
		$str = preg_replace('/<script[^>]*?>([\w\W]*?)<\/script>/i', "", $str);
		$str = preg_replace('/<a[^>]+href="([^"]+)"[^>]*>(.*?)<\/a>/i', "[url=$1]$2[/url]", $str);
		$str = preg_replace('/<font[^>]+color="(.*?)"[^>]*>(.*?)<\/font>/i', "[color=$1]$2[/color]", $str);
		$str = preg_replace('/<img[^>]+src="([^"]+)"[^>]*>/i', "\n[img]$1[/img]\n", $str);
		$str = preg_replace('/<p[^>]*?>/i', "\n\n", $str);
		$str = preg_replace('/<([\/]?)b>/i', "[$1b]", $str);
		$str = preg_replace('/<([\/]?)strong>/i', "[$1b]", $str);
		$str = preg_replace('/<([\/]?)u>/i', "[$1u]", $str);
		$str = preg_replace('/<([\/]?)i>/i', "[$1i]", $str);
		$str = preg_replace('/&nbsp;/', " ", $str);
		$str = preg_replace('/&amp;/', "&", $str);
		$str = preg_replace('/&quot;/', "\"", $str);
		$str = preg_replace('/&lt;/', "<", $str);
		$str = preg_replace('/&gt;/', ">", $str);
		$str = preg_replace('/<br>/i', "\n", $str);
		$str = preg_replace('/<br\/>/i', "\n", $str);
		$str = preg_replace('/<br \/>/i', "\n", $str);
		$str = preg_replace('/<[^>]*?>/', "", $str);
		$str = preg_replace('/\[url=([^\]]+)\]\n(\[img\]\1\[\/img\])\n\[\/url\]/', "$2", $str);
		$str = preg_replace('/\n+/', "\n", $str);
		//去除空引用
		$str = str_replace('[quote][/quote]', '', $str);
		//去除空视频连接
		$str = preg_replace('/\[flash=\d+,\d+\]\[\/flash\]/', '', $str);
		return $str;
	}

	/**
	 * 数字范围转换为ids
	 * @param string $numrange 数字范围
	 * @return string $ids    返回字符串
	 */
	public static function idrange2ids($numrange)
	{
		$ids = '';
		$strs = array();

		if (preg_match('/[^\d|,|-]/', $numrange))
		{
			echo "只能包含逗号数字和减号";
		}
		$numrange = trim($numrange, ',');
		$strs = explode(',', $numrange);
		foreach ($strs as $str)
		{
			if (!strpos($str, '-'))
			{
				$ids .= $str . ',';
				continue;
			}
			list($head, $end) = explode('-', $str);
			for ($i = $head; $i <= $end; $i++)
			{
				$ids .= $i . ',';
			}
		}
		$ids = trim($ids, ',');
		return $ids;
		echo $ids;
	}

	public static function get_file_extension($filename)
	{
		return strtolower(substr($filename, strrpos($filename, '.') + 1));
	}

	/**
	 * 判断是否手机号
	 * @param string $mobile 手机号
	 * @return bool
	 */
	public static function valid_mobile($mobile)
	{
		if (preg_match('/^1[34578][0-9]{9}$/', $mobile))
		{
			return true;
		}
		return false;
	}

	public static function send_mail($to, $title, $content, $attachment = '')
	{
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->SMTPSecure = 'ssl';
		$mail->Host = "smtp.exmail.qq.com";
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->Username = "monitor@xmeise.com";
		$mail->Password = "U17aTv0Q";
		$mail->setFrom('monitor@xmeise.com', 'monitor');
		if (is_array($to))
		{
			foreach ($to as $t)
			{
				if (!empty($t))
				{
					$mail->addCC($t);
				}
			}
		}
		else
		{
			$mail->addAddress($to);
		}
		if (is_array($attachment))
		{
			foreach ($attachment as $a)
			{
				$mail->addAttachment($a);
			}
		}
		elseif (!empty($attachment))
		{
			$mail->addAttachment($attachment);
		}
		$mail->Subject = $title;
		$mail->msgHTML($content);
		$mail->AltBody = $content;
		$mail->CharSet = 'utf-8';
		return $mail->send();
	}

	/**
	 * 初始化表数据
	 * @param array $cols_titles 列标题
	 * @param array $rows_titles 行标题
	 */
	public static function init_table_rows($cols_titles, $rows_titles)
	{
		array_unshift($cols_titles, current($rows_titles));
		//$i 为行, $j为列
		$table_rows = array(); //表数据，从第一行第一列开始
		$i = 1; //第一行
		foreach ($rows_titles as $rows_title)
		{
			$j = 1; //第一列
			foreach ($cols_titles as $cols_title)
			{
				if ($i == 1 && $j == 1) //第一行第一列是行标题
				{
					$table_rows[$i][$j] = $rows_title;
				}
				elseif ($i == 1 && $j > 1) //第一行,其他列为列标题
				{
					$table_rows[$i][$j] = $cols_title;
				}
				elseif ($i > 1 && $j == 1) //第一列，其他行为行标题
				{
					$table_rows[$i][$j] = $rows_title;
				}
				else //其他行其他列，初始化为0
				{
					$table_rows[$i][$j] = 0;
				}
				$j++;
			}
			$i++;
		}
		return $table_rows;
	}

	public static function export_csv($data, $filename = '')
	{
		header('Content-Type: application/csv');
		header('Content-Disposition: attachement; filename="' . $filename . '.csv";');
		echo "\xEF\xBB\xBF";
		$output = fopen('php://output', 'w');
		foreach ($data as $row)
		{
			fputcsv($output, $row);
		}
		exit();
	}

	/**
	 * 导出Excel
	 * @param array $data 数据，包含生成多个表单的数据
	 * @param string $filename 保存的文件名
	 * @param mixed $col_type 列类型 allstring/allnumber/array('A','B') string
	 */
	public static function export_excel($data, $filename = '', $col_type = 'allstring')
	{
		$cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_igbinary;
		\PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
		$objPHPExcel = new \PHPExcel();
		$c = 0;
		$atoz = funcs::get_atoz();
		foreach ($data as $title => $rows)
		{
			$excel_sheet = $objPHPExcel->createSheet($c);
			$i = 1;
			foreach ($rows as $row)
			{
				$j = 1;
				foreach ($row as $value)
				{
					if ($col_type == 'allstring')
					{
						$excel_sheet->setCellValueExplicit($atoz[$j - 1] . $i, $value);
					}
					elseif ($col_type == 'allnumber')
					{
						$excel_sheet->setCellValue($atoz[$j - 1] . $i, $value);
					}
					else
					{
						if (in_array($atoz[$j - 1], $col_type))
						{
							$excel_sheet->setCellValueExplicit($atoz[$j - 1] . $i, $value);
						}
						else
						{
							$excel_sheet->setCellValue($atoz[$j - 1] . $i, $value);
						}
					}
					$j++;
				}
				$i++;
			}
			if (is_integer($title))
			{
				$excel_sheet->setTitle('Sheet' . $c);
			}
			else
			{
				$excel_sheet->setTitle($title);
			}
			$c++;
		}
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit();
	}

	/*
	 * 生成表头列, export_excel里用到了
	 */
	public static function get_atoz()
	{
		$s = array();
		for ($i = 65; $i <= 90; $i++)
		{
			$s[] = chr($i);
		}
		for ($i = 65; $i <= 90; $i++)
		{
			$s[] = 'A' . chr($i);
		}
		return $s;
	}

	public static function zip($to_zip_files, $dst_file)
	{
		$zip = new \ZipArchive();
		if ($zip->open($dst_file, \ZIPARCHIVE::CREATE) !== TRUE)
		{
			exit("cannot create zip file : $dst_file\n");
		}
		foreach ($to_zip_files as $file)
		{
			if (!file_exists($file) || filesize($file) == 0)
			{
				continue;
			}
			$zip->addFile($file, basename($file));
		}
		$zip->close();
	}

	/*
	 * 检测php扩展有没有加载
	 */
	public static function check_ext_loaded($ext)
	{
		$all_exts = get_loaded_extensions();
		if (in_array($ext, $all_exts) !== false)
		{
			return true;
		}
		return false;
	}

	public static function create_hidden_ip($ip)
	{
		$is_ipv6 = strpos($ip, ':') !== false;
		$ip = str_replace('.', ':', $ip);
		$arr_ip = funcs::explode($ip, ':');
		array_pop($arr_ip);
		array_pop($arr_ip);
		$ip = implode(":", $arr_ip) . ':*';
		if (!$is_ipv6)
		{
			$ip = str_replace(':', '.', $ip);
		}
		return $ip;
	}

	/*
	 * @$dst_dir 选择递归创建目录的位置
	 * @$dst_filename 要递归创建的目录
	 */
	public static function recursive_mkdir($dst_dir, $dst_filename)
	{
		$dirs = explode('/', $dst_filename);
		$dir = $dst_dir . '/';
		for ($i = 0; $i < count($dirs) - 1; $i++)
		{
			$dir .= $dirs[$i] . '/';
			if (!is_dir($dir))
			{
				$ret = mkdir($dir, 0777);
				if (!$ret)
				{
					return false;
				}
			}
		}
		return true;
	}

	public static function ubb2html($sUBB)
	{
		global $emotPath, $cnum, $arrcode, $bUbb2htmlFunctionInit;
		$sHtml = $sUBB;
		$cnum = 0;
		$arrcode = array();
		$emotPath = '../xheditor_emot/'; //表情根路径


		if (!$bUbb2htmlFunctionInit)
		{

			function saveCodeArea($match)
			{
				global $cnum, $arrcode;
				$cnum++;
				$arrcode[$cnum] = $match[0];
				return "[\tubbcodeplace_" . $cnum . "\t]";
			}
		}
		$sHtml = preg_replace_callback('/\[code\s*(?:=\s*((?:(?!")[\s\S])+?)(?:"[\s\S]*?)?)?\]([\s\S]*?)\[\/code\]/i', 'saveCodeArea', $sHtml);

		$sHtml = preg_replace("/&/", '&amp;', $sHtml);
		$sHtml = preg_replace("/</", '&lt;', $sHtml);
		$sHtml = preg_replace("/>/", '&gt;', $sHtml);
		$sHtml = preg_replace("/\r?\n/", '<br />', $sHtml);

		$sHtml = preg_replace('/\[(\/?)(b|u|i|s|sup|sub)\]/i', '<$1$2>', $sHtml);
		$sHtml = preg_replace('/\[color\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i', '<span style="color:$1;">', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getSizeName($match)
			{
				$arrSize = array(
					'10px',
					'13px',
					'16px',
					'18px',
					'24px',
					'32px',
					'48px'
				);
				if (preg_match('/^\d+$/', $match[1]))
				{
					$match[1] = $arrSize[$match[1] - 1];
				}
				return '<span style="font-size:' . $match[1] . ';">';
			}
		}
		$sHtml = preg_replace_callback('/\[size\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i', 'getSizeName', $sHtml);
		$sHtml = preg_replace('/\[font\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i', '<span style="font-family:$1;">', $sHtml);
		$sHtml = preg_replace('/\[back\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]/i', '<span style="background-color:$1;">', $sHtml);
		$sHtml = preg_replace('/\[\/(color|size|font|back)\]/i', '</span>', $sHtml);

		for ($i = 0; $i < 3; $i++)
		{
			$sHtml = preg_replace('/\[align\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\](((?!\[align(?:\s+[^\]]+)?\])[\s\S])*?)\[\/align\]/', '<p align="$1">$2</p>', $sHtml);
		}
		$sHtml = preg_replace('/\[img\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/img\]/i', '<img src="$1" alt="" />', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getImg($match)
			{
				$alt = $match[1];
				$p1 = $match[2];
				$p2 = $match[3];
				$p3 = $match[4];
				$src = $match[5];
				$a = $p3 ? $p3 : (!is_numeric($p1) ? $p1 : '');
				return '<img src="' . $src . '" alt="' . $alt . '"' . (is_numeric($p1) ? ' width="' . $p1 . '"' : '') . (is_numeric($p2) ? ' height="' . $p2 . '"' : '') .
				($a ? ' align="' . $a . '"' : '') . ' />';
			}
		}
		$sHtml = preg_replace_callback('/\[img\s*=([^,\]]*)(?:\s*,\s*(\d*%?)\s*,\s*(\d*%?)\s*)?(?:,?\s*(\w+))?\s*\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*)?\s*\[\/img\]/i', 'getImg', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getEmot($match)
			{
				global $emotPath;
				$arr = split(',', $match[1]);
				if (!isset($arr[1]))
				{
					$arr[1] = $arr[0];
					$arr[0] = 'default';
				}
				$path = $emotPath . $arr[0] . '/' . $arr[1] . '.gif';
				return '<img src="' . $path . '" alt="' . $arr[1] . '" />';
			}
		}
		$sHtml = preg_replace_callback('/\[emot\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\/\]/i', 'getEmot', $sHtml);
		$sHtml = preg_replace('/\[url\]\s*(((?!")[\s\S])*?)(?:"[\s\S]*?)?\s*\[\/url\]/i', '<a href="$1">$1</a>', $sHtml);
		$sHtml = preg_replace('/\[url\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]\s*([\s\S]*?)\s*\[\/url\]/i', '<a href="$1">$2</a>', $sHtml);
		$sHtml = preg_replace('/\[email\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/email\]/i', '<a href="mailto:$1">$1</a>', $sHtml);
		$sHtml = preg_replace('/\[email\s*=\s*([^\]"]+?)(?:"[^\]]*?)?\s*\]\s*([\s\S]+?)\s*\[\/email\]/i', '<a href="mailto:$1">$2</a>', $sHtml);
		$sHtml = preg_replace('/\[quote\]/i', '<blockquote>', $sHtml);
		$sHtml = preg_replace('/\[\/quote\]/i', '</blockquote>', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getFlash($match)
			{
				$w = $match[1];
				$h = $match[2];
				$url = $match[3];
				if (!$w)
				{
					$w = 480;
				}
				if (!$h)
				{
					$h = 400;
				}
				return '<embed type="application/x-shockwave-flash" src="' . $url . '" wmode="opaque" quality="high" bgcolor="#ffffff" menu="false" play="true" loop="true" width="' .
				$w . '" height="' . $h . '" />';
			}
		}
		$sHtml = preg_replace_callback('/\[flash\s*(?:=\s*(\d+)\s*,\s*(\d+)\s*)?\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/flash\]/i', 'getFlash', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getMedia($match)
			{
				$w = $match[1];
				$h = $match[2];
				$play = $match[3];
				$url = $match[4];
				if (!$w)
				{
					$w = 480;
				}
				if (!$h)
				{
					$h = 400;
				}
				return '<embed type="application/x-mplayer2" src="' . $url . '" enablecontextmenu="false" autostart="' . ($play == '1' ? 'true' : 'false') . '" width="' . $w .
				'" height="' . $h . '" />';
			}
		}
		$sHtml = preg_replace_callback('/\[media\s*(?:=\s*(\d+)\s*,\s*(\d+)\s*(?:,\s*(\d+)\s*)?)?\]\s*(((?!")[\s\S])+?)(?:"[\s\S]*?)?\s*\[\/media\]/i', 'getMedia', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getTable($match)
			{
				return '<table' . (isset($match[1]) ? ' width="' . $match[1] . '"' : '') . (isset($match[2]) ? ' bgcolor="' . $match[2] . '"' : '') . '>';
			}
		}
		$sHtml = preg_replace_callback('/\[table\s*(?:=(\d{1,4}%?)\s*(?:,\s*([^\]"]+)(?:"[^\]]*?)?)?)?\s*\]/i', 'getTable', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getTR($match)
			{
				return '<tr' . (isset($match[1]) ? ' bgcolor="' . $match[1] . '"' : '') . '>';
			}
		}
		$sHtml = preg_replace_callback('/\[tr\s*(?:=(\s*[^\]"]+))?(?:"[^\]]*?)?\s*\]/i', 'getTR', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getTD($match)
			{
				$col = isset($match[1]) ? $match[1] : 0;
				$row = isset($match[2]) ? $match[2] : 0;
				$w = isset($match[3]) ? $match[3] : null;
				return '<td' . ($col > 1 ? ' colspan="' . $col . '"' : '') . ($row > 1 ? ' rowspan="' . $row . '"' : '') . ($w ? ' width="' . $w . '"' : '') . '>';
			}
		}
		$sHtml = preg_replace_callback('/\[td\s*(?:=\s*(\d{1,2})\s*,\s*(\d{1,2})\s*(?:,\s*(\d{1,4}%?))?)?\s*\]/i', 'getTD', $sHtml);
		$sHtml = preg_replace('/\[\/(table|tr|td)\]/i', '</$1>', $sHtml);
		$sHtml = preg_replace('/\[\*\]((?:(?!\[\*\]|\[\/list\]|\[list\s*(?:=[^\]]+)?\])[\s\S])+)/i', '<li>$1</li>', $sHtml);
		if (!$bUbb2htmlFunctionInit)
		{

			function getUL($match)
			{
				$str = '<ul';
				if (isset($match[1]))
				{
					$str .= ' type="' . $match[1] . '"';
				}
				return $str . '>';
			}
		}
		$sHtml = preg_replace_callback('/\[list\s*(?:=\s*([^\]"]+))?(?:"[^\]]*?)?\s*\]/i', 'getUL', $sHtml);
		$sHtml = preg_replace('/\[\/list\]/i', '</ul>', $sHtml);
		$sHtml = preg_replace('/\[hr\/\]/i', '<hr />', $sHtml);

		for ($i = 1; $i <= $cnum; $i++)
		{
			$sHtml = str_replace("[\tubbcodeplace_" . $i . "\t]", $arrcode[$i], $sHtml);
		}

		if (!$bUbb2htmlFunctionInit)
		{

			function fixText($match)
			{
				$text = $match[2];
				$text = preg_replace("/\t/", '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $text);
				$text = preg_replace("/ /", '&nbsp;', $text);
				return $match[1] . $text;
			}
		}
		$sHtml = preg_replace_callback('/(^|<\/?\w+(?:\s+[^>]*?)?>)([^<$]+)/i', 'fixText', $sHtml);
		$sHtml = str_replace('[code]', '<pre>', $sHtml);
		$sHtml = str_replace('[/code]', '</pre>', $sHtml);
		$bUbb2htmlFunctionInit = true;

		return $sHtml;
	}

	public static function get_province_city_by_ip($ip)
	{
		$province = '';
		$city = '';
		$ip_info = IP::find($ip);
		if ($ip_info != 'N/A')
		{
			$province = $ip_info[1];
			$city = $ip_info[2];
		}
		return [
			$province,
			$city
		];
	}

	/*
	 * 今年的时间只显示月-日 时:分
	 * 今天的时间只显示时:分
	 * 其他时间显示年-月-日 时:分
	 */
	public static function get_simple_datetime($timestamp)
	{
		if (date('Y', $timestamp) < date('Y'))
		{
			return date('Y-n-d H:i', $timestamp);
		}
		elseif (date('G', $timestamp) > date('G'))
		{
			return date('n月d日 H:i', $timestamp);
		}
		$limit = time() - $timestamp;
		if ($limit < 3600)
		{
			return floor($limit / 60) . '分钟前';
		}
		elseif ($limit >= 3600 && $limit < 86400)
		{
			return '今天' . date('H:i', $timestamp);
		}
		elseif ($limit >= 86400 && $limit < 604800)
		{
			// 1周内
			return floor($limit / 86400) . '天前';
		}
		elseif ($limit >= 604800 && $limit < 1209600)
		{
			return '1周前';
		}
		elseif ($limit >= 1209600 && $limit < 1814400)
		{
			return '2周前';
		}
		elseif ($limit >= 1814400 && $limit < 31536000)
		{
			// 1年内
			return date('n月d日', $timestamp);
		}
		else
		{
			return date('Y-n-d', $timestamp);
		}
	}

	public static function direct_output($str)
	{
		echo str_pad(' ', 4096);
		echo $str;
		ob_flush();
		flush();
	}

	public static function guid()
	{
		if (function_exists('com_create_guid'))
		{
			$uuid = com_create_guid();
			return substr($uuid, 1, -1);
		}
		else
		{
			mt_srand((double)microtime() * 10000);
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);
			$uuid = substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
			return $uuid;
		}
	}

	public static function explode($s, $delimiter = '')
	{
		if (empty($s))
		{
			return array();
		}
		$s = str_replace(array(
			';',
			', ',
			"\r",
			"\n"
		), ',', $s);
		if ($delimiter != '')
		{
			$s = str_replace($delimiter, ",", $s);
		}
		$s = preg_replace('/,+/i', ',', $s);
		return explode(',', $s);
	}

	/**
	 * 编码转换,gbk -> utf8
	 */
	public static function gbk2utf8($s)
	{
		return self::iconv_array("gbk", "utf-8", $s);
	}

	/**
	 * 编码转换,utf8 -> gbk
	 */
	public static function utf82gbk($s)
	{
		return self::iconv_array("utf-8", "gbk", $s);
	}

	/**
	 * 对数组进行编码转换
	 *
	 * @param strint $in_charset 输入编码
	 * @param string $out_charset 输出编码
	 * @param array $arr 输入数组
	 * @return array              返回数组
	 */
	public static function iconv_array($in_charset, $out_charset, $arr)
	{
		if (strtolower($in_charset) == "utf8")
		{
			$in_charset = "UTF-8";
		}
		if (strtolower($out_charset) == "utf-8" || strtolower($out_charset) == 'utf8')
		{
			$out_charset = "UTF-8";
		}
		if (is_array($arr))
		{
			foreach ($arr as $key => $value)
			{
				$arr[$key] = self::iconv_array($in_charset, $out_charset, $value);
			}
		}
		else
		{
			if (!is_numeric($arr))
			{
				@$arr = iconv($in_charset, $out_charset, $arr);
			}
		}
		return $arr;
	}

	public static function full2half($str)
	{
		$chars = Array(
			'【' => '[',
			'】' => ']',
			'０' => '0',
			'１' => '1',
			'２' => '2',
			'３' => '3',
			'４' => '4',
			'５' => '5',
			'６' => '6',
			'７' => '7',
			'８' => '8',
			'９' => '9',
			'Ａ' => 'A',
			'Ｂ' => 'B',
			'Ｃ' => 'C',
			'Ｄ' => 'D',
			'Ｅ' => 'E',
			'Ｆ' => 'F',
			'Ｇ' => 'G',
			'Ｈ' => 'H',
			'Ｉ' => 'I',
			'Ｊ' => 'J',
			'Ｋ' => 'K',
			'Ｌ' => 'L',
			'Ｍ' => 'M',
			'Ｎ' => 'N',
			'Ｏ' => 'O',
			'Ｐ' => 'P',
			'Ｑ' => 'Q',
			'Ｒ' => 'R',
			'Ｓ' => 'S',
			'Ｔ' => 'T',
			'Ｕ' => 'U',
			'Ｖ' => 'V',
			'Ｗ' => 'W',
			'Ｘ' => 'X',
			'Ｙ' => 'Y',
			'Ｚ' => 'Z',
			'ａ' => 'a',
			'ｂ' => 'b',
			'ｃ' => 'c',
			'ｄ' => 'd',
			'ｅ' => 'e',
			'ｆ' => 'f',
			'ｇ' => 'g',
			'ｈ' => 'h',
			'ｉ' => 'i',
			'ｊ' => 'j',
			'ｋ' => 'k',
			'ｌ' => 'l',
			'ｍ' => 'm',
			'ｎ' => 'n',
			'ｏ' => 'o',
			'ｐ' => 'p',
			'ｑ' => 'q',
			'ｒ' => 'r',
			'ｓ' => 's',
			'ｔ' => 't',
			'ｕ' => 'u',
			'ｖ' => 'v',
			'ｗ' => 'w',
			'ｘ' => 'x',
			'ｙ' => 'y',
			'ｚ' => 'z',
			'：' => ':',
			'、' => '/',
			'／' => '/',
			'，' => ',',
			'～' => '~',
			'。' => '.',
			'（' => '(',
			'）' => ')',
			'＋' => '+',
			'　' => ' ',
			'—' => '-',
			'•' => '·'
		);
		return str_replace(array_keys($chars), $chars, $str);
	}

	// 改善关键词, 去除无效字符
	public static function pretty_keyword($str)
	{
		$search = array(
			'　',
			"\xc2\xa0",
			'\\',
			'\''
		);
		$replace = array(
			' ',
			'',
			'',
			''
		);
		$str = str_replace($search, $replace, $str);
		$str = trim($str);
		return $str;
	}

	/**
	 * 获取图片路径
	 * @param string $md5
	 * @return string
	 */
	public static function get_md5_path($md5)
	{
		if (empty($md5))
		{
			$md5 = 'fe5179f59bc54744b759b19d11d46798';
		}
		$imgName1 = substr($md5, 0, 2);
		$imgName2 = substr($md5, 2, 2);
		return $imgName1 . "/" . $imgName2 . "/" . $md5;
	}

	/*
	 * 根据订单号计算code, 防止订单完成页面被抓取
	 * code与当前时间, 订单号, 固定参数有关
	 * 每个小时的code都不一样
	 */
	public static function generate_code($order_sn, $date = '')
	{
		if ($date == '')
		{
			$date = date('Y-m-d H', time());
		}
		$code = substr(md5($order_sn . '[meise_code]' . $date), 0, 10);

		return $code;
	}
	/*
	 * 去除商品详情页的无效字符
	 */
	public static function pretty_goods_descr($descr)
	{
		$descr = preg_replace('/ sizcache\d*?="\d+"/', '', $descr);
		$descr = preg_replace('/ sizset="\d*?"/', '', $descr);
		$descr = preg_replace('/ class=".*?"/', '', $descr);
		$descr = preg_replace_callback('/style="([^"]*?)"/', function ($matches) {
			$dict_css = array(
					'font-size',
					'text-align',
					'font-weight',
					'color'
			);
			$s = '';
			$pieces = explode('; ', strtolower($matches[1]));
			$css_arr = array();
			foreach ($pieces as $piece)
			{
				if ($piece == '')
				{
					continue;
				}
				$list = explode(': ', $piece);
				if (!isset($list[1]))
				{
					$list = explode(':', $piece);
				}
				$css_arr[$list[0]] = $list[1];
			}
			foreach ($css_arr as $css => $v)
			{
				if (in_array($css, $dict_css))
				{
					$v = str_replace(';', '', $v);
					$v = str_replace('color', '', $v);
					if ($css == 'font-weight' && $v == 'normal')
					{
						continue;
					}
					if ($css == 'font-size' && ($v == '14px' || $v == '12px'))
					{
						continue;
					}
					$s .= "$css: $v;";
				}
			}
			return "style=\"$s\"";
		}, $descr);
		// 去除font标签
		$descr = preg_replace('/<font[^>]*>/', '', $descr);
		$descr = preg_replace('/<\/font>/', '', $descr);
		$descr = str_replace(' alt=""', '', $descr);
		$descr = str_replace(' style=""', '', $descr);
		$descr = str_replace(' loaded="true"', '', $descr);
		$descr = str_replace("\r\n", '', $descr);
		$descr = str_replace('	', '', $descr);
		// 去除span div p标签
		$pattern = '/<(span|div|p|strong)[^>]*>([&nbsp;])*<\/\1>/';
		while(preg_match($pattern, $descr))
		{
			$descr = preg_replace($pattern, '', $descr);
		}
		$descr = str_replace(' type="image"', '', $descr);
		$descr = str_replace('<h1', '<span', $descr);
		$descr = str_replace('</h1>', '</span>', $descr);
		return $descr;
	}
		
		//  获取图片md5 url
	public static function zf_md5_img($md5)
	{
	    if(empty($md5))
        {
            $md5 = "cadd1973d9cab5764c661117977414c4";
        }
        if (strpos($md5,'img.zuofan.cn') !== false )
        {
            //  如果是新版的img图片地址则
            //  http://img.zuofan.cn/thumb/z/9f/64/9f64ad5c967fc6353d1ae90d2205144d.jpg
            $path = pathinfo($md5);
            //$path['dirname'] = str_replace('/z/','/f/',$path['dirname']);
            $url = $path['dirname'] .'/'. $path['filename']. ',c_fill,';
        }
        elseif (strlen($md5) == 32)
		{
			$dir1 = substr($md5, 0, 2);
			$dir2 = substr($md5, 2, 2);
			$url = "http://img.zuofan.cn/thumb/z/" . $dir1 . "/" . $dir2 . "/" . $md5.",c_fill,";			
		}
		else
		{
		    //http://img.zuofan.cn/thumb/z/image/allimg/160930/241-160930142KO39.jpg

            //http://images.zuofan.cn/image/allimg/160930/241-160930142KO39.jpg
            //http://images.zuofan.cn/allimg/120320/104G44C2-0-lp.jpg
			//http://img.zuofan.cn/thumb/z/104G44C2-0,c_fill,h_120,w_120.jpg
			$url = str_replace('http://images.zuofan.cn/','/uploads/',$md5);
			$url = str_replace('-lp', '', $url);
			$url = str_replace('uploads/', '', $url);
			$p = pathinfo($url);
            $dirname = isset($p['dirname']) ? $p['dirname'] : "";
			$url = $dirname . '/' . $p['filename'];
			$url = 'http://img.zuofan.cn/thumb/z' . $url . ',c_fill,';
		}
		return $url;
	}

    //
    public function get_category_aids($cat_id)
    {
        //  获取分类下文章ids
//        $ids =

    }

	public static function isMobile()
	{
		// 如果有HTTP_X_WAP_PROFILE则一定是移动设备
		if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
		{
			return true;
		}
		// 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
		if (isset ($_SERVER['HTTP_VIA']))
		{
			// 找不到为flase,否则为true
			return stristr($_SERVER['HTTP_VIA'], "wap") ? true : 'unknown_wap';
		}
		// 脑残法，判断手机发送的客户端标志,兼容性有待提高
		if (isset ($_SERVER['HTTP_USER_AGENT']))
		{
			$clientkeywords = array ('nokia',
				'sony',
				'ericsson',
				'mot',
				'samsung',
				'htc',
				'sgh',
				'lg',
				'sharp',
				'sie-',
				'philips',
				'panasonic',
				'alcatel',
				'lenovo',
				'iphone',
				'ipod',
				'blackberry',
				'meizu',
				'android',
				'netfront',
				'symbian',
				'ucweb',
				'windowsce',
				'palm',
				'operamini',
				'operamobi',
				'openwave',
				'nexusone',
				'cldc',
				'midp',
				'unknown',
				'mobile'
			);
			// 从HTTP_USER_AGENT中查找手机浏览器的关键字
			if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
			{
				return strtolower($_SERVER['HTTP_USER_AGENT']);
			}
		}
		// 协议法，因为有可能不准确，放到最后判断
		if (isset ($_SERVER['HTTP_ACCEPT']))
		{
			// 如果只支持wml并且不支持html那一定是移动设备
			// 如果支持wml和html但是wml在html之前则是移动设备
			if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
			{
				return true;
			}
		}
		return 'unknown_wap';
	}
}