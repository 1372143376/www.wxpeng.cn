<?php
namespace cg;

class sms
{
	public static $dict_sn = array(
		1 => 'SDK-QZT-010-00075,668212',
		2 => 'SDK-QZT-010-00077,368828',
		3 => 'SDK-QZT-010-00078,341813',
		4 => 'SDK-QZT-010-00083,988950',
		5 => 'SDK-QZT-010-00198,333526',
		6 => 'SDK-QZT-010-00132,233708' //不能接受回复? 没有扩展子号权限 ext 必须为空
	);
	public static $dict_error = array(
		'1' => '没有需要取得的数据',
		'-1' => '重复注册',
		'-2' => '帐号/密码不正确',
		'-4' => '余额不足支持本次发送',
		'-5' => '数据格式错误',
		'-6' => '参数有误',
		'-7' => '权限受限',
		'-8' => '流量控制错误',
		'-9' => '扩展码权限错误',
		'-10' => '内容长度长',
		'-11' => '内部数据库错误',
		'-12' => '序列号状态错误',
		'-13' => '没有提交增值内容',
		'-14' => '服务器写文件失败',
		'-15' => '文件内容base64编码错误',
		'-16' => '返回报告库参数错误',
		'-17' => '没有权限',
		'-18' => '上次提交没有等待返回不能继续提交',
		'-19' => '禁止同时使用多个接口地址',
		'-20' => '相同手机号，相同内容重复提交',
		'-22' => 'Ip鉴权失败'
	);
	private static $send_url = 'http://sdk2.zucp.net:8060/webservice.asmx/mt';
	private static $receive_url = "http://sdk2.zucp.net:8060/webservice.asmx/mo";
	private static $balance_url = "http://sdk2.zucp.net:8060/webservice.asmx/balance";
	public static $sn_id = 0;

	public static function send($mobile, $content, $ext = '')
	{
		$post_data = self::get_sn_pwd($ext);
		$content = iconv("utf-8", 'gbk', $content);
		$post_data['mobile'] = $mobile;
		$post_data['content'] = $content;
		$post_data['ext'] = self::$sn_id == '6' ? '' : $ext;
		$post_data['stime'] = '';
		$post_data['rrid'] = '';
		$result = self::post(self::$send_url, $post_data);
		$matches = array();
		preg_match('/>(.*?)<\/string>/i', $result, $matches);
		return $matches[1];
	}

	private static function get_sn_id($ext = '')
	{
		if ($ext == '')
		{
			$s = '1';
		}
		else
		{
			$s = '1';
		}
		$a = explode(',', $s);
		$k = array_rand($a);
		return $a[$k];
	}

	public static function receive()
	{
		$data = array();
		foreach (self::$dict_sn as $id => $line)
		{
			self::set_sn_id($id);
			$post_data = self::get_sn_pwd('');
			$data[$post_data['sn']] = self::post(self::$receive_url, $post_data);
		}
		return $data;
	}

	public static function balance()
	{
		$data = array();
		foreach (self::$dict_sn as $id => $line)
		{
			self::set_sn_id($id);
			$post_data = self::get_sn_pwd('');
			$result = self::post(self::$balance_url, $post_data);
			$matches = array();
			preg_match('/>(.*?)<\/string>/i', $result, $matches);
			$balance = intval($matches[1]);
			if ($balance < 0)
			{
				$balance = 0;
			}
			$data[$post_data['sn']] = $balance;
		}
		return $data;
	}

	public static function set_sn_id($sn_id)
	{
		self::$sn_id = $sn_id;
	}

	private static function get_sn_pwd($ext = '')
	{
		if (self::$sn_id == 0)
		{
			self::$sn_id = self::get_sn_id($ext);
		}
		$line = self::$dict_sn[self::$sn_id];
		list($sn, $pwd) = explode(',', $line);
		$pwd = strtoupper(md5($sn . $pwd));
		return array(
			'sn' => $sn,
			'pwd' => $pwd
		);
	}

	private static function post($url, $data)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		$agent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36";
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		return curl_exec($ch);
	}
}