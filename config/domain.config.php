<?php

/**
 * 域名与apps目录对应关系
 */
$domain_config = [
	'www.wxpeng.cn' => 'backend',
	'backend.wxpenng.cn' => 'home',
    'blog.wxpeng.cn' => 'blog'
];
return $domain_config;
