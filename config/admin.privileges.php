<?php
/**
 * 管理后台菜单配置
 * 右侧子导航名数量为1或者名称为空则不显示
 *
 * 顶级菜单1 = [
 *    左侧菜单名1|controller|action|右侧子导航名1|是否显示
 *    左侧菜单名1|controller|action|右侧子导航名2|是否显示
 *    左侧菜单名2|controller|action|右侧子导航名|是否显示
 *    左侧菜单名3|controller|action|右侧子导航名|是否显示
 * ],
 * 顶级菜单2 = [
 *    左侧菜单名1|controller|action|右侧子导航名1|是否显示
 *    左侧菜单名1|controller|action|右侧子导航名2|是否显示
 *    左侧菜单名2|controller|action|右侧子导航名|是否显示
 *    左侧菜单名3|controller|action|右侧子导航名|是否显示
 * ]
 */
return [
	'首页' => [
		'首页|index|index|首页|1',
		'通知中心|notification|index|未读|0',
		'通知中心|notification|read|已读|0',
		'phpinfo|index|phpinfo|php info|1',
		'phpinfo|index|iframe_phpinfo|phpinfo|0',
		'缓存管理|index|cache|缓存清理|1',
		'缓存管理|index|clear_cache|缓存清理|0',
		'用户管理|user|list|用户列表|1',
		'用户管理|user|add|添加用户|1',
		'用户管理|user|edit|编辑用户|0',
		'组管理|group|list|后台用户组列表|1',
		'组管理|group|edit|编辑用户组权限|0',
		'个人设置|index|account|个人设置|1',

		'登录日志|index|logs_login|登录日志|1',
		'服务|image|view|查看图片|0'
	],
	'分类管理' => [
		'菜谱分类|cp_category|index|分类管理-列表|1',
		'菜谱分类|cp_category|add|分类管理-添加|1',
		'菜谱分类|cp_category|insert|分类管理-insert|0',
		'菜谱分类|cp_category|edit|分类管理-编辑|0',
		'菜谱分类|cp_category|update|分类管理-update|0',
		'菜谱分类|cp_category|update|分类管理-更新|0',
		'菜谱分类|cp_category|delete|分类管理-删除|0',


		'饮食健康|jk_category|index|分类管理-列表|1',
		'饮食健康|jk_category|add|分类管理-添加|1',
		'饮食健康|jk_category|insert|分类管理-insert|0',
		'饮食健康|jk_category|edit|分类管理-编辑|0',
		'饮食健康|jk_category|update|分类管理-update|0',
		'饮食健康|jk_category|update|分类管理-更新|0',
		'饮食健康|jk_category|delete|分类管理-删除|0',

		'食材百科|sc_category|index|分类管理-列表|1',
		'食材百科|sc_category|add|分类管理-添加|1',
		'食材百科|sc_category|insert|分类管理-insert|0',
		'食材百科|sc_category|edit|分类管理-编辑|0',
		'食材百科|sc_category|update|分类管理-update|0',
		'食材百科|sc_category|update|分类管理-更新|0',
		'食材百科|sc_category|delete|分类管理-删除|0'
	],
];
