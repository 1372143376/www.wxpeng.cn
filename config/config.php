<?php
use cg\core\caching\cache_mem_server;

/**
 * include:
 * system_config
 * router_config
 * db_config
 *
 */
class config
{
	public static $config;
	public static $ERROR_404_DOCUMENT = '/web/404.php';
	public static $ERROR_404_ROUTE = '/error/404';
	private static $inited;
	private static $application_name = 'water_rate'; //缓存key前缀，可以为空，多个cgbt共用memcached则需要区分
	private static $system_salt_key = 'xxxxaaaabbbb1111'; //系统唯一key
	private static $system_founder = 'admin'; //创始人，可以登录后台

	public static function init()
	{
		if (self::$inited)
		{
			return;
		}
		self::$inited = true;
		self::domain_config();
		self::system_config();
		self::router_config();
		self::db_config();
		self::cache_config();
		self::other_config();
		self::cookie_config();
		self::admin_privileges_config();
	}

	public static function cookie_config()
	{
		self::$config['cookie'] = array(
			'cookie_prefix' => 'quxiangtou_',
			'salt' => true,
			'salt_key' => self::$system_salt_key,
			'salt_agent' => false,
			'salt_ip' => false,
			'path' => '/',
			'secure' => false,
			'httponly' => true
		);
		self::$config['dict_cookie_domain'] = array(
			'water.zuofan.cn' => '.zuofan.cn'
		);
		$cookie_domain = '';
		if (isset(self::$config['dict_cookie_domain'][$_SERVER['HTTP_HOST']]))
		{
			$cookie_domain = self::$config['dict_cookie_domain'][$_SERVER['HTTP_HOST']];
		}
		self::$config['cookie']['domain'] = $cookie_domain;
	}

	public static function db_config()
	{
		self::$config['db'] = include 'db.config.php';
	}

	public static function cache_config()
	{
		$cache_config = array();
		$cache_config['cache_type'] = 'memcached';
		if (DIRECTORY_SEPARATOR == '\\')
		{
			// 兼容 windows 下 php memcache
			$cache_config['cache_type'] = 'memcache';
		}
		$cache_config['file'] = array(
			'cache_dir' => 'cache'
		);
		$cache_config['memcache'] = [
			'key_prefix' => self::$application_name,
			'server' => [
				0 => new cache_mem_server([
					"host" => "127.0.0.1",
					"port" => "11211",
				])
			]
		];
		self::$config['cache'] = $cache_config;
	}

	public static function router_config()
	{
		$r = include 'router.config.php';
		self::$config['router'] = isset($r[self::$config['domain'][$_SERVER['HTTP_HOST']]]) ? $r[self::$config['domain'][$_SERVER['HTTP_HOST']]] : array();
	}

	public static function domain_config()
	{
		self::$config['domain'] = include 'domain.config.php';
	}

	public static function admin_privileges_config()
	{
		self::$config['admin_privileges'] = include 'admin.privileges.php';
	}

	public static function system_config()
	{
		// 程序的根目录
		$system_config['APP_ROOT'] = self::fix_dir(substr(dirname(__FILE__), 0, -7) . DIRECTORY_SEPARATOR);
		// 项目的名字
		$system_config['APP_NAME'] = self::$config['domain'][$_SERVER['HTTP_HOST']];
		// 每个网站的根目录
		$system_config['APP_PATH'] = $system_config['APP_ROOT'] . 'apps/' . $system_config['APP_NAME'] . '/';
		// 控制器的命名空间
        $system_config['CONTROLLER_NAMESPACE'] = '\\apps\\' . self::$config['domain'][$_SERVER['HTTP_HOST']] . '\\controller';

		$protocol = self::is_ssl() ? 'https' : 'http';
		$system_config['APP_URL'] = $protocol . '://' . $_SERVER['HTTP_HOST'];
		if (substr($system_config['APP_URL'], -1) != '/')
		{
			$system_config['APP_URL'] .= '/';
		}
		$system_config['ERROR_404_DOCUMENT'] = self::$ERROR_404_DOCUMENT;
		$system_config['ERROR_404_ROUTE'] = self::$ERROR_404_ROUTE;
		$system_config['AUTO_ROUTE'] = true;
		self::$config['system'] = $system_config;
	}

	public static function other_config()
	{
		self::$config['system_salt_key'] = self::$system_salt_key;
		self::$config['system_founder'] = self::$system_founder;
		self::$config['lang'] = array(
			'default' => 'cn',
			'multi_lang' => true,
			'all' => array(
				'en' => 'en-us',
				'cn' => 'zh-cn',
				'tw' => 'zh-tw'
			)
		);
		self::$config['view'] = array(
			'all_templates' => array(
				'default' => array(
					'name' => '默认',
					'engine' => 'cg'
				)
			)
		);

		self::$config['forums'] = array(
			'db_config_name' => 'discuzx',
			'uc_config' => array(
				'db_config_name' => 'discuzx',
				'uc_api' => 'http://discuzx.xmeise.com/uc_server',
				'uc_key' => 'xxxxxx',
				'uc_appid' => '2',
				'uc_ip' => '192.168.0.100'
			)
		);
	}

	private static function fix_dir($dir)
	{
		return str_replace('\\', '/', $dir);
	}

	private static function is_ssl()
	{
		//sometimes, php can't detect https
		if (isset($_SERVER['HTTPS']))
		{
			if ($_SERVER['HTTPS'] === 1)
			{
				return TRUE;
			}
			elseif ($_SERVER['HTTPS'] === 'on')
			{
				return TRUE;
			}
		}

		if ($_SERVER['SERVER_PORT'] == 443)
		{
			return TRUE;
		}
		return FALSE;
	}
}

