<?php
namespace model\water;

use cg\core\cg_model;
use cg\core\cg;

class base extends cg_model
{
    public function __construct()
    {
        $this->db_config_name = 'water';
        $this->table_prefix = cg::config()->config['db'][$this->db_config_name]['table_prefix'];
    }

	/**
	 * 返回分页数据
	 * @param array $where 查询条件
	 * @param integer $page 第几页
	 * @param string $other_sql  排序 sql
	 * @param integer $page_size 一页多少个
	 * @return array
	 */
	public function pagination($where, $page, $other_sql = '', $page_size = 10)
	{
		if (!empty($where))
		{
			$where_sql = implode(' and ', $where);
		}
		else
		{
			$where_sql ='id >0';
		}
		$sql = "select count(1) from $this->table where $where_sql";
		$total = $this->db()->createCommand($sql)->queryScalar();

		if ($page < 1)
		{
			$page = 1;
		}
		// 总共可分多少页
		$total_page = ceil($total / $page_size);
		// 下一页页数, 为0代表没有下一页
		$next_page = $page + 1;
		if ($next_page > $total_page)
		{
			$next_page = 0;
		}

		// 从哪开始
		$start = ($page - 1) * $page_size;
		$sql = "select * from $this->table where $where_sql $other_sql limit $start,$page_size";
		$rows = $this->db()->createCommand($sql)->queryAll();

		return [
			'rows' => $rows,
			'next_page' => $next_page,
			'total_page' => $total_page
		];
	}
}
