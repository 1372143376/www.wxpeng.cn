<?php

namespace model\water;

class audit_log extends base
{
	public function __construct()
	{
		parent::__construct();
		$this->table = $this->table('audit_log');
		$this->pk = 'id';
	}

	/**
	 *
	 * @return audit_log
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}

}