<?php

namespace model\water;

class tests extends base
{
	public function __construct()
	{
		parent::__construct();
		$this->table = $this->table('tests');
	}

	/**
	 *
	 * @return tests
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}

}