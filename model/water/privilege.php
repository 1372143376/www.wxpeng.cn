<?php

namespace model\water;

class privilege extends base
{
	public function __construct()
	{
		parent::__construct();
		$this->table = $this->table('privilege');
		$this->pk = 'id';
	}

	/**
	 *
	 * @return privilege
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}

	public function get_privilege_by_pid($pid)
	{
		$sql = "select * from $this->table where id in ($pid) and is_del = 0";
		return $this->db()->createCommand($sql)->queryAll();
	}

	public function get_by_action_name($action_name)
	{
		$sql = "select * from $this->table where action_name = '$action_name' and parent_id = 0";
		return $this->db()->createCommand($sql)->queryOne();
	}

}