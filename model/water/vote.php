<?php

namespace model\water;

class vote extends base
{
	public function __construct()
	{
		parent::__construct();
		$this->table = $this->table('vote');
		$this->pk = 'id';
	}

	/**
	 *
	 * @return vote
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}

	public function get_use_by_account($votename)
	{
		$sql = "select * from $this->table where account = '$votename' and status = 0";
		return $this->db()->createCommand($sql)->queryOne();
	}

}