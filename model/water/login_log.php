<?php

namespace model\water;

class login_log extends base
{
	public function __construct()
	{
		parent::__construct();
		$this->table = $this->table('login_log');
		$this->pk = 'id';
	}

	/**
	 *
	 * @return login_log
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}

	/*
	 * 获取最近一次登录信息
	 */
	public function get_last_login($account)
	{
		$sql = "select * from $this->table where account = '$account' order by created_at desc limit 1";
		return $this->db()->createCommand($sql)->queryAll();
	}
}