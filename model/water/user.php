<?php

namespace model\water;

class user extends base
{
	public function __construct()
	{
		parent::__construct();
		$this->table = $this->table('user');
		$this->pk = 'id';
	}

	/**
	 *
	 * @return user
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}

	public function get_use_by_account($username)
	{
		$sql = "select * from $this->table where account = '$username' and status = 0";
		return $this->db()->createCommand($sql)->queryOne();
	}

}