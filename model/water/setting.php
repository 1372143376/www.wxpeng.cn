<?php

namespace model\water;

class setting extends base
{
	public function __construct()
	{
		parent::__construct();
		$this->table = $this->table('setting');
		$this->pk = 'id';
	}

	/**
	 *
	 * @return user
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}

}