<!-- index -->
<div class="index">
    <div class="place">
        <span>位置: <a href="/" style="color: #014c8c">首页</a></span>
    </div>

    <div class="mainindex">


        <div class="welinfo">
            <span><img src="/static/backend/images/sun.png" alt="天气"/></span>
            <b><?= isset($name) ? $name : '**'; ?>，欢迎使用信息管理系统</b>
            <a href="/user/index/">帐号设置</a>
        </div>

        <div class="welinfo">
            <span><img src="/static/backend/images/time.png" alt="时间"/></span>
            <i><?php echo !isset($login_log[0]['created_at']) ? '您是第一次登录哦！' : '您上次登录的时间:' . $login_log[0]['created_at'] . '&nbsp;&nbspip:' . $login_log[0]['ip']; ?></i>
            <!--   （不是您登录的？<a href="#">请点这里</a>）-->
        </div>
        <div class="xline"></div>
        <div class="box">
        </div>

        <div class="welinfo">
            <span><img src="/static/backend/images/dp.png" alt="提醒"/></span>
            <b>最近的操作日志</b>
        </div>
        <div class="xline"></div>
		<?php if ($is_admin == 1): ?>
			<?php foreach ($audit_log['rows'] as $data): ?>
                <ul class="iconlist">
                    <li><p><?= $data['id']; ?>、<?= json_decode($data['action'], true)[0] ?></p>
                        --------<?= $data['created_at'] ?></li>
                </ul>
			<?php endforeach; ?>
            <div class="pagin">
                <div class="message">共<i class="blue"><?= $audit_log['total_page'] ?></i>条记录，当前显示第&nbsp;<i
                            class="blue"><?=$page?>&nbsp;</i>页
                </div>
                <ul class="paginList">
                    <li class="paginItem"><a href="/index/index/?page=<?= $audit_log['next_page'] - 2 ?>"><span
                                    class="pagepre"></span></a></li>
					<?php for ($i = 1; $i <= $audit_log['total_page']; $i++): ?>
                        <li class="paginItem <?= $page == $i? 'current' :'';?>"><a href="/index/index/?page=<?= $i ?>"><?= $i ?></a></li>
					<?php endfor; ?>
                    <li class="paginItem"><a href="/index/index/?page=<?= $audit_log['next_page'] ?>"><span
                                    class="pagenxt"></span></a></li>
                </ul>
            </div>
		<?php endif; ?>
        <div class="box">
        </div>
        <div class="xline"></div>
        <div class="box">
        </div>
        <div class="welinfo">
            <span><img src="/static/backend/images/vote.png" alt="提醒"/></span>
            <b>点赞</b>
        </div>
        <div id=mood>
            <ul></ul>
        </div>
    </div>
    <div class="clear"></div>

</div>
<script type="text/javascript">
    $(function(){

/*        var sUserAgent = navigator.userAgent.toLowerCase();
        var isAndroid = sUserAgent.match(/android/i) == 'android';
        var isIos = sUserAgent.match(/iphone os/i) == 'iphone os';
        if(isAndroid||isIos)
        {
            //移动端
            alert(1);
        }
        else
        {
            //pc
        }*/


        $.ajax({
            type: 'GET',
            url: '/index/vote',
            cache: false,
            data: 'id='+<?= isset($member_id) ? $member_id : 888; ?>,
            dataType: 'json',
            error: function(){
                alert('出错了！');
            },
            success: function(json){
                if(json.msg){
                    console.log(json);
                    $.each(json.msg,function(index,array){

                        var str = "<li><span>"+array['mood_val']+"</span><div class=\"pillar\" style=\"height:"+array['height']+"px;\"></div><div class=\"face\" rel=\""+array['mid']+"\"><img src=\"/static/backend/images/"+array['mood_pic']+"\"><br/>"+array['mood_name']+"</div></li>";
                        $("#mood ul").append(str);
                    });
                }
            }
        });
        $(".face").live('click',function(){
            var face = $(this);
            var mid = face.attr("rel");
            var value = face.parent().find("span").html();
            var val = parseInt(value)+1;
            $.post("mood.php?action=send",{moodid:mid,id:2},function(data){
                if(data>0){
                    face.prev().css("height",data+"px");
                    face.parent().find("span").html(val);
                    face.find("img").addClass("selected");
                }else{
                    alert(data);
                }
            });
        });
    });
</script>