<?php
/**
 * @var DebugBar\JavascriptRenderer $debug_bar_renderer ;
 * @var cg\core\Template $this
 * @var cg\core\assets\manager $assets
 */

$this->partial('header');
?>

<?php
$this->partial('left');
?>

<?= $this->get_content() ?>
