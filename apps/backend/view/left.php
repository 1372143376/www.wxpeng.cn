﻿<?php if (!empty($id)): ?>
    <div class="left">
        <div class="lefttop" style="background-color: #046DA6">
            <span></span><?php echo isset($menu[0][$id]['privilege_name']) ? $menu[0][$id]['privilege_name'] : '功能'; ?>
        </div>
        <div class="xline"></div>
<?php if(isset($menu[$id])):?>
		<?php foreach ($menu[$id] as $k => $val): ?>
            <div class="lefttop"><span></span><a href="<?= $val['action_name'] ?>"><?= $val['privilege_name'] ?></a>
            </div>
            <div class="xline"></div>
		<?php endforeach; ?>
<?php endif;?>
    </div>
<?php else: ?>
    <div class="left">
        <div class="lefttop" style="background-color: #046DA6">
            <span></span>全部功能
        </div>
        <div class="xline"></div>

		<?php foreach ($menu as $k => $vals): ?>
			<?php foreach ($vals as $val): ?>
                <div class="lefttop" <?php if($val['parent_id'] == 0):?>style = "background-color:dimgrey" <?php endif;?>><span></span><a href="<?= $val['action_name'] ?>"><?= $val['privilege_name'] ?></a>
                </div>
                <div class="xline"></div>
			<?php endforeach; ?>
		<?php endforeach; ?>
    </div>
<?php endif; ?>
