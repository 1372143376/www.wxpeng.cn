﻿<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>欢迎登录后台管理系统</title>
<link href="/static/backend/css/style.css" rel="stylesheet" type="text/css"/>
<script language="JavaScript" src="/static/common/js/jquery-1.7.2.min.js"></script>
<script src="/static/backend/js/cloud.js" type="text/javascript"></script>

<script language="javascript">
    $(function(){
        $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
        $(window).resize(function(){
            $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
        })
    });
</script>

<body style="background-color:#1c77ac; background-image:url(/static/backend/images/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;">


<div id="mainBody">
    <div id="cloud1" class="cloud"></div>
    <div id="cloud2" class="cloud"></div>
</div>


<div class="logintop">
    <span>欢迎登录后台管理界面平台</span>
    <ul>
        <li><a href="/">回首页</a></li>
        <li><a href="#">帮助</a></li>
        <li><a href="#">关于</a></li>
    </ul>
</div>

<div class="loginbody">

    <span class="systemlogo"></span>

    <div class="loginbox">

        <ul>
            <li><input name="user" type="text" class="loginuser" value="" placeholder="用户名"
                       onclick="JavaScript:this.value=''"/></li>
            <li><input name="pwd" type="password" class="loginpwd" value="" placeholder="密码"
                       onclick="JavaScript:this.value=''"/></li>
            <li><input name="" type="button" class="loginbtn" value="登录"/>
                <label><input name="" type="checkbox" value="" checked="checked"/>记住密码

                </label>
                <label>                    <a target="_blank" href="http://sighttp.qq.com/authd?IDKEY=cca7095adef8edea8987e1b3205d7195024e03c6b28c9219">
                        <img border="0"  src="http://wpa.qq.com/imgd?IDKEY=cca7095adef8edea8987e1b3205d7195024e03c6b28c9219&pic=52"
                             alt="点击这里给我发消息" title="点击这里给我发消息"/></a>忘记密码？</a></label></li>
        </ul>


    </div>

</div>


<div class="loginbm">copyright@</div>
</body>
<script>
    $(function () {
        $('.loginbtn').click(function () {
            var user = $('.loginuser').val();
            var pwd = $('.loginpwd').val();
            var url = "/site/verify/?pwd=" + pwd + "&user=" + user;
            $.getJSON(url, function (data) {
                if (data.error === true) {
                    location.href = '/index/index/';
                }
                alert(data.msg);
            });
        })
    })
</script>