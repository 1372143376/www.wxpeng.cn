﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<?= $assets->outputCss('header') ?>
	<?= $assets->outputJs('header') ?>
    <style type="text/css">
        /*left*/
        .top{
            position: fixed;
            top:0;
            left:0;
            width: 100%;
        }
        .left{
            position: fixed;
            left:0;
            top:88px;
        }
        .index{
            position: fixed;
            top: 88px;
            left: 788px;
            min-width: 980px;
        }
        .place,.mainindex{
            margin-left: -600px;
        }
        #menuson li,#menuson{
            float: none;
            background: #f0f9fd;
        }
        #menuson li a{
            color:#000;
        }
        .topright ul li a{
            color:#000;
        }
        .topright ul li a:hover {
            color: #000;
        }

    </style>
</head>
<body style="background:url(/static/backend/images/topbg.gif) repeat-x;">
<!-- top -->
<div class="top">
    <div class="topleft">
        <a href="/" target="_parent"><img src="/static/backend/images/logo.png" title="系统首页"/></a>
    </div>

    <ul class="nav">

        <?php foreach ($menu[0] as $k => $val):?>
            <li><a href="<?=$val['action_name']?>" ><img src="<?=$val['img_url']?>" title="<?=$val['privilege_name']?>"/>
                    <h2><?=$val['privilege_name']?></h2></a></li>
        <?php endforeach;?>
    </ul>

    <div class="topright">
        <ul>
            <li><span><img src="/static/backend/images/help.png" title="帮助" class="helpimg"/></span><a href="#">帮助</a>
            </li>
            <li><a href="#">关于</a></li>
            <li><a href="/site/logout/" target="_parent">退出</a></li>
        </ul>
        <div class="user">
            <span><?= isset($name) ? $name : '**'; ?></span>
           <!-- <i>消息</i>
            <b>5</b>-->
        </div>
    </div>
</div>