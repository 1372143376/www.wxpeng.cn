<style type="text/css">        table {
        border-collapse: collapse;
    }

    td {
        border: 1px solid #ccc;
    }
</style>
<div class="index">
    <div class="place">
        <span>位置：</span>
        <ul class="placeul">
            <li><a href="/">首页</a></li>
            <li><a href="/user/index">用户管理</a></li>
        </ul>
    </div>

    <div class="formbody">
        <table id="tb" class="tablelist">
            <thead>
            <tr>
                <td style="width: 35px;"><input id="chkAll" type="checkbox"/></td>
                <td>
                    <button id="chkAll"> 反/全选</button>
                </td>
            </tr>
            </thead>
            <tbody>
			<?php if (empty($user_info))
			{
				$pid = [];
			}
			else
			{
				$pid = explode(',', $user_info['pid']);
				echo '<input id="userid" type="hidden" value="' . $user_info['id'] . '">';
			} ?>
			<?php foreach ($all_privilege as $val): ?>

                <tr>
                    <td><input name="chkItem" type="checkbox"
                               value="<?= $val['id'] ?>" <?= in_array($val['id'], $pid) ? 'checked' : ''; ?>/></td>
                    <td>
                        <a style='color: red;'><?= $val['parent_id'] > 0 ? '--->' . $val['parent_id'] : $val['id']; ?></a> <?= $val['privilege_name'] ?>
                    </td>
                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
        <button class="btn btn-primary is_disabled audit check" id="btn_submit" data-tag=-3>修改用户权限</button>
    </div>

</div>
<script type="text/javascript">
    $(function () {
        // chkAll全选事件
        $("#chkAll").bind("click", function () {
            $("[name = chkItem]:checkbox").attr("checked", this.checked);
        });
        // chkItem事件
        $("[name = chkItem]:checkbox").bind("click", function () {
            var $chk = $("[name = chkItem]:checkbox");
            $("#chkAll").attr("checked", $chk.length == $chk.filter(":checked").length);
        });
        //提交事件
        $("#btn_submit").click(function () {
            var pid = '';
            $.each($('input[name = chkItem]:checkbox:checked'), function () {
                pid += $(this).val() + ',';
            });
            if (pid === '') {
                alert('用户不能没有权限!');
            }
            var is_del = confirm('确定要修改用户该权限?');
            if (is_del) {
                var id = $('#userid').val();
                console.log(id);
                $.getJSON('/user/edit_privilege/?id='+id+'&privilege_id=' + pid, function (data) {
                    if (data.status === 500) {
                        alert(data);
                    }
                    else {
                        location.reload();
                    }
                });
                return;
            }
        })
    });
</script>