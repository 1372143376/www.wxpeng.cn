<link rel="stylesheet" type="text/css" href="/static/backend/css/xcConfirm.css"/>
<script type="text/javascript" src="/static/backend/js/jquery.idTabs.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".click").click(function () {
            $(".tip").fadeIn(10);
        });

        $(".tiptop a").click(function () {
            $(".tip").fadeOut(200);
        });

        $(".sure").click(function () {
            $(".tip").fadeOut(100);
        });

        $(".cancel").click(function () {
            $(".tip").fadeOut(100);
        });

    });
</script>
<!-- index -->
<div class="index">
    <div class="place">
        <span>位置：</span>
        <ul class="placeul">
            <li><a href="/">首页</a></li>
            <li><a href="/bill/bill_list">账单列表</a></li>
        </ul>
    </div>
    <div class="rightinfo">
        <table class="tablelist">
            <caption>账单，是按创建时间排序</caption>
            <thead>
            <tr>
                <th>ID<i class="sort"><img src="/static/backend/images/px.gif"/></i></th>
                <th>用户</th>
                <th>是否结算</th>
                <th>几号机井</th>
                <th>所属年份</th>
                <th>开始时间</th>
                <th>结束时间</th>
                <th>备注</th>
                <th>合计时间</th>
                <th>应支付</th>

                <th>结算操作</th>
            </tr>
            </thead>
            <tbody>
			<?php foreach ($order_list as $val): ?>
                <tr>
                    <td><?= $val['id'] ?></td>
                    <td><?= $val['user'] ?></td>
                    <td><?= $val['status'] == 1 ? '<span style="color: red">已结算</span>' : '未结算' ?></td>
                    <td><?= $val['type'] ?></td>
                    <td><?= $val['year'] ?></td>
                    <td><?= $val['start_time'] ?></td>
                    <td><?= $val['end_time'] ?></td>
                    <td><?= $val['remark'] ?></td>
                    <td><?= $val['count'] ?></td>
                    <td><span style="padding-bottom:1px; border-bottom:1px solid firebrick;"><?= $val['price'] ?>
                            元</span></td>
                    <td><a href="" class="tablelink">结算</a> <a
                                href="" class="tablelink">该用户总结算</a></td>
                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $('.tablelist tbody tr:odd').addClass('odd');
</script>
</body>

