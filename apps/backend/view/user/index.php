<div class="index">
    <div class="place">
        <span>位置：</span>
        <ul class="placeul">
            <li><a href="/">首页</a></li>
            <li><a href="/user/index">用户管理</a></li>
        </ul>
    </div>
    <div class="rightinfo">
        <table class="tablelist">
            <thead>
            <tr>
                <th>ID</th>
                <th>账号</th>
                <th>姓名</th>
                <th>状态</th>
                <th>创建时间</th>
                <th>拥有权限</th>

                <th>操作</th>
            </tr>
            <thead>
            <tbody>
			<?php foreach ($all_user

			as $val): ?>
            <tr>
                <td><?= $val['id'] ?></td>
                <td><?= $val['account'] ?></td>
                <td>
					<?= $val['username'] ?>
                </td>
                <td><?= $val['status'] == 1 ? '停用' : '启用' ?></td>
                <td><?= $val['create_time'] ?></td>
                <td>
                    <ul class="talk-img clearfix">
						<?php $pid_array = array_filter(explode(',', $val['pid'])); ?>
						<?php foreach ($pid_array as $pid): ?>
							<?php if (!isset($all_privilege[$pid]['privilege_name']))
							{
								continue;
							} ?>
                            <li value="<?= $pid ?>" class="hotTags">
                                <a href=""><?= $all_privilege[$pid]['privilege_name'] ?></a>
                                <span>x</span>
                            </li>
						<?php endforeach; ?>
                    </ul>
                </td>

                <td>
                    <input type='hidden' value="<?= $val['id'] ?>"/>
                    <button class="btn btn-primary is_disabled audit del" id="btn_submit" data-tag=-3>删除用户</button>
                    <a href="/user/add/?id=<?= $val['id'] ?>">
                        <button class="btn btn-primary is_disabled audit add" id="btn_submit" data-tag=-3>用户加权限
                        </button>
                    </a>
                </td>
            </tr>
            <tbody>
			<?php endforeach; ?>
        </table>
    </div>
</div>
<script>
    $('.tablelist tbody tr:odd').addClass('odd');
    $(function () {
        $(".talk-img li span").on("click", function () {
            var is_del = confirm('确定要删除用户该权限?');
            if (is_del) {
                var privilege_id = $(this).parent('li').val();
                var id = $(this).parent().parent().parent().next().find('input').val();
                $.getJSON('/user/del_privilege/?id=' + id + '&privilege_id=' + privilege_id, function (data) {
                    if (data.status === 500) {
                        alert(data);
                    }
                    else {
                        $(this).parent("li").remove();
                        location.reload();
                    }
                });
                return;
            }
        });

        //删除用户
        $('.del').click(function () {
            var is_del = confirm('确定要删除该用户?');
            if (is_del) {
                var id = $(this).prev('input').val();
                $.getJSON('/user/del/?id=' + id, function (data) {
                    if (data.status === 500) {
                        alert(data);
                    }
                    else {
                        alert(data.msg);
                        location.reload();
                    }
                });
                return;
            }
        });


    });
</script>