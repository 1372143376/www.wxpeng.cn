<link rel="stylesheet" type="text/css" href="/static/backend/css/shijian.css"/>
<script src="/static/backend/js/jquer_shijian.js?ver=1" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/static/backend/js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="/static/backend/js/select-ui.min.js"></script>
<script type="text/javascript">
    KE.show({
        id: 'content7',
        cssPath: './index.css'
    });
</script>
<script type="text/javascript">
    $(document).ready(function (e) {
        $(".select1").uedSelect({
            width: 345
        });
    });
</script>
<!-- index -->
<div class="index">
    <div class="place">
        <span>位置：</span>
        <ul class="placeul">
            <li><a href="/">首页</a></li>
            <li><a href="/bill/index">账单系统</a></li>
        </ul>
    </div>

    <div class="formbody">


        <div id="usual1" class="usual">

            <div class="itab">
                <ul>
                    <li><a href="/bill/add" class="selected">写帐</a></li>
                </ul>
            </div>

            <div id="tab1" class="tabson">

                <ul class="forminfo">
                    <li>
                        <label><b>*</b>姓名<b>*</b></label>
                        <input name="name" type="text" class="dfinput"
                               value="<?= isset($info['privilege_name']) ? $info['privilege_name'] : ''; ?>"
                               style="width:518px;"/><i>姓名，尽量避免同音</i>
                    </li>
                    <li><label><b>*</b>机井<b>*</b></label>


                        <div class="vocation">
                            <select class="select1" name="type" style="width: 345px;">
                                <option value="0"></option>
								<?php foreach ($well as $k => $val): ?>
                                    <option value="<?= $k ?>">
										<?= $val ?>
                                    </option>
								<?php endforeach; ?>
                            </select>
                        </div>
                        <i>不能为空，所使用的机井</i>

                    </li>
                    <li>
                        <label><b>*</b>开始的时间<b>*</b></label>
                        <input type="text" name="start_time" id="input1" class="dfinput" value="" style="width:518px;"/>
                    </li>
                    <li>
                        <label><b>*</b>结束的时间<b>*</b></label>
                        <input type="text" name="end_time" id="input2" class="dfinput" value="" style="width:518px;"/>
                    </li>
                    <li>
                        <label><b>*</b>是否结算<b>*</b></label>
                        <input name="status" type="radio"
                               value="1" />是&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="status" type="radio"
                               value="0" checked/>否
                    </li>
                    <li>
                        <label><b>*</b>使用年份<b>*</b></label>
                        <input name="year" type="radio"
                               value="2018" checked/>2018年&nbsp;&nbsp;&nbsp;

                    </li>
                    <li>
                        <label>备注<b></b></label>
                        <textarea name="remark" type="text" class="dfinput"
                                  style="width:400px; height: auto;"></textarea>
                    </li>
                    <li><label>&nbsp;</label><input name="" type="button" class="btn" value="确定"/></li>
                </ul>
                <input id="action" type="hidden" value="<?= $action ?>">
                <input id="idd" type="hidden" value="<?= isset($info['id']) ? $info['id'] : ''; ?>">
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $("#usual1 ul").idTabs();
    $(function () {
        //默认点击显示
        //时间插件
        $("#input1").shijian();
        $("#input2").shijian();

        $('.btn').click(function () {
            var name = $("input[name='name']").val();
            var type = $("select[name='type']").val();
            var start_time = $("input[name='start_time']").val();
            var end_time = $("input[name='end_time']").val();
            var status = $("input[name='status']").val();
            var remark = $("textarea[name='remark']").val();
            var year = $("input[name='year']").val();
            var action = $("#action").val();
            var id = $("#idd").val();
            if (name === '' || type === '' || start_time === '' || end_time === '' || action === '') {
                alert('有红点的选项,不能为空！');
                return;
            }
            if (end_time <= start_time)
            {
                alert('结束时间不能小于开始时间');
                return;
            }
            $.ajax({
                url: '/bill/data/',
                datatype: 'json',
                type: 'post',
                data: {
                    "data": 'action=' + action + '&name=' + name + '&type=' + type + '&start_time=' + start_time + '&end_time=' + end_time + '&status=' + status + '&year=' + year + '&remark=' + remark + '&id=' + id
                },
                success: function (data) {
                    alert(data);
                    location.href = '/bill/index/';
                }
            })
        })

    })
</script>