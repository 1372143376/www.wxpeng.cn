<script type="text/javascript" src="/static/backend/js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="/static/backend/js/select-ui.min.js"></script>
<script type="text/javascript">
    KE.show({
        id: 'content7',
        cssPath: './index.css'
    });
</script>

<script type="text/javascript">
    $(document).ready(function (e) {
        $(".select1").uedSelect({
            width: 345
        });
    });
</script>
<!-- index -->
<div class="index">
    <div class="place">
        <span>位置：</span>
        <ul class="placeul">
            <li><a href="/">首页</a></li>
            <li><a href="/operator/index">系统设置</a></li>
        </ul>
    </div>

    <div class="formbody">


        <div id="usual1" class="usual">

            <div class="itab">
                <ul>
                    <li><a href="#tab1" class="selected">添加权限</a></li>
                </ul>
            </div>

            <div id="tab1" class="tabson">

                <ul class="forminfo">
                    <li>
                        <label><b>*</b>权限名称<b>*</b></label>
                        <input name="name" type="text" class="dfinput"
                               value="<?= isset($info['privilege_name']) ? $info['privilege_name'] : ''; ?>"
                               style="width:518px;"/><i>权限名称</i>
                    </li>
                    <li><label><b>*</b>上级权限<b>*</b></label>


                        <div class="vocation">
                            <select class="select1" name="big_id">
                                <option value="0"></option>
								<?php foreach ($big_privilege as $val): ?>
                                    <option value="<?= $val['id'] ?>" <?= isset($info['parent_id']) && $info['parent_id'] == $val['id'] ? 'selected' : ''; ?>>
										<?= $val['privilege_name'] ?>
                                    </option>
								<?php endforeach; ?>
                            </select>
                        </div>
                        <i>选为空，即没有上级权限</i>

                    </li>
                    <li>
                        <label><b>*</b>控制器/方法<b>*</b></label>
                        <input name="action_name" type="text" class="dfinput"
                               value="<?= isset($info['action_name']) ? $info['action_name'] : ''; ?>"
                               style="width:518px;"/><i>/operator/list/</i>
                    </li>
                    <li>
                        <label><b>*</b>状态<b>*</b></label>
                        <input name="status" type="radio"
                               value="1" checked/>启用&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="status" type="radio"
                               value="0"/>停用
                    </li>
                    <li>
                        <label>设置的logo</label>
                        <input name="img_url" type="text" class="dfinput"
                               value="<?= isset($info['img_url']) ? $info['img_url'] : ''; ?>" style="width:518px;"/><i>/static/backend/images/icon06.png</i>
                    </li>
                    <li><label>&nbsp;</label><input name="" type="button" class="btn" value="马上发布"/></li>
                </ul>
                <input id="action" type="hidden" value="<?= $action ?>">
                <input id="idd" type="hidden" value="<?= isset($info['id']) ? $info['id'] : ''; ?>">
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $("#usual1 ul").idTabs();
    $(function () {
        $('.btn').click(function () {
            var name = $("input[name='name']").val();
            var big_id = $("select[name='big_id']").val();
            var action_name = $("input[name='action_name']").val();
            var status = $("input[name='status']").val();
            var img_url = $("input[name='img_url']").val();
            var action = $("#action").val();
            var id = $("#idd").val();
            if (name === '' || big_id === '' || action_name === '' || action === '') {
                alert('有红点的选项,不能为空！');
                return;
            }
            $.ajax({
                url: '/operator/data/',
                datatype: 'json',
                type: 'post',
                data: {
                    "data": 'action=' + action + '&name=' + name + '&big_id=' + big_id + '&action_name=' + action_name + '&status=' + status + '&img_url=' + img_url + '&id=' + id
                },
                success: function (data) {
                    alert('操作成功！');
                    location.href = '/operator/index/';
                }
            })
        })

    })
</script>