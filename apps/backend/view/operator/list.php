<script type="text/javascript">
    $(document).ready(function () {
        $(".click").click(function () {
            $(".tip").fadeIn(10);
        });

        $(".tiptop a").click(function () {
            $(".tip").fadeOut(200);
        });

        $(".sure").click(function () {
            $(".tip").fadeOut(100);
        });

        $(".cancel").click(function () {
            $(".tip").fadeOut(100);
        });

    });
</script>
<!-- index -->
<div class="index">
    <div class="place">
        <span>位置：</span>
        <ul class="placeul">
            <li><a href="/">首页</a></li>
            <li><a href="/operator/list">权限列表</a></li>
        </ul>
    </div>
    <div class="rightinfo">
        <table class="tablelist">
            <thead>
            <tr>
                <th>权限ID<i class="sort"><img src="/static/backend/images/px.gif"/></i></th>
                <th>上级ID</th>
                <th>权限名称</th>
                <th>控制器@动作名称</th>
                <th>logo</th>
                <th>状态</th>
                <th>发布时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
			<?php foreach ($all_menu as $val): ?>
                <tr>
                    <td><?= $val['id'] ?></td>
                    <td><?= $val['parent_id'] ?></td>
                    <td><?= $val['privilege_name'] ?></td>
                    <td><?= $val['action_name'] ?></td>
                    <td><img src="<?= $val['img_url'] ?>"/></td>
                    <td><?= $val['status'] == 1 ? '启用' : '停用' ?></td>
                    <td><?= $val['updated_at'] ?></td>
                    <td><a href="/operator/edit/?id=<?= $val['id'] ?>" class="tablelink">修改</a> <a
                                href="/operator/del/?id=<?= $val['id'] ?>" class="tablelink">删除</a></td>
                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $('.tablelist tbody tr:odd').addClass('odd');
</script>
</body>

