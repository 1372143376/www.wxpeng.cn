<?php
/**
 * User: ${weipeng}
 * Date: 17/12/21
 * Time: 上午 10:30
 */

namespace apps\backend\controller;

use model\water\user;
use model\water\privilege;
use module\tools_module;
use module\log_module;

class user_controller extends base_controller
{
	private $user_model;

	private $privilege_model;

	private $log_module;

	private $all_privilege;

	public function __construct()
	{
		$this->user_model = user::get_instance();
		$this->privilege_model = privilege::get_instance();
		$this->log_module = log_module::get_instance();
		parent::__construct();
		$this->all_privilege = $this->privilege_model->get_query()->where('is_del = 0 and status = 1')->indexBy('id')->all();
	}

	public function index_action()
	{
		//分析小菜单
		$this->parse_small_menu($_SERVER['REQUEST_URI']);
		$all_user = $this->user_model->get_query()->all();
		echo $this->render('user/index', [
			'all_user' => $all_user,
			'all_privilege' => $this->all_privilege
		]);
	}

	public function add_action()
	{
		$id = $this->request_service->get('id', '');
		$user_info = $this->user_model->find($id);
		echo $this->render('user/add', [
			'all_privilege' => $this->all_privilege,
			'user_info' => $user_info
		]);
	}


	public function del_action()
	{
		$id = $this->request_service->get('id', '');
		$this->user_model->delete($id);
		//记录日志
		$this->log_module->set_audit_log($this->user_info['id'], [
			'删除用户成功',
			'action' => 'del',
			'user' => $id,
			'pid' => ''
		], 1);
		$this->showmessage('删除用户成功');
	}


	public function del_privilege_action()
	{
		$privilege_id = $this->request_service->get('privilege_id', '');
		$id = $this->request_service->get('id', '');

		$user_info = $this->user_model->find($id);
		$tools_module = tools_module::get_instance();

		$pid = $tools_module->userinfo_pid_deal($user_info['pid'], $privilege_id);
		if (!empty($pid))
		{
			$this->user_model->update([
				'pid' => $pid
			], $id);
			//记录日志
			$this->log_module->set_audit_log($this->user_info['id'], [
				'解除权限成功',
				'action' => 'del',
				'user' => $id,
				'pid' => $privilege_id
			], 1);
		}
		$this->showmessage('解除权限成功');
	}


	public function edit_privilege_action()
	{
		$pid = $this->request_service->get('privilege_id', '');
		$id = $this->request_service->get('id', '');

		$user_info = $this->user_model->find($id);
		if (empty($user_info) || empty($pid))
		{
			$this->showalert('用户数据不存在', '/user/index/');
		}
		$this->user_model->update([
			'pid' => rtrim($pid, ',')
		], $id);
		//记录日志
		$this->log_module->set_audit_log($this->user_info['id'], [
			'修改用户权限成功',
			'action' => 'edit',
			'user' => $id,
			'pid' => $pid
		], 1);
		$this->showmessage('修改用户权限成功');
	}
}