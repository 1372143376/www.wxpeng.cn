<?php

namespace apps\backend\controller;

use cg\core\assets\manager;
use cg\core\cg_controller;
use cg\core\cg;
use cg\funcs;
use cg\core\cg_encrypt;
use cg\core\cg_cookie;
use model\water\setting;
use model\water\user;
use model\water\privilege;
use module\setting_module;
use MongoDB\Client;

class  base_controller extends cg_controller
{
	protected $layout = 'main';

	protected $template_file;
	protected $setting;
	public $is_windows;
	public $user_info;
	public $menu;
	//大分类的id
	private $id;
	/**
	 * @var manager
	 */
	protected $assets;
	private $setting_model;

	public $data;

	public $tag;

	public $admin;
	///缓冲
	// $redis_cache   $memcache_cache
	/// 是否开启 mongoDb
	protected $mongoDb = false;
	/// 是否启动tools工具
	protected $tools =true;

	public function __construct()
	{
		parent::__construct();
		if ($this->mongoDb)
		{
			$mongod = new Client();
			$ll = $mongod->selectCollection('test','water_user')->find(["id" => 1])->toArray();
			$result = $mongod->selectCollection('test','water_user')->find()->toArray();
			var_dump($ll);die;
			//插入
		/*	$result = $mongod->selectCollection('test','water_user')->insertOne([
				'id' => 3,
				'account' => 'test'
			]);*/
			//var_dump($result->isAcknowledged());
			die;
		}

		session_cache_limiter('private, must-revalidate');
		session_start();
		header('Cache-control:private, must-revalidate');
		header("content-type:text/html;charset=utf-8");
		mb_internal_encoding("UTF-8");

		if (!isset($_SERVER['HTTP_USER_AGENT']))
		{
			$_SERVER['HTTP_USER_AGENT'] = '';
		}

		$this->is_windows = DIRECTORY_SEPARATOR == '\\';
		$this->setting_model = setting::get_instance();
		$this->setting = $this->setting_model->get_query()->indexBy('skey')->all();
		$this->admin = setting_module::$admin;
	}


	/**
	 * This will be called before the actual action is executed
	 */
	public function beforeRun($resource, $action, $module_name = '')
	{
		parent::beforeRun($resource, $action, $module_name);
		if($this->tools === true && $this->controller_name == 'tools_controller')
		{
			return;
		}
		$this->template_engine = 'php';
		//控制器对应的静态文件的一级目录
		$this->template_dir = '';
		$this->init_config();
		$this->assets = cg::app()->get_assets_manager();
		$this->assets
			->collection('header')
			->addCss('select.css')
			->addCss('style.css')
			// 很多东西依赖 jquery, 所以放到顶部输出
			->addCommonJs('jquery-1.7.2.min.js');
		if ($this->controller_name != 'site_controller')
		{
			// 检查是否登录
			$this->check_login();
			// 分析大菜单
			$this->parse_menu();

		}

	}

	private function parse_menu()
	{
		$privilege_model = privilege::get_instance();
		$all = $privilege_model->get_privilege_by_pid($this->user_info['pid']);
		$data = [];
		foreach ($all as $k => $val)
		{
			$data[$val['parent_id']][$val['id']] = $val;
		}
		$this->menu = $data;
	}

// 分析小菜单
	public function parse_small_menu($action_name)
	{
		$privilege_model = privilege::get_instance();
		$privilege = $privilege_model->get_by_action_name($action_name);
		if (!in_array($privilege['id'], array_filter(explode(',', $this->user_info['pid']))))
		{
			echo $this->redirect('/index/show404');
		}
		$this->id = $privilege['id'];
	}

	protected function check_login()
	{
		if (empty($_SESSION['user']))
		{
			// 记录登录前需要访问的页面链接
			$_SESSION['visit_url'] = $_SERVER['REQUEST_URI'];
			echo $this->redirect('/site/login');
		}
		//登录成功,获取用户信息
		$user_model = user::get_instance();
		$this->user_info = $user_model->get_use_by_account($_SESSION['user']);
	}

	protected function redirect($url)
	{
		header('Location: ' . $url);
		die();
	}

	/**
	 * 渲染模版, 预先配置的变量都放在这里, 不需要额外弄个 $this->data 变量来存储
	 * 记载公共模板和css、js
	 * Renders a view and applies layout if available
	 * @param string $template
	 * @param array $data
	 * @return string 模版内容
	 */
	protected function render($template, array $data = [])
	{
		$data['setting'] = $this->setting;
		if (!empty($this->user_info))
		{
			$data['name'] = $this->user_info['username'];
			$data['pid'] = $this->user_info['pid'];
			$data['mobile'] = $this->user_info['account'];
			$data['member_id'] = $this->user_info['id'];
		}
		$data['selected_nav'] = '';
		$data['seo_title'] = '';
		$data['ip'] = $this->ip;
		$data['is_ipv6'] = stripos($this->ip, ':');
		// 分析菜单
		$data['menu'] = $this->menu;
		$data['id'] = $this->id;
		$data['assets'] = $this->assets;
		if (!empty($this->layout))
		{
			// 使用了布局模版
			$this->view()->set_layout_name($this->layout);
		}
		return $this->view()->render($template, $data);
	}

	/**
	 * Renders a view without applying layout
	 * 不加载公共模块和css、js文件
	 * @param string $template
	 * @param array $data
	 * @return string
	 */
	protected function renderPartial($template, array $data = [])
	{
		$this->layout = '';
		return $this->render($template, $data);
	}

	/**
	 *
	 * @param array|string $msg_data
	 * @param string $error
	 * @example:
	 * $msg_data = array(
	 *        'error' => true,false,'default'
	 *        'msg' => 'params error',
	 *        'return_url' => false, '/search/', 'default'   //default: history.back(-1)
	 * );
	 */
	protected function showmessage($msg_data = '', $error = 'default')
	{
		if (is_string($msg_data))
		{
			$msg_data = array(
				'error' => $error,
				'msg' => $msg_data
			);
		}

		if ($this->request_service->getIsAjax())
		{
			die(json_encode($msg_data));
		}
		else
		{
			// @todo, 根据请求类型返回
			$return_type = $this->request_service->get('return_type', '');
			if (empty($return_type))
			{
				$return_type = $this->request_service->post('return_type', '');
			}
			if ($return_type == 'text')
			{
				if ($msg_data['error'] !== false)
				{
					die('error: ' . $msg_data['msg']);
				}
				else
				{
					die($msg_data['msg']);
				}
			}
			$data['msg_data'] = $msg_data;

			// 不使用布局模版
			die($this->render('message', $data));
		}
	}

	/*
	 * 输出一个alert，跳转到指定的页面
	 * @$message string   输出提示信息
	 * @location_url  跳转的链接
	 */
	protected function showalert($message = '', $url = '')
	{
		if (empty($message))
		{
			$message = '操作成功!';
		}
		if (empty($url))
		{
			$url = '/index/index';
		}
		echo "<script>alert('" . $message . "');location.href = '" . $url . "'</script>";
	}

	private function init_config()
	{
		cg_cookie::init(cg::config()->config['cookie']);
		cg_encrypt::init(cg::config()->config['system_salt_key']);
	}

	public function format_content($content)
	{
		$content = str_replace("<br>", "<br />", $content);
		$content = str_replace("<img \nsrc", "<img src", $content);
		$content = funcs::pretty_goods_descr($content);
		$content = str_replace("<br />", "<br />\r\n", $content);
		$content = strip_tags($content, '<img><strong><p>');
		$content = str_replace('&nbsp;', '', $content);
		$content = str_replace(' border="1"', '', $content);
		$content = str_replace(' border="0"', '', $content);
		$content = str_replace(' align=""', '', $content);
		$content = str_replace(' align="left"', '', $content);
		$content = str_replace(">", ">\n", $content);
		$content = strip_tags($content, '<img><strong>');
		$content = preg_replace('/(\t*\r?\n\t*)+/', "\n", $content);
		$content = str_replace("<strong>\n", "<strong>", $content);
		$content = nl2br($content);
		$content = $content . '<br />';
		$content = str_replace('<img', '<br /><img', $content);
		$content = str_replace(array(
			'#p#',
			'#e#',
			"　",
			"\n",
			"\r",
			"\t "
		), "", $content);
		$content = preg_replace("/(<br \/>( |))+/", "<br />", $content);
		$content = str_replace("<br />", "<br />\r\n", $content);
		$content = preg_replace('/(.*?)<br \/>/i', '<p>$1</p>', $content);
		$content = str_replace('<p><img', '<p style="text-align:center"><img', $content);
		$content = str_replace('<p></p>', '', $content);
		$content = str_replace('<p> </p>', '', $content);
		$content = str_replace('<p><p>', '<p>', $content);
		$content = str_replace('</p></p>', '</p>', $content);
		$content = preg_replace('/<p>(( |)(&nbsp;|nbsp; ))+/i', '<p>', $content);
		return $content;
	}
}