<?php
/**
 * User: ${weipeng}
 * Date: 17/12/20
 * Time: 下午 15:56
 */

namespace apps\backend\controller;

use module\log_module;
use model\water\user;
use cg\core\cg_cookie;


class site_controller extends base_controller
{

	private $user_model;

	public function __construct()
	{
		parent::__construct();
		$this->user_model = user::get_instance();
	}

	public function login_action()
	{
		echo $this->renderPartial('site/login');
	}

	public function verify_action()
	{
		$user = $this->request_service->get('user');
		$pwd = $this->request_service->get('pwd');
		$user_info = [
			'user' => $user,
			'pwd' => $pwd,
		];
		if (empty($user) || empty($pwd))
		{
			$this->showmessage('用户或密码不能为空', false);
		}
		if (!$this->set_sign($user_info))
		{
			$this->showmessage('用户和密码有误', false);
		}
		//登录验证成功
		$this->showmessage('登录成功', true);

	}

	/*
	 * @param array $user_info 用户输入
	 * return bool
	 */
	private function set_sign($user_info)
	{
		$user_model = user::get_instance();
		ksort($user_info);
		$str = http_build_query($user_info);
		$info = $user_model->get_use_by_account($user_info['user']);
		if (empty($info))
		{
			return false;
		}
		if ($info['pwd'] != sha1($str))
		{
			return false;
		}
		//将用户存进cookie
		$cookie_days = 1;
		$cookie_time = $cookie_days * 86400;
		$this->set_login_cookie($user_info['user'], $user_info['pwd'], $cookie_time);

		//记录登录日志
		$log_module = log_module::get_instance();
		$log_module->set_login_log($info['account']);

		$_SESSION['user'] = $user_info['user'];
		return true;
	}

	private function set_login_cookie($user, $pwd, $cookie_time)
	{
		$expires = time() + $cookie_time;
		cg_cookie::set("user", $user, $expires);
		cg_cookie::set("pwd", $pwd, $expires);
	}

	public function logout_action()
	{
		$this->set_logout_cookie();
		echo "<script type='text/javascript'>location.href='/site/login/';</script>";
		die();
	}

	private function set_logout_cookie()
	{
		cg_cookie::delete("user");
		cg_cookie::delete("pwd");
	}
}