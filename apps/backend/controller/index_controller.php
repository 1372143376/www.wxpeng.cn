<?php

namespace apps\backend\controller;

use model\water\login_log;
use model\water\audit_log;
use model\water\vote;

class index_controller extends base_controller
{

	//心情说明，用半角逗号隔开
	private $moodname = '给力,淡定,学习,坑爹,打酱油';
//心情图标文件，用半角逗号隔开(images/目录)
	private $moodpic = 'geili.gif,dandian.gif,xuexi.gif,kengdie.gif,dajiangyou.gif';
//统计心情柱图标最大高度
	private $moodpicheight = 80;

	public function index_action()
	{
		//var_dump($this->data);die;
		$login_log_model = login_log::get_instance();
		$login_log = $login_log_model->get_last_login($this->user_info['account']);
		$audit_log_model = audit_log::get_instance();
		//$audit_log = $audit_log_model->get_query()->orderBy('created_at desc')->limit(8)->all();
		$page = $this->request_service->get('page', '');
		if (empty($page))
		{
			$page = 1;
		}
		$audit_log = $audit_log_model->pagination([], $page, 'order by created_at desc', 5);
		//var_dump($audit_log);die;
		echo $this->render('index/index', [
			'login_log' => $login_log,
			//是否是最高管理员
			'is_admin' => in_array($this->user_info['account'], $this->admin) ? 1 : 0,
			'audit_log' => $audit_log,
			'page' => $page
		]);
	}

//vote
	public function vote_action()
	{
		$action = $this->request_service->get('action', '');
		$vote_model = vote::get_instance();
		if ($action == 'send')
		{ //发表心情
			//文章id
			$id = (int)$this->request_service->post('id');
			//对应表情的id
			$mid = (int)$this->request_service->post('moodid');
			if ($mid < 0 || !$id)
			{
				echo "错误";
				exit;
			}

			$havemood = $this->chk_mood($id);
			if ($havemood == 1)
			{
				echo "您已表达过了";
				exit;
			}
			$field = 'mood' . $mid;
			//查询是否有这个id
			$row = $vote_model->find($id);
			if (is_array($row))
			{
				$query = $vote_model->update([
					"$field" => $row["$field"] + 1
				], $id);
				if ($query)
				{
					setcookie("mood" . $id, $mid . $id, time() + 3600);
					$rs = $vote_model->find($id);
					$total = $rs['mood0'] + $rs['mood1'] + $rs['mood2'] + $rs['mood3'] + $rs['mood4'];
					$height = round(($rs[$field] / $total) * $this->moodpicheight);
					echo $height;
				}
				else
				{
					echo -1;
				}
			}
			else
			{
				$vote_model->insert([
					'id' => $id,
					'mood0' => 0,
					'mood1' => 0,
					'mood2' => 0,
					'mood3' => 0,
					'mood4' => 0,

				]);

				$vote_model->update([
					"$field" => $row["$field"] + 1
				], $id);
				setcookie("mood" . $id, $mid . $id, time() + 3600);
				echo $this->moodpicheight;
			}
		}
		else
		{ //获取心情
			$mname = explode(',', $this->moodname);//心情说明
			$num = count($mname);
			$mpic = explode(',', $this->moodpic);//心情图标
			$id = (int)$this->request_service->get('id');
			$rs = $row = $vote_model->find($id);
			$arr = [];
			if ($rs)
			{
				$total = $rs['mood0'] + $rs['mood1'] + $rs['mood2'] + $rs['mood3'] + $rs['mood4'];
				for ($i = 0; $i < $num; $i++)
				{
					$field = 'mood' . $i;
					$m_val = intval($rs[$field]);
					$height = 0; //柱图高度
					if ($total && $m_val)
					{
						$height = round(($m_val / $total) * $this->moodpicheight); //计算高度
					}
					$arr[] = [
						'mid' => $i,
						'mood_name' => $mname[$i],
						'mood_pic' => $mpic[$i],
						'mood_val' => $m_val,
						'height' => $height
					];
				}
				die(json_encode($arr));
			}
			else
			{
				for ($i = 0; $i < $num; $i++)
				{
					$arr[] = [
						'mid' => $i,
						'mood_name' => $mname[$i],
						'mood_pic' => $mpic[$i],
						'mood_val' => 0,
						'height' => 0
					];
				}
				die(json_encode($arr));
			}
		}
	}

	public function phpinfo_action()
	{
		echo $this->renderPartial('index/phpinfo');
	}


	public function show404_action()
	{
		echo $this->renderPartial('index/show404');
	}

	//验证是否提交过
	private function chk_mood($id)
	{
		$cookie = isset($_COOKIE['mood' . $id]) ? $_COOKIE['mood' . $id] : null;
		if ($cookie)
		{
			$doit = 1;
		}
		else
		{
			$doit = 0;
		}
		return $doit;
	}
}