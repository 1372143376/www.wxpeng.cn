<?php
/**
 * User: ${weipeng}
 * Date: 17/12/31
 * Time: 下午 22:31
 */

namespace apps\backend\controller;

use module\setting_module;
use model\water\bill;
use module\log_module;

class bill_controller extends base_controller
{
	//几号机井
	private $well;

	private $bill_model;

	private $log_module;


	public function __construct()
	{
		parent::__construct();
		$this->well = setting_module::$well;
		$this->bill_model = bill::get_instance();
		$this->log_module = log_module::get_instance();
	}

	public function index_action()
	{
		//分析小菜单
		$this->parse_small_menu($_SERVER['REQUEST_URI']);

		$order_list = $this->bill_model->get_query()->orderBy('create_at desc')->all();
		//计算分钟单价
		$m_price = (float)sprintf('%.2f', $this->setting['price']['svalue'] / 60);
		$h_price = (int)$this->setting['price']['svalue'];
		$new_order_list = [];
		foreach ($order_list as $k => $val)
		{
			$new_order_list[$k] = $val;
			list($h, $m) = $this->count_bill($val['start_time'], $val['end_time']);
			$new_order_list[$k]['type'] = $this->well[$val['type']];
			$new_order_list[$k]['count'] = '用了' . $h . '小时' . $m . '分钟';
			$new_order_list[$k]['price'] = number_format($h * $h_price + $m * $m_price, 2);
		}

		echo $this->render('user/bill_list', [
			'order_list' => $new_order_list
		]);
	}

	public function add_action()
	{
		echo $this->render('user/bill_form', [
			'well' => $this->well,
			'action' => 'add'
		]);
	}


	///添加和修改数据
	public function data_action()
	{
		parse_str($this->request_service->post('data'));
		//添加
		if ($action == 'add')
		{
			$pid = $this->bill_model->insert([
				'user' => $name,
				'type' => $type,
				'start_time' => $start_time,
				'end_time' => $end_time,
				'status' => $status,
				'create_at' => $this->datetime,
				'writer' => $this->user_info['id'],
				'remark' => $remark,
				'year' => $year
			]);
			//记录日志
			$this->log_module->set_audit_log($this->user_info['id'], [
				'写入账成功',
				'action' => 'add',
				'user' => $name,
				'pid' => $type
			], 1);
		}
		//修改
		if ($action == 'edit')
		{
			$this->bill_model->update([
				'user' => $name,
				'type' => $type,
				'start_time' => $start_time,
				'end_time' => $end_time,
				'status' => $status,
				'remark' => $remark,
				'year' => $year,
				'update_at' => $this->datetime,
				'updator' => $this->user_info['id'],
			], $id);
			//记录日志
			$this->log_module->set_audit_log($this->user_info['id'], [
				'修改账成功',
				'action' => 'edit',
				'user' => $name,
				'pid' => $type
			], 1);
		}
		$this->showmessage('操作成功', true);
	}


	//计算小时分钟数  和总价
	private function count_bill($start_time = 0, $end_time = 0)
	{
		if ($end_time <= $start_time)
		{
			return [
				0,
				0
			];
		}
		$mian_time = strtotime($end_time) - strtotime($start_time);
		//$d = '';
		$h = '';
		$m = '';
		//$s = '';
		//计算天数
		//$d .= intval($mian_time / 86400) . '天';
		//$mian_h = $mian_time % 86400;

		//计算小时
		$h .= intval($mian_time / 3600);
		$mian_m = $mian_time % 3600;

		//计算分钟
		$m .= intval($mian_m / 60);
		//$mian_s = $mian_m % 60;
		//计算秒
		//$s .= intval($mian_s / 60) . '秒';
		return [
			$h,
			$m
		];
	}
}