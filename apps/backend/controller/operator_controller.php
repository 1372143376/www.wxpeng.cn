<?php
/**
 * User: ${weipeng}
 * Date: 17/12/20
 * Time: 下午 15:56
 */

namespace apps\backend\controller;

use model\water\privilege;
use model\water\user;
use module\log_module;

class operator_controller extends base_controller
{


	private $privilege_model;
	private $big_privilege = [];

	private $log_module;

	public function __construct()
	{
		$this->privilege_model = privilege::get_instance();
		$this->log_module = log_module::get_instance();
		//取出所有的大权限
		$this->big_privilege = $this->privilege_model->get_query()->where('parent_id = 0')->all();
		parent::__construct();
	}

	public function index_action()
	{
		$menu = $this->privilege_model->get_query()->where('status = 1 and is_del = 0')->orderBy('parent_id')->all();
		//分析小菜单
		$this->parse_small_menu($_SERVER['REQUEST_URI']);
		echo $this->render('operator/list',
			[
				'all_menu' => $menu
			]);
	}

	public function add_action()
	{
		echo $this->render('operator/form', [
			'big_privilege' => $this->big_privilege,
			'action' => 'add'
		]);
	}

	public function edit_action()
	{
		$id = $this->request_service->get('id','');
		$info =$this->privilege_model->find($id);
		if(empty($info))
		{
			$this->showmessage('异常的操作');
		}
		echo $this->render('operator/form', [
			'big_privilege' => $this->big_privilege,
			'info' => $info,
			'action' => 'edit'
		]);
	}

	public function del_action()
	{
		$id = $this->request_service->get('id','');
		$this->privilege_model->update([
			'is_del' => 1,
		],$id);
		//array
		$admin = $this->admin;
		$pid_array = explode(',',$this->user_info['pid']);
		if(in_array($this->user_info['account'],$admin) && in_array($id,$pid_array))
		{
			$pid ='';
			foreach ($pid_array as $val)
			{
				if ($val == $id)
				{
					continue;
				}
				$pid .=$val.',';
			}
			$user_model = user::get_instance();
			$user_model->update([
				'pid' => rtrim($pid,',')
			],$this->user_info['id']);
			//记录日志
			$this->log_module->set_audit_log($this->user_info['id'], [
				'添加权限成功',
				'action' => 'del',
				'user' => '',
				'pid' => $pid
			], 1);
		}
		$this->showalert('删除成功！','/operator/index/');
	}

	///添加和修改数据
	public function data_action()
	{
		parse_str($this->request_service->post('data'));
		//添加
		if ($action == 'add')
		{
			$pid = $this->privilege_model->insert([
				'privilege_name' => $name,
				'action_name' => $action_name,
				'status' => $status,
				'created_at' => $this->datetime,
				'parent_id' => $big_id,
				'img_url' => $img_url,
			]);
			//array
			$admin = $this->admin;
			if(in_array($this->user_info['account'],$admin))
			{
				$user_model = user::get_instance();
				$user_model->update([
					'pid' => $this->user_info['pid'].','.$pid
				],$this->user_info['id']);
			}
			//记录日志
			$this->log_module->set_audit_log($this->user_info['id'], [
				'添加权限成功',
				'action' => 'add',
				'user' => '',
				'pid' => $pid
			], 1);
		}
		//修改
		if ($action == 'edit')
		{
			$this->privilege_model->update([
				'privilege_name' => $name,
				'action_name' => $action_name,
				'status' => $status,
				'created_at' => $this->datetime,
				'parent_id' => $big_id,
				'img_url' => $img_url,
			],$id);
			//记录日志
			$this->log_module->set_audit_log($this->user_info['id'], [
				'修改权限成功',
				'action' => 'edit',
				'user' => '',
				'pid' => $pid
			], 1);
		}
		$this->showmessage('操作成功',true);
	}
}