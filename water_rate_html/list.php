<script type="text/javascript">
        $(document).ready(function(){
            $(".click").click(function(){
                $(".tip").fadeIn(10);
            });

            $(".tiptop a").click(function(){
                $(".tip").fadeOut(200);
            });

            $(".sure").click(function(){
                $(".tip").fadeOut(100);
            });

            $(".cancel").click(function(){
                $(".tip").fadeOut(100);
            });

        });
	</script>
<!-- index -->
<div class="index">
<div class="place">
	<span>位置：</span>
	<ul class="placeul">
		<li><a href="#">首页</a></li>
		<li><a href="#">数据表</a></li>
		<li><a href="#">基本内容</a></li>
	</ul>
</div>

<div class="rightinfo">

	<div class="tools">

		<ul class="toolbar">
			<li class="click"><span><img src="images/t01.png" /></span>添加</li>
			<li class="click"><span><img src="images/t02.png" /></span>修改</li>
			<li><span><img src="images/t03.png" /></span>删除</li>
			<li><span><img src="images/t04.png" /></span>统计</li>
		</ul>


		<ul class="toolbar1">
			<li><span><img src="images/t05.png" /></span>设置</li>
		</ul>

	</div>


	<table class="tablelist">
		<thead>
		<tr>
			<th><input name="" type="checkbox" value="" checked="checked"/></th>
			<th>权限ID<i class="sort"><img src="images/px.gif" /></i></th>
			<th>上级ID</th>
			<th>权限名称</th>
			<th>控制器@动作名称</th>
			<th>logo</th>
			<th>跳转链接</th>
			<th>状态</th>
			<th>发布时间</th>
			<th>操作</th>
		</tr>
		</thead>
		<tbody>

		<tr>
			<td><input name="" type="checkbox" value="" /></td>
			<td>1</td>
			<td>552</td>
			<td>admin</td>
			<td>江苏南京</td>
			<td>admin</td>
			<td>江苏南京</td>
			<td>江苏南京</td>
			<td>2013-09-02 15:05</td>
			<td><a href="#" class="tablelink">修改</a>     <a href="#" class="tablelink">删除</a></td>
		</tr>

		</tbody>
	</table>


	<div class="pagin">
		<div class="message">共<i class="blue">1256</i>条记录，当前显示第&nbsp;<i class="blue">2&nbsp;</i>页</div>
		<ul class="paginList">
			<li class="paginItem"><a href="javascript:;"><span class="pagepre"></span></a></li>
			<li class="paginItem"><a href="javascript:;">1</a></li>
			<li class="paginItem current"><a href="javascript:;">2</a></li>
			<li class="paginItem"><a href="javascript:;">3</a></li>
			<li class="paginItem"><a href="javascript:;">4</a></li>
			<li class="paginItem"><a href="javascript:;">5</a></li>
			<li class="paginItem more"><a href="javascript:;">...</a></li>
			<li class="paginItem"><a href="javascript:;">10</a></li>
			<li class="paginItem"><a href="javascript:;"><span class="pagenxt"></span></a></li>
		</ul>
	</div>


	<div class="tip">
		<div class="tiptop"><span>提示信息</span><a></a></div>

		<div class="tipinfo">
			<span><img src="images/ticon.png" /></span>
			<div class="tipright">
				<p>是否确认对信息的修改 ？</p>
				<cite>如果是请点击确定按钮 ，否则请点取消。</cite>
			</div>
		</div>

		<div class="tipbtn">
			<input name="" type="button"  class="sure" value="确定" />&nbsp;
			<input name="" type="button"  class="cancel" value="取消" />
		</div>

	</div>




</div>
</div>
<script type="text/javascript">
    $('.tablelist tbody tr:odd').addClass('odd');
</script>
</body>

