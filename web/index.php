<?php
if ('127.0.0.1' == $_SERVER['SERVER_ADDR'])
{
	define('CG_DEBUG', true);
	ini_set('display_errors', '1');
	error_reporting(E_ALL | E_STRICT | E_NOTICE );
}
else
{
	define('CG_DEBUG', false);
	ini_set('display_errors', '0');
}

date_default_timezone_set('Asia/Shanghai');
include '../vendor/autoload.php';
include '../config/config.php';

config::init();

cg\core\cg::config()->init(config::$config);
cg\core\cg::app()->route = config::$config['router'];
cg\core\cg::app()->run();
