<?php
namespace module;

use cg\core\cg;
use cg\funcs;
use cg\core\cg_model;
use model\water\setting;

class base_module extends cg_model
{
	public $setting = [];
	public $cookie;
	public $cache;
	public $is_wap = false;
	public $timestamp, $ip, $datetime;

	public function __construct()
	{
		$this->get_setting();

		$this->cache = cg::cache();
		$this->cookie = &$_COOKIE;
		$this->ip = $this->get_ip();
		$this->timestamp = time();
		$this->datetime = date("Y-m-d H:i:s", $this->timestamp);
	}
    /**
     *
     * @return base_module
     */
    public static function get_instance()
    {
        static $instance;
        $name = __CLASS__;
        if (!isset($instance[$name]))
        {
            $instance[$name] = new $name();
        }
        return $instance[$name];
    }


	public function get_setting()
	{
		$setting_model = setting::get_instance();
		$this->setting = $setting_model->get_query()->all();
	}

	public function get_ip()
	{
		$ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
		$ips = explode(',', $ip);
		if (!empty($ips))
		{
			$ip = $ips[0];
		}

		return $ip;
	}
}
