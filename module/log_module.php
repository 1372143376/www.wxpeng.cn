<?php

namespace module;

use model\water\login_log;
use model\water\audit_log;
use cg\funcs;

class log_module extends base_module
{
	private $login_log_model;
	private $audit_log_model;

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * @return log_module
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}

	/*
	 * 登录日志
	 */
	public function set_login_log($account)
	{
		$this->login_log_model = login_log::get_instance();
		$this->login_log_model->insert([
			'ip' => $this->ip,
			'created_at' => $this->datetime,
			'reg_os' => funcs::isMobile() == true ? 'pc' : funcs::isMobile(),
			'account' => $account
		]);
	}

	/*
	 * 操作日志
	 * $param string $meber_id 操作者的id
	 * $param array $action 操作记录的内容
	 * ***[
	 * ***'说明' ,
	 * ***'action' => 'del\edit\add',
	 * ***'user' => '操作者id'，
	 * ***'pid' => '操作方式id'
	 * ***]
	 * $param string $type 操作类型  setting_module;
	 *
	 */
	public function set_audit_log($member_id, $action, $type)
	{
		$this->audit_log_model = audit_log::get_instance();
		$this->audit_log_model->insert([
			'member_id' => $member_id,
			'created_at' => $this->datetime,
			'action' => json_encode($action),
			'type' => $type
		]);
	}
}
