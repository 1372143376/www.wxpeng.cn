<?php

namespace module;


class tools_module
{
	/**
	 *
	 * @return tools_module
	 */
	public static function get_instance()
	{
		static $instance;
		$name = __CLASS__;
		if (!isset($instance[$name]))
		{
			$instance[$name] = new $name();
		}
		return $instance[$name];
	}
	/*
	 * @param $pid string 权限id
	 * @param $id string 权限id
	 * return string //4,5,6,2
	 */
	public function userinfo_pid_deal($pid, $id = '')
	{
		$pid_array = explode(',', $pid);
		if (in_array($id, $pid_array))
		{
			$pid = '';
			foreach ($pid_array as $val)
			{
				if ($val == $id)
				{
					continue;
				}
				$pid .= $val . ',';
			}

			return rtrim($pid, ',');
		}

		return '';
	}

}
