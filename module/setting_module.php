<?php

namespace module;


class setting_module
{

	//最大权限用户
	public static $admin = [
		'weipeng',
		'admin',
		'17611102400',
		'15835132883'
	];

	//记录操作的类型,记录audit表

	public static $type = [
		'1',
		//后台管理的增删改加
		'2'
		//账单系统的增删改加
	];

	//几号机井
	public static $well = [
		1 => '东堡',
		2 => '西堡'
	];

}
